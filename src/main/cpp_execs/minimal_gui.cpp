/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Minimalistic main program.
 */
#include <iostream>
#include <QApplication>
#include <QMessageBox>
#include <QToolTip>
#include "common/FileSys.h"
#include "gui/factory/MinimalFactory.h"
#include "gui/controller/AppController.h"
#include "timekeeper/ClockTraits.h"
#include "timekeeper/TimeStamp.h"
#include "util/Exception.h"

using namespace std;
using namespace UA_CoMP::Timekeeper;
using namespace UA_CoMP::Util;
using namespace Minimal;
using namespace SimShell::Gui::Controller;

int main(int argc, char **argv)
{
	int exit_status = EXIT_SUCCESS;

	try {
		// Go ...
		cout << "Minimal Gui starting up at: " << TimeStamp().ToString() << endl;
		cout << "Executing " << argv[0]<< endl << endl;

		// qApp: closing last window quits app and exits app process.
		QApplication app(argc, argv, true);
		QObject::connect(qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));

		// Set icon search path
		QStringList search_paths = QIcon::themeSearchPaths();
		search_paths.push_back(QString::fromStdString(SimShell::InstallDirs::GetDataDir() + "/icons"));
		QIcon::setThemeSearchPaths(search_paths);
		QIcon::setThemeName("Tango");

		// Preliminary graphics objects
		QPalette tooltippalette = QToolTip::palette();
		QColor transparentcolor = QColor(tooltippalette.brush(QPalette::Window).color());
		tooltippalette.setBrush(QPalette::Window, QBrush(transparentcolor));
		QToolTip::setPalette(tooltippalette);

		// Main app components
		auto factory = make_shared<MinimalFactory>();
		AppController controller(factory);
		controller.setVisible(true);

		// Execute the app
		exit_status = app.exec();

		// Done ...
		cout << endl << controller.GetTimings() << endl << endl;
		cout << "Minimal Gui exiting at: " << TimeStamp().ToString() << endl << endl;
	}
	catch (exception& e) {
		cerr << e.what() << endl;
		QString qmess = QString("Exception:\n%1\n").arg(e.what());
		QMessageBox::critical(0, "Critical Error", qmess,
					QMessageBox::Abort, QMessageBox::NoButton, QMessageBox::NoButton);
		exit_status = EXIT_FAILURE;
	}
	catch (...) {
		cerr << "Unknown exception." << endl;
		QString qmess = QString("Unknown exception.");
		QMessageBox::critical(0, "Critical Error", qmess);
		exit_status = EXIT_FAILURE;
	}

	return exit_status;
}
