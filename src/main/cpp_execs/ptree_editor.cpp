/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Jan Broeckhove, Joeri Exelmans, UA/CoMP.
 */
/**
 * @file
 * Implementation for main program for ptree editor.
 */

#include <cstdlib>
#include <iostream>
#include <QApplication>
#include <QIcon>
#include <QMainWindow>
#include <QMessageBox>
#include <QSettings>
#include "common/FileSys.h"
#include "ptree/PTreeEditor.h"
#include "tclap/CmdLine.h"
#include "util/Exception.h"

using namespace std;
using namespace SimShell;
using namespace UA_CoMP::Util;

int main(int argc, char** argv)
{
	int exit_status = EXIT_SUCCESS;

	// qApp: closing last window quits app and exits app process.
	QApplication app(argc, argv, true);
	qRegisterMetaType<std::string>("std::string");
	QObject::connect(qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));

	try {
		// Specify & parse command line options
		TCLAP::CmdLine cmd("vleaf2 property tree editor", ' ', "1.0");
		TCLAP::ValueArg<std::string> file_arg("f", "file", "path to file to open", false, "", "PATH", cmd);
		cmd.parse(argc, argv);

		// Assign parsed command line argument values to variables
		std::string file = file_arg.getValue();

		// Set icon search path
		QStringList search_paths = QIcon::themeSearchPaths();
		search_paths.push_back(QString::fromStdString(InstallDirs::GetDataDir() + "/icons"));
		QIcon::setThemeSearchPaths(search_paths);
		QIcon::setThemeName("Tango");

		VirtualLeaf::Gui::PTreeEditor editor;
		editor.show();
		editor.resize(QSize(800, 500));

		// Read geometry from settings
		QSettings settings;
		if (settings.contains("editor_geometry_x") && settings.contains("editor_geometry_y")
			&& settings.contains("editor_geometry_w") && settings.contains("editor_geometry_h")) {
			int x = settings.value("editor_geometry_x").toInt();
			int y = settings.value("editor_geometry_y").toInt();
			int w = settings.value("editor_geometry_w").toInt();
			int h = settings.value("editor_geometry_h").toInt();
			editor.setGeometry(x, y, w, h);
		} else {
			editor.setGeometry(0, 0, 600, 800);
		}

		if (!file.empty()) {
			editor.OpenPath(file);
		}

		int exit_status = app.exec();

		// Write geometry to settings for next launch
		settings.setValue("editor_geometry_x", editor.geometry().x());
		settings.setValue("editor_geometry_y", editor.geometry().y());
		settings.setValue("editor_geometry_w", editor.geometry().width());
		settings.setValue("editor_geometry_h", editor.geometry().height());
	}
	catch (exception& e) {
		cerr << e.what() << endl;
		QString qmess = QString("Exception caught: %1").arg(e.what());
		QMessageBox::critical(0, "Critical Error", qmess,
			QMessageBox::Abort, QMessageBox::NoButton, QMessageBox::NoButton);
		exit_status = EXIT_FAILURE;
	}
	catch (...) {
		cerr << "Unknown exception." << endl;
		QString qmess = QString("Unknown exception.");
		QMessageBox::critical(0, "Critical Error", qmess);
		exit_status = EXIT_FAILURE;
	}

	return exit_status;
}
