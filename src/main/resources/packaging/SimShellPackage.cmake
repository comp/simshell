#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################

#============================================================================
# Generic CPack info.
#============================================================================
set( CPACK_PACKAGE_VERSION_MAJOR        "2" )
set( CPACK_PACKAGE_VERSION_MINOR        "0" )
set( CPACK_PACKAGE_VERSION_PATCH        "7" )
set( CPACK_PACKAGE_VENDOR               "VLEAF Consortium" ) 
set( CPACK_PACKAGE_NAME                 "SimShell" )
set( CPACK_PACKAGE_VERSION              ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH} )
set( CPACK_RESOURCE_FILE_LICENSE        ${CMAKE_SOURCE_DIR}/site/txt/gpl3.txt )
set( CPACK_PACKAGE_DESCRIPTION_SUMMARY  ${CMAKE_SOURCE_DIR}/main/resources/txt/summary.txt )
set( CPACK_PACKAGE_DESCRIPTION_FILE     ${CMAKE_SOURCE_DIR}/main/resources/txt/description.txt )
set( CPACK_RESOURCE_FILE_WELCOME        ${CMAKE_SOURCE_DIR}/main/resources/txt/welcome.txt )
set( CPACK_RESOURCE_FILE_README         ${CMAKE_SOURCE_DIR}/main/resources/txt/description.txt )

#============================================================================
# Platform dependent includes.
#============================================================================
if( APPLE )
	include(${CMAKE_SOURCE_DIR}/main/resources/packaging/MacOsX.cmake)
elseif( WIN32 )
	include(${CMAKE_SOURCE_DIR}/main/resources/packaging/Windows.cmake)
else( APPLE )
	include(${CMAKE_SOURCE_DIR}/main/resources/packaging/GenericOS.cmake)	
endif( APPLE )

#############################################################################
