#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################

#============================================================================
# Name of the application and paths inside the application bundle
#============================================================================
SET(APP_BUNDLE_NAME SimShell)
SET(APP_BUNDLE_BIN_DIR ${APP_BUNDLE_NAME}.app/Contents/MacOS)
SET(APP_BUNDLE_RESOURCE_DIR ${APP_BUNDLE_NAME}.app/Contents/Resources)

#============================================================================
# Resolves and copies all dependencies of a bundle and fixes all 
# the paths of the dynamic link libraries
# @param PLUGINDIR      A directory containing plugin libraries. These libs 
#                       are not directly referenced in the object files and 
#                       therefore cannot be discovered automatically
#                       by the fix_bundle function.
#============================================================================
function(install_fix_bundle  PLUGINDIR )

	# The name of the bundle directory  (Application name + .app)
	SET(APPS "\${CMAKE_INSTALL_PREFIX}/${APP_BUNDLE_NAME}.app")

	# Look in this directory for library dependencies, other external libs can be added
	SET(QTDIRS ${QT_LIBRARY_DIR})
		
	# Create the installation code that will fix the bundle, this code must we written
	# in a code block because it must be executed after all the targets are installed.
	#
	# GLOB_RECURSE lists all files in a directory and its sub-directories
	# fixup_bundle( BUNDLEPATH LIBS_IN_BUNDLE_TO_FIX DEPENDENCY_DIRS) 
	INSTALL(CODE "
		file(GLOB_RECURSE R_PLUGINS
		\"\${CMAKE_INSTALL_PREFIX}/${APP_BUNDLE_BIN_DIR}/${PLUGINDIR}/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
		
	include(BundleUtilities)
	SET(BU_CHMOD_BUNDLE_ITEMS ON)
	fixup_bundle(\"${APPS}\" \"\${R_PLUGINS}\" \"${QTDIRS};${APP_BUNDLE_BIN_DIR}\")
		" COMPONENT Runtime)
		
endfunction(install_fix_bundle)

#============================================================================
# Installs the "main" target into the package. This target will be executed when the 
# user double clicks on the application. This macro will set the required properties of
# the target and will install the required dependencies
# @param TARGETNAME The name of the cmake target
# @param ICON_PATH 	The full location of the icon in the source folder eg. src/icons/myicon.ics
# @param PICON 		The name of the icon eg. myicon.ics, the icon also has to be added as a source
#					file of the target (needed by cmake during the bundle creation).
#============================================================================
macro(package_main_target TARGETNAME ICON_PATH PICON)

	# Rename the main target to the bundle name, this is the name which will shown in Finder
	SET_TARGET_PROPERTIES( ${TARGETNAME} PROPERTIES RUNTIME_OUTPUT_NAME ${APP_BUNDLE_NAME}) 
		
	# Set Icon in the Info.plist file
	SET(MACOSX_BUNDLE_ICON_FILE ${PICON}) 
	
	# Set where in the bundle to put the icns file
	SET_SOURCE_FILES_PROPERTIES(${ICON_PATH} PROPERTIES MACOSX_PACKAGE_LOCATION Resources)
	
	# Install the main target into the bundle
	install( TARGETS        ${TARGETNAME}
		BUNDLE DESTINATION . RENAME ${APP_BUNDLE_NAME})
		
	# Copy the image format plugins to the bundle, currently this are the only plugin in use			 
	INSTALL(DIRECTORY "${QT_PLUGINS_DIR}/imageformats" 
		DESTINATION ${APP_BUNDLE_BIN_DIR}/plugins COMPONENT Runtime)

	# Create a qt.conf file. The file can be empty, if there are no settings qt will just look for 
	# directories with predefined names. If the qt.conf file is absent the global configuration
	# will be used, which has wrong plugin directories defined. 
		
	# Create the qt.conf file in the resource directory. This is the default location
	# if the application bundle is launched using Finder
	INSTALL(CODE "
		file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${APP_BUNDLE_RESOURCE_DIR}/qt.conf\" \"\")
		" COMPONENT Runtime)

	# Create the qt.conf file in the resource directory. This is the default location
	# if the application is launched using the terminal	
	INSTALL(CODE "
		file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${APP_BUNDLE_BIN_DIR}/qt.conf\" \"\")
		" COMPONENT Runtime)

	# Fix the qt plugins of the bundle		
	install_fix_bundle(plugins)
		
endmacro(package_main_target)

#============================================================================
# Installs the other targets into the package.
# @param ARG0..ARGN	A list of targets
#============================================================================
function(package_target_install)
	install( TARGETS   ${ARGV} DESTINATION    ${APP_BUNDLE_BIN_DIR} )	
endfunction(package_target_install)

#============================================================================
# Installs test files into the package.
# This may be used for packaging Java and Python test files.
# @param ARG0..ARGN	A list of files
#============================================================================
function(package_file_install)
	install( FILES     ${ARGV} DESTINATION    ${APP_BUNDLE_BIN_DIR} )	
endfunction(package_file_install)

#============================================================================
# Installs the the helper plugins into the package.
# @param ARG0..ARGN	A list of plugins
#============================================================================
function(package_helperplugin_install)
	install( TARGETS ${ARGV}     DESTINATION   ${APP_BUNDLE_BIN_DIR}/helper_plugins )
	install_fix_bundle(helper_plugins)	
endfunction(package_helperplugin_install)

#============================================================================
# Installs a link to the documentation for convenience of the user
#============================================================================ 
function(package_create_doc_link)
	install( DIRECTORY  "${CMAKE_SOURCE_DIR}/main/resources/packaging/macosx_files/SimShell Documentation.app"
		DESTINATION  .   USE_SOURCE_PERMISSIONS)
endfunction(package_create_doc_link)

#============================================================================
# Configure various variables if the package creation is enabled
#============================================================================
set( DATA_INSTALL_LOCATION		${APP_BUNDLE_NAME}.app/Contents/data)
set( DOC_INSTALL_LOCATION		${APP_BUNDLE_NAME}.app/Contents/doc)
set( TESTS_INSTALL_LOCATION		${CMAKE_INSTALL_PREFIX}/${APP_BUNDLE_NAME}.app/Contents/tests)
set( BIN_INSTALL_LOCATION 		${APP_BUNDLE_NAME}.app/Contents/MacOS)
	 
set( CPACK_GENERATOR 			"DragNDrop")
set( CPACK_BUNDLE_NAME			"SimShell")
set( CPACK_PACKAGE_ICON			${CMAKE_SOURCE_DIR}/main/resources/icons/leaficon.icns )
set( CPACK_PACKAGE_INSTALL_DIRECTORY	${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION} )
set( CPACK_INSTALL_PREFIX		"${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")  
set( CPACK_DMG_DS_STORE			"${CMAKE_SOURCE_DIR}/main/resources/packaging/macosx_files/DS_Store")
set( CPACK_DMG_BACKGROUND_IMAGE		"${CMAKE_SOURCE_DIR}/main/resources/packaging/macosx_files/background.png")
	
# set( CPACK_PACKAGING_INSTALL_PREFIX	"")
# set( CPACK_PACKAGING_INSTALL_PREFIX	"/SimShell")  #Use this line to place the applications in a folder
	
# The commands that will install the package, some post install commands can be added later
set( CPACK_INSTALL_COMMANDS				"make install")
	
set( CPACK_SET_DESTDIR                  OFF )   
set(CPACK_BINARY_DRAGNDROP 		ON)
include( CPack )

#############################################################################
