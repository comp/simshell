#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################

SET(APP_BUNDLE_NAME SimShell)

#============================================================================
# Resolves and copies all dependencies of a bundle and fixes all 
# the paths of the dynamic link libraries
# @param PLUGINDIR      A directory containing plugin libraries. These libs 
#                       are not directly referenced in the object files and 
#                       therefore cannot be discovered automatically
#                       by the fix_bundle function.
#============================================================================
function(install_fix_bundle  PLUGINDIR )

	# The name of the bundle directory  (Application name + .exe)
	#SET(APPS "\${CMAKE_INSTALL_PREFIX}/${APP_BUNDLE_NAME}.exe")

	# Look in this directory for library dependencies, other external libs can be added
	#SET(QTDIRS ${QT_LIBRARY_DIR})
		
	# Create the installation code that will fix the bundle, this code must we written
	# in a code block because it must be executed after all the targets are installed.
	#
	# GLOB_RECURSE lists all files in a directory and its sub-directories
	# fixup_bundle( BUNDLEPATH LIBS_IN_BUNDLE_TO_FIX DEPENDENCY_DIRS) 
		
	# The fixup_bundle does not work on windows with mingw. 
	# To check the dependencies the dumpbin tool is used, but this tool
	# is only available with Visual Studio. The cmake code should be
	# patched to also support the objdump tool, which is available with mingw.
		
	# To enable the packaging of dependencies, the install_required_libraries 
	# function can be used to copy the dependencies manually.		
		
	#INSTALL(CODE "
	#file(GLOB_RECURSE R_PLUGINS
	#\"\${CMAKE_INSTALL_PREFIX}/${PLUGINDIR}/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
		
	#set(gp_tool \"dumpbin\")
	#set(gp_cmd \"objdump -p\")
		
	#include(BundleUtilities)
	#fixup_bundle(\"${APPS}\" \"\${R_PLUGINS}\" \"${QTDIRS};\${CMAKE_INSTALL_PREFIX}\")
	#" COMPONENT Runtime)
		 
endfunction(install_fix_bundle)

#============================================================================
# Resolves all dependencies and copies the dynamic link libraries 
# manually to the installation folder. 
#============================================================================
macro(install_required_libraries)

	INSTALL(FILES ${QT_BINARY_DIR}/QtCore4.dll  DESTINATION ${BIN_INSTALL_LOCATION})
	INSTALL(FILES ${QT_BINARY_DIR}/QtGui4.dll   DESTINATION ${BIN_INSTALL_LOCATION})
	INSTALL(FILES ${QT_BINARY_DIR}/QtSvg4.dll   DESTINATION ${BIN_INSTALL_LOCATION})
	INSTALL(FILES ${QT_BINARY_DIR}/QtXml4.dll   DESTINATION ${BIN_INSTALL_LOCATION})

	find_library( STDCPPLOCATION NAMES  stdc++-6 )
	if( NOT STDCPPLOCATION-NOTFOUND)	
		INSTALL(FILES ${STDCPPLOCATION} DESTINATION ${BIN_INSTALL_LOCATION})	
	endif()
	if( Boost_REGEX_FOUND )
		INSTALL(FILES ${Boost_REGEX_LIBRARY_RELEASE} DESTINATION ${BIN_INSTALL_LOCATION})
	endif()
	if( HDF5_FOUND )
		if ( HDF5_LIBRARIES_RELEASE)
			INSTALL(FILES ${HDF5_LIBRARIES_RELEASE} DESTINATION ${BIN_INSTALL_LOCATION})
		endif()	
	endif()
	
endmacro(install_required_libraries)

#============================================================================
# Installs the "main" target into the package. This target will be executed when the 
# user double clicks on the application. This macro will set the required properties of
# the target and will install the required dependencies
# @param TARGETNAME     The name of the cmake target
# @param ICON_PATH      The full location of the icon in the source folder eg. src/icons/myicon.ics
# @param PICON 	        The name of the icon eg. myicon.ics, the icon also has to be added as a source
#                       file of the target (needed by cmake during the bundle creation).
#============================================================================
macro(package_main_target TARGETNAME ICON_PATH PICON)

	# Install the main target into the bundle
	install( TARGETS   ${TARGETNAME}   DESTINATION  ${BIN_INSTALL_LOCATION})
		
	# Copy the image format plugins to the bundle, currently this are the only plugins in use			 
	INSTALL(DIRECTORY "${QT_PLUGINS_DIR}/imageformats" DESTINATION ${BIN_INSTALL_LOCATION}/plugins COMPONENT Runtime)

	# Create a qt.conf file. The file can be empty, if there are no settings qt will just look for 
	# directories with predefined names. If the qt.conf file is absent the global configuration
	# will be used, which has wrong plugin directories defined. 
		
	# Create the qt.conf file in the resource directory. This is the default location
	# if the application bundle is launched using Finder
	INSTALL(CODE "
		file(WRITE \"\${CMAKE_INSTALL_PREFIX}/qt.conf\" \"\")
		" COMPONENT Runtime)
				
	install_required_libraries()	
	
endmacro(package_main_target)

#============================================================================
# Installs the other targets into the package.
# @param ARG0..ARGN	A list of targets
#============================================================================
function(package_target_install)
	install( TARGETS   ${ARGV}  DESTINATION   ${BIN_INSTALL_LOCATION} )	
endfunction(package_target_install)

#============================================================================
# Installs non-executable files into the package.
# This may be used for packaging Java and Python test files.
# @param ARG0..ARGN	A list of files
#============================================================================
function(package_file_install)
	install( FILES    ${ARGV}   DESTINATION   ${TESTS_INSTALL_LOCATION}/bin )	
endfunction(package_file_install)

#============================================================================
# Installs the the helper plugins into the package.
# 
# @param ARG0..ARGN	A list of plugins
#============================================================================
function(package_helperplugin_install)
	install( TARGETS ${ARGV}   DESTINATION   ${BIN_INSTALL_LOCATION}/helper_plugins )
	install_fix_bundle(helper_plugins)
endfunction(package_helperplugin_install)

#============================================================================
# Installs a link to the documentation for  convenience of the user
#============================================================================
function(package_create_doc_link)
# Do nothing
endfunction(package_create_doc_link)

#============================================================================
# Configure various variables if the package creation is enabled
#============================================================================
message("========================================================")
message("= Creating an NSIS installation package                =") 
message("= The NSIS tool is required, if not installed,         =") 
message("= download it from http://nsis.sourceforge.net/        =") 
message("========================================================")
	
set( DATA_INSTALL_LOCATION                data  )
set( DOC_INSTALL_LOCATION                 doc   )
set( TESTS_INSTALL_LOCATION               tests )
set( BIN_INSTALL_LOCATION                 .     )
	
set( CPACK_GENERATOR                      "NSIS")
set( CPACK_BUNDLE_NAME                    "SimShell")
	
set( CPACK_NSIS_MUI_ICON                  ${CMAKE_SOURCE_DIR}/main/resources/icons/leaficon.ico )
set( CPACK_NSIS_MUI_UNIICON               ${CMAKE_SOURCE_DIR}/main/resources/icons/leaficon.ico )
	
set( CPACK_PACKAGE_INSTALL_DIRECTORY      ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION} )
set( CPACK_INSTALL_PREFIX                 "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")  
		
#set( CPACK_PACKAGING_INSTALL_PREFIX      ".")
# set( CPACK_PACKAGING_INSTALL_PREFIX     "/SimShell")  #Use this line to place the applications in a folder
	
set( CPACK_PACKAGE_EXECUTABLES            ""   )
	
# The commands that will install the package, some post install commands can be added later
# set( CPACK_INSTALL_COMMANDS             "${CMAKE_MAKE_PROGRAM} install")
	
# Add shortcuts to the startmenu
set( CPACK_NSIS_MENU_LINKS               "doc" "Documentation" )

set( CPACK_SET_DESTDIR                  OFF )   
include( CPack )

#############################################################################
