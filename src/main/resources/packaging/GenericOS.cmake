#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################

#============================================================================
# Installs the "main" target into the package. This target will be executed when the 
# user double clicks on the application. This macro will set the required properties of
# the target and will install the required dependencies
# @param TARGETNAME     The name of the cmake target
# @param ICON_PATH      The full location of the icon in the source folder eg. src/icons/myicon.ics
# @param PICON          The name of the icon eg. myicon.ics, the icon also has to be added as a source
#                       file of the target (needed by cmake during the bundle creation).
#============================================================================
macro(package_main_target TARGETNAME ICON_PATH PICON)
	# Install the main target into the bundle
	install( TARGETS   ${TARGETNAME}   DESTINATION   ${BIN_INSTALL_LOCATION})
endmacro(package_main_target)

#============================================================================
# Installs the other targets into the package.
# @param ARG0..ARGN	A list of targets
#============================================================================
function(package_target_install)
	install( TARGETS  ${ARGV}   DESTINATION   ${BIN_INSTALL_LOCATION} )	
endfunction(package_target_install)

#============================================================================
# Installs test files into the package.
# This may be used for packaging Java and Python test files.
# @param ARG0..ARGN	A list of files
#============================================================================
function(package_file_install)
	install( FILES    ${ARGV}   DESTINATION   ${TESTS_INSTALL_LOCATION}/bin )	
endfunction(package_file_install)

#============================================================================
# Installs the the helper plugins into the package.
# @param ARG0..ARGN	A list of plugins
#============================================================================
function(package_helperplugin_install)
	install( TARGETS ${ARGV}   DESTINATION   ${BIN_INSTALL_LOCATION}/helper_plugins )	
endfunction(package_helperplugin_install)

#============================================================================
# Installs a link to the documentation for  convenience of the user
#============================================================================
function(package_create_doc_link)
# Do nothing
endfunction(package_create_doc_link)

#============================================================================
# Installs a link to the documentation for  convenience of the user
#============================================================================
function(package_create_cli_link)
# Do nothing
endfunction(package_create_cli_link)

#============================================================================
# Configure various variables if the package creation is enabled
#============================================================================
set( BIN_INSTALL_LOCATION                 bin   )
set( DATA_INSTALL_LOCATION                data  )
set( DOC_INSTALL_LOCATION                 doc   )
set( TESTS_INSTALL_LOCATION               tests )
set( CPACK_GENERATOR                      "ZIP" )
set( CPACK_BUNDLE_NAME                    "SimShell")
set( CPACK_PACKAGE_ICON	                  ${CMAKE_SOURCE_DIR}/main/resources/icons/leaficon.icns )
set( CPACK_PACKAGE_INSTALL_DIRECTORY      ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION} )
set( CPACK_INSTALL_PREFIX                 "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")  
# set( CPACK_PACKAGING_INSTALL_PREFIX     "/SimShell")  #Use this to put the applications in a folder
	
# The commands that will install the package, some post install commands can be added later
set( CPACK_INSTALL_COMMANDS               "make install")
set( CPACK_SET_DESTDIR                    OFF )   
include( CPack )

#############################################################################
