#ifndef GUI_CONTROLLER_H_INCLUDED
#define GUI_CONTROLLER_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2010 Roeland Merks.
 *  Copyright 2012, 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Header of GuiController.
 */
#include <string>
#include <chrono>
#include <boost/property_tree/ptree.hpp>

#include <QMainWindow>
#include <QSettings>

#include "adapt2rfp/adapt2rfp.h"
#include "clocks/clocks.h"
#include "factory/IFactory.h"
#include "workspace/IWorkspaceFactory.h"
#include "ProjectActions.h"

class QDockWidget;
class QMenu;
class QWidget;

namespace SimShell {

class WorkspaceQtModel;

namespace Gui {

class IPromptOnClose;
class PTreeContainer;
class PTreeContainerPreferencesObserver;
class WorkspaceView;

/**
 * Qt based GUI structure for the application.
 */
class GuiController: public QMainWindow, public UA_CoMP::Clock::Timeable<std::chrono::milliseconds>
{
	Q_OBJECT
public:
	/**
	 * @param canvas       canvas used for drawing the leaf
	 * @param mesh         mesh object embodies model
	 * @param parent       parent of window
	 * @param f            window flags
	 */
	GuiController(const std::shared_ptr<Factory::IFactory>&,
	              QWidget* parent = 0);

	/// Virtual destructor as we are overriding QMAinWindow methods.
	virtual ~GuiController();

	/// Initializes work space if need be.
	void InitWorkspace(std::string const& workspace_path);

	/// Accumulated execution timings from all projects in duration units specified in seconds.
	virtual TimingsType GetTimings() const;

private:
	typedef void (SigQtHandlerType)(QtMsgType, const char*);
	typedef UA_CoMP_Adapt2rfp::Adaptor<SigQtHandlerType> SigQtAdaptorType;

private:
	/**
	 * Shows confirmation dialogs for unsaved changes (overrides Qt method).
	 */
	virtual void closeEvent(QCloseEvent* event);

	/**
	 *
	 */
	bool CloseWorkspace();

	/**
	 * Query whether project has been successfully opened.
	 * @return	true iff project in open, valid state.
	 */
	bool IsProjectOpen() const;

	/**
	 * Query whether work space has been successfully opened.
	 * @return	true iff work space in open, valid state.
	 */
	bool IsWorkspaceOpen() const;

	/**
	 *
	 */
	bool OpenWorkspace(std::string const& path);

	/// Checks / un-checks the "running" toggle.
	void SetRunning(bool);

	/**
	 * Handler for Qt errors.
	 */
	void SigQtHandler(QtMsgType type, const char* msg);

private	slots:
	/// Connected to WorkspaceView::CloseProject signal. Emitted right before project removal.
	void ForceCloseProject(const std::string& project);

	/// Set up a project to run.
	bool OpenProject(const std::string& project, const std::string& leaf);

	/// Connected to timer
	void TimeStep();

	/// Display error message as a QMessageBox
	void UserError(std::string message);

	/// Display message in status bar
	void UserMessage(std::string message, int timeout = 0);

private	slots:
	// "File" menu
	void ShowNewProjectDialog();
	virtual bool CloseProject();
	//void ShowSimStateDialog();
	void RefreshWorkspace();
	void RunWorkspaceWizard();

	// "Simulation" menu
	void ToggleRunning(bool);

	// "Help" menu
	void AboutDialog();

	// Handlers for pressing 'Apply' button in one of the editors
	void ApplyParameters(const boost::property_tree::ptree&);
	void ApplyProjectPreferences(const boost::property_tree::ptree&);
	void ApplyWorkspacePreferences(const boost::property_tree::ptree&);

private:
	/// Close project without prompting to save changes.
	void ForceCloseProject();

private:
	std::shared_ptr<WorkspaceQtModel>          m_workspace_model;    ///< Work space with working directories for projects.
	std::shared_ptr<Factory::IFactory> m_shell_factory;
	std::shared_ptr<Ws::IWorkspaceFactory>     m_workspace_factory;
	std::shared_ptr<Ws::IProject>              m_ws_project;
	std::shared_ptr<Factory::ISimShellProject> m_project;            ///< Currently opened project, nullptr if no opened project.
	std::shared_ptr<ProjectActions>            m_project_actions;    ///< Creates actions associated with a project.
	std::string                                m_project_name;       ///< Name of currently opened project (in the context of WorkspaceQtModel).
	SigQtAdaptorType	                   m_sig_qt_adaptor;     ///< Controller method to raw function pointer for qt error handler.
	TimingsType                                m_timings;            ///< Accumulated timing records.

	QSettings                      m_settings;           ///< Application settings, stored on disk in user's home dir.
	QMenu*                         m_menu_exporters;     ///< Project export actions are added to this menu dynamically.
	QMenu*                         m_menu_viewers;       ///< Project viewer actions are added to this menu dynamically.
	QMenu*                         m_menu_simulation;    ///< Project simulation actions are added to this menu dynamically.
	QAction*                       m_action_run;
	QAction*                       m_action_single_step;
	QTimer*                        m_timer;              ///< Timer timeout() signal causes TimeStep to be called.
	WorkspaceView*                 m_workspace_view;     ///< Visual representation of workspace.

	/**
	 * Collection of actions and menus that have to be enabled/disabled with a controller state transition.
	 */
	class EnabledActions {
	public:
		void Add(QAction* a) {
			actions.push_back(a);
		}
		void Add(QMenu* m) {
			menus.push_back(m);
		}
		void Enable() {
			for (auto a : actions) a->setEnabled(true);
			for (auto m : menus) m->setEnabled(true);
		}
		void Disable() {
			for (auto a : actions) a->setEnabled(false);
			for (auto m : menus) m->setEnabled(false);
		}
	private:
		std::vector<QAction*>  actions;
		std::vector<QMenu*>    menus;
	};

	EnabledActions                  m_workspace_enabled_actions;     ///< Actions to be enabled not until a workspace is opened.
	EnabledActions                  m_project_enabled_actions;       ///< Actions to be enabled not until a project is opened.

	std::shared_ptr<PTreeContainer>                     m_parameters;              ///< Collection of menu, dock window, and editor associated with parameters.
	std::shared_ptr<PTreeContainerPreferencesObserver>  m_project_preferences;     ///< Collection of menu, dock window, and editor associated with project preferences.
	std::shared_ptr<PTreeContainerPreferencesObserver>  m_workspace_preferences;   ///< Collection of menu, dock window, and editor associated with workspace preferences.

	typedef std::map<std::string, IPromptOnClose*>  PromptOnCloseMap;

	PromptOnCloseMap  m_close_project_prompt_list;    ///< Items that may contain unsaved changes and must be closed together with project / leaf.
	PromptOnCloseMap  m_close_workspace_prompt_list;  ///< Items to be closed when work space is closed but can contain unsaved changes.

	static const QString g_workspace_caption;
};

} // namespace Gui
} // namespace SimShell

#endif
