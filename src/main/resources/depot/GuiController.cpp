/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011 Jan Broeckhove, Joeri Exelmans, UA/CoMP.
 */
/**
 * @file
 * Implementation of GuiController.
 */

#include <array>
#include <iomanip>
#include <sstream>
#include <QAction>
#include <QDockWidget>
#include <QIcon>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QMouseEvent>
#include <QStatusBar>
#include <QTimer>
#include <QToolTip>
#include "common/FileSys.h"
#include "common/PTreeQtState.h"
#include "clocks/clocks.h"
#include "IPromptOnClose.h"
#include "qtmodel/WorkspaceQtModel.h"
#include "math/math.h"
#include "util/Exception.h"
#include "ptree/PTreeUtils.h"
#include "util/log_debug.h"
#include "workspace/IFile.h"
#include "sim/SimBuilder.h"
#include "GuiController.h"
#include "NewProjectDialog.h"
#include "PTreeContainer.h"
#include "PTreeContainerPreferencesObserver.h"
#include "PTreeMenu.h"
#include "SaveChangesDialog.h"
#include "WorkspaceView.h"
#include "WorkspaceWizard.h"

namespace SimShell {
namespace Gui {

using namespace std;
using namespace std::chrono;
using namespace std::placeholders;
using namespace boost::property_tree;
using namespace UA_CoMP::Clock;
using namespace UA_CoMP::Util;
using namespace Factory;
using namespace ::VLeaf2_Sim;
using VLeaf2_Sim::Sim;
using VLeaf2_Sim::Event::SimEvent;

const QString GuiController::g_workspace_caption("Workspace");

GuiController::GuiController(const shared_ptr<IFactory>& factory,
                             QWidget* parent)
	: QMainWindow(parent),
	  m_workspace_model(nullptr),
	  m_shell_factory(factory),
	  m_workspace_factory(factory->CreateWorkspaceFactory()),
	  m_sig_qt_adaptor(bind(&GuiController::SigQtHandler, this, placeholders::_1, placeholders::_2)),
	  m_settings(QString::fromStdString(factory->GetOrganizationName()), QString::fromStdString(factory->GetApplicationName()))
{
	// Set icon search path
	QStringList search_paths = QIcon::themeSearchPaths();
	search_paths.push_back(QString::fromStdString(InstallDirs::GetDataDir() + "/icons"));
	QIcon::setThemeSearchPaths(search_paths);
	QIcon::setThemeName("Tango");

	// Preliminary graphics objects
	QPalette tooltippalette = QToolTip::palette();
	QColor transparentcolor = QColor(tooltippalette.brush(QPalette::Window).color());
	tooltippalette.setBrush(QPalette::Window, QBrush(transparentcolor));
	QToolTip::setPalette(tooltippalette);

	m_workspace_view = new WorkspaceView(this);
	setCentralWidget(m_workspace_view);
	connect(m_workspace_view, SIGNAL(ProjectOpened(std::string const&, std::string const&)),
		this, SLOT(OpenProject(std::string const&, std::string const&)));

	connect(m_workspace_view, SIGNAL(ProjectClosed(std::string const&)),
		this, SLOT(ForceCloseProject(std::string const&)));

	// Parameters dock
	m_parameters = make_shared<PTreeContainer>("Parameters"); {
		QDockWidget* dock = m_parameters->GetDock();
		addDockWidget(Qt::RightDockWidgetArea, dock);
		dock->hide();
	}
	m_close_project_prompt_list.insert(pair<string, IPromptOnClose*>("Parameters", m_parameters.get()));
	connect(m_parameters.get(), SIGNAL(Applied(const boost::property_tree::ptree &)),
		this, SLOT(ApplyParameters(const boost::property_tree::ptree &)));

	// Project Preferences dock
	m_project_preferences = make_shared<PTreeContainerPreferencesObserver>("Project Preferences"); {
		QDockWidget* dock = m_project_preferences->GetDock();
		addDockWidget(Qt::RightDockWidgetArea, dock);
		//dock->hide();
	}
	m_close_project_prompt_list.insert(pair<string, IPromptOnClose*>("Project Preferences", m_project_preferences.get()));
	connect(m_project_preferences.get(), SIGNAL(Applied(boost::property_tree::ptree const &)),
		this, SLOT(ApplyProjectPreferences(boost::property_tree::ptree const &)));


	// Workspace Preferences dock
	m_workspace_preferences = make_shared<PTreeContainerPreferencesObserver>("Workspace Preferences"); {
		QDockWidget* dock = m_workspace_preferences->GetDock();
		addDockWidget(Qt::RightDockWidgetArea, dock);
		dock->hide();
	}
	m_project_preferences->SetOnlyEditData(true);
	m_workspace_preferences->SetOnlyEditData(true);
	m_close_workspace_prompt_list.insert(pair<string, IPromptOnClose*>("Workspace Preferences", m_workspace_preferences.get()));
	connect(m_workspace_preferences.get(), SIGNAL(Applied(boost::property_tree::ptree const &)),
		this, SLOT(ApplyWorkspacePreferences(boost::property_tree::ptree const &)));

	QMenuBar* menu = menuBar();

	// ----------------------- FILE MENU ----------------------------------------------
	QMenu* menu_file = new QMenu("&File", menu);

	QAction* action_new_project = new QAction(QIcon::fromTheme("document-new"), "New Project...", this);
	action_new_project->setShortcut(QKeySequence("Ctrl+N"));
	action_new_project->setEnabled(false);
	connect(action_new_project, SIGNAL(triggered()), this, SLOT(ShowNewProjectDialog()));
	menu_file->addAction(action_new_project);
	m_workspace_enabled_actions.Add(action_new_project);

	menu_file->addSeparator();

	m_menu_exporters = new QMenu("&Export", this);
	m_menu_exporters->setEnabled(false);
	menu_file->addMenu(m_menu_exporters);

	menu_file->addSeparator();

	QAction* action_open = m_workspace_view->GetOpenAction();
	menu_file->addAction(action_open);

	QAction* action_rename = m_workspace_view->GetRenameAction();
	menu_file->addAction(action_rename);

	QAction* action_remove = m_workspace_view->GetRemoveAction();
	menu_file->addAction(action_remove);

	menu_file->addSeparator();

	QAction* action_refresh_workspace = new QAction(QIcon::fromTheme("view-refresh"), "Refresh Workspace", this);
	action_refresh_workspace->setShortcut(QKeySequence("F5"));
	action_refresh_workspace->setEnabled(false);
	connect(action_refresh_workspace, SIGNAL(triggered()), this, SLOT(RefreshWorkspace()));
	menu_file->addAction(action_refresh_workspace);
	m_workspace_enabled_actions.Add(action_refresh_workspace);


	QAction* action_set_workspace = new QAction("&Switch Workspace...", this);
	connect(action_set_workspace, SIGNAL(triggered()), this, SLOT(RunWorkspaceWizard()));
	menu_file->addAction(action_set_workspace);

	menu_file->addSeparator();

	QAction* action_close = new QAction("&Close", this);
	action_close->setShortcut(QKeySequence("Ctrl+W"));
	action_close->setEnabled(false);
	connect(action_close, SIGNAL(triggered()), this, SLOT(CloseProject()));
	menu_file->addAction(action_close);
	m_project_enabled_actions.Add(action_close);

	QAction* action_exit = new QAction("E&xit", this);
	connect(action_exit, SIGNAL(triggered()), this, SLOT(close()));
	action_exit->setShortcut(QKeySequence("Ctrl+Q"));
	menu_file->addAction(action_exit);

	menu->addMenu(menu_file);

	// ----------------------- EDIT MENU ----------------------------------------------
	QMenu* menu_edit = new QMenu("&Edit", menu);
	menu_edit->addMenu(m_project_preferences->GetMenu());
	menu_edit->addMenu(m_workspace_preferences->GetMenu());
	menu->addMenu(menu_edit);

	// ----------------------- VIEWERS MENU -------------------------------------------
	m_menu_viewers = new QMenu("Viewers", menu);
	m_menu_viewers->setEnabled(false);
	menu->addMenu(m_menu_viewers);

	// ----------------------- SIMULATION MENU -----------------------------------------------
	m_menu_simulation = new QMenu("&Simulation", menu);

	m_action_single_step = new QAction("Single &Step", this);
	m_action_single_step->setShortcut(QKeySequence("S"));
	m_action_single_step->setShortcutContext(Qt::ApplicationShortcut);
	m_action_single_step->setEnabled(false);
	connect(m_action_single_step, SIGNAL(triggered()), this, SLOT(TimeStep()));
	m_menu_simulation->addAction(m_action_single_step);
	m_project_enabled_actions.Add(m_action_single_step);

	m_action_run = new QAction("&Run Simulation", this);
	m_action_run->setShortcut(QKeySequence("R"));
	m_action_run->setShortcutContext(Qt::ApplicationShortcut);
	m_action_run->setEnabled(false);
	m_action_run->setCheckable(true);
	connect(m_action_run, SIGNAL(toggled(bool)), this, SLOT(ToggleRunning(bool)));
	m_menu_simulation->addAction(m_action_run);
	m_project_enabled_actions.Add(m_action_run);

	m_menu_simulation->addSeparator();

	menu->addMenu(m_menu_simulation);

	// --------------------------PARAMETERS MENU --------------------------------------
	menu->addMenu(m_parameters->GetMenu());

	// ----------------------- VIEW MENU ----------------------------------------------
	QMenu* menu_view = new QMenu("&View", menu);

	menu_view->addAction(m_parameters->GetDock()->toggleViewAction());
	menu_view->addAction(m_project_preferences->GetDock()->toggleViewAction());
	menu_view->addAction(m_workspace_preferences->GetDock()->toggleViewAction());

	menu->addMenu(menu_view);

	// ----------------------- HELP MENU ----------------------------------------------
	QMenu* menu_help = new QMenu("&Help", menu);

	QAction* action_about = new QAction("About", this);
	connect(action_about, SIGNAL(triggered()), this, SLOT(AboutDialog()));
	menu_help->addAction(action_about);

	menu->addMenu(menu_help);

	// --------- Start timer which repetitively invokes a simulation time step -------
	m_timer = new QTimer(this);
	connect(m_timer, SIGNAL(timeout()), SLOT(TimeStep()));

	// ----------------------- Rounding up -------------------------------------------
	SetRunning(false);
	setAnimated(false);
	UserMessage("Ready");
	setWindowTitle(QString::fromStdString(m_shell_factory->GetApplicationName()));
	setDockOptions(AllowTabbedDocks);

	// ----------------------- Qt error handler --------------------------------------
	qInstallMsgHandler(m_sig_qt_adaptor);

	// ----------------------- Read workspace ----------------------------------------
	string workspace;
	if (m_settings.contains("workspace")) {
		workspace = m_settings.value("workspace").toString().toStdString();
	}
	InitWorkspace(workspace);
}

GuiController::~GuiController()
{
}

void GuiController::AboutDialog()
{
	static QMessageBox* about = new QMessageBox(QMessageBox::Information,
		QString::fromStdString(m_shell_factory->GetApplicationName()),
		QString::fromStdString(m_shell_factory->GetAbout()),
		QMessageBox::Ok, this);

	about->setButtonText(1, "Dismiss");
	about->setIconPixmap(QPixmap(m_shell_factory->GetIcon()));
	about->show();
}

void GuiController::ApplyParameters(ptree const& pt)
{
	m_project->SetParameters(pt);
}

void GuiController::ApplyProjectPreferences(ptree const& new_prefs)
{
	auto project = m_workspace_model->Workspace()->Get(m_project_name);
	project->SetPreferences(new_prefs);

	try {
		// Plot, write png, xml, ...
		m_project->ForceExport();
	} catch (std::exception & e) {
		UserError(string("Could not set preferences: ") + e.what()); // Display error.

		// Roll back
		m_project_preferences->Undo();
	}
}

void GuiController::ApplyWorkspacePreferences(ptree const& pt)
{
	m_workspace_model->Workspace()->SetPreferences(pt);

	if (m_project) {
		try {
			// Plot, write png, xml, ...
			m_project->ForceExport();
		} catch (std::exception & e) {
			UserError(string("Could not set preferences: ") + e.what()); // Display error.

			// Roll back
			m_workspace_preferences->Undo();
		}
	}
}

void GuiController::closeEvent(QCloseEvent* event)
{
	if (m_project) {
		m_workspace_model->Workspace()->SetUserData("recently_opened", ptree(m_project_name));
	} else {
		m_workspace_model->Workspace()->SetUserData("recently_opened", ptree());
	}
	if (CloseWorkspace()) {
		event->accept();
		return;
	}
	event->ignore();
}

bool GuiController::CloseProject()
{
	bool status = false;
	if (SaveChangesDialog::Prompt(this, m_close_project_prompt_list)) {
		ForceCloseProject();
		status = true;
	}
	return status;
}

void GuiController::ForceCloseProject(std::string const& project)
{
	if (m_project_name == project) {
		ForceCloseProject();
	}
}

bool GuiController::CloseWorkspace()
{
	bool status = false;
	if (CloseProject()) {
		if (m_workspace_model) {
			if (SaveChangesDialog::Prompt(this, m_close_workspace_prompt_list))
			{
				ptree gui_state;
				gui_state.put_child("workspace_view", m_workspace_view->GetPTreeState());
				gui_state.put_child("project_prefs", m_project_preferences->GetPTreeState());
				gui_state.put_child("workspace_prefs", m_workspace_preferences->GetPTreeState());
				gui_state.put_child("parameters", m_parameters->GetPTreeState());
				// GuiController state
				{
					ptree gui_controller_state;
					auto widget_state = PTreeQtState::GetWidgetState(this);
					gui_controller_state.put_child("widget", widget_state);

					// Get dock window areas
					auto project_prefs_dock_state  = PTreeQtState::GetDockWidgetArea(this, m_project_preferences->GetDock());
					auto workspace_prefs_dock_state  = PTreeQtState::GetDockWidgetArea(this, m_workspace_preferences->GetDock());
					auto parameters_dock_state  = PTreeQtState::GetDockWidgetArea(this, m_project_preferences->GetDock());

					gui_controller_state.put_child("project_prefs_dock", project_prefs_dock_state);
					gui_controller_state.put_child("workspace_prefs_dock", workspace_prefs_dock_state);
					gui_controller_state.put_child("parameters_dock", parameters_dock_state);
					gui_state.put_child("gui_controller", gui_controller_state);
				}

				m_workspace_model->Workspace()->SetUserData("gui_state", gui_state);

				for (auto& prompt : m_close_workspace_prompt_list) {
						prompt.second->ForceClose();
				}

				m_workspace_model->Workspace()->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Unregister(m_workspace_preferences);

				m_workspace_model.reset();
				m_workspace_enabled_actions.Disable();
				status = true;
			}
		} else {
			status = true;
		}
	}
	return status;
}

void GuiController::ForceCloseProject() {
	if (m_project) {
		// Before destruction, get timings from project.
		m_timings.Merge(m_project->GetTimings());
		// Empty menus
		m_menu_exporters->clear();
		m_menu_exporters->setEnabled(false);

		ptree gui_state;
		gui_state.put_child("project_prefs", m_project_preferences->GetPTreeState());
		gui_state.put_child("parameters", m_parameters->GetPTreeState());
		m_ws_project->SetUserData("gui_state", gui_state);
		// Force close all project-dependent widgets
		for (auto& item : m_close_project_prompt_list) {
			item.second->ForceClose();
		}

		// Unregister project preferences dock from ws_project
		m_ws_project->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Unregister(m_project_preferences);

		for (auto a : m_project_actions->GetSimActions())
			m_menu_simulation->removeAction(a);

		QMenu* viewer_menu = m_project_actions->GetViewerMenu();
		if (viewer_menu) {
			menuBar()->insertMenu(viewer_menu->menuAction(), m_menu_viewers);
			menuBar()->removeAction(viewer_menu->menuAction());
		}
		m_menu_viewers->clear();
		m_project_actions.reset();
		m_project.reset(); // Delete project
	}

	// Go back to "closed project"-state
	setWindowTitle(QString::fromStdString(m_shell_factory->GetApplicationName()));
	SetRunning(false);
	m_project_enabled_actions.Disable();
}

DurationRecords<typename GuiController::DurationType> GuiController::GetTimings() const
{
	return m_timings.GetRecords<DurationType>();
}

void GuiController::InitWorkspace(std::string const& workspace_path)
{
	if (OpenWorkspace(workspace_path)) {
		// Ask user to create new project if work space is empty
		if (m_workspace_model->Workspace()->begin() == m_workspace_model->Workspace()->end()) {
			NewProjectDialog(m_workspace_factory, this);
		}

		try {
			auto& recently_opened = m_workspace_model->Workspace()->GetUserData("recently_opened");
			if (m_workspace_model->Workspace()->Find(recently_opened.get_value<string>()) != m_workspace_model->Workspace()->end())
				OpenProject(recently_opened.get_value<string>(), string());
		}
		// Ignore non-existing values.
		catch (ptree_bad_path&) {}
		catch (ptree_bad_data&) {}
	} else {
		// If program starts up with an invalid workspace,
		// run wizard to setup a brand-new workspace (or use an existing one)
		RunWorkspaceWizard();
	}
}

bool GuiController::IsProjectOpen() const
{
    return (bool) m_project;
}

bool GuiController::IsWorkspaceOpen() const
{
	return (bool) m_workspace_model;
}

void GuiController::ShowNewProjectDialog()
{
	NewProjectDialog dialog(m_workspace_factory);
	if (dialog.exec() == QDialog::Accepted)
	{
		auto src_project = dialog.GetSrcProject();
		auto project_name = dialog.GetProjectName();

		string src_file;
		string dst_file;
		string dst_file_path;

		try {
			QDir dst_dir(QString::fromStdString(m_workspace_model->Workspace()->GetPath()));
			dst_dir.mkdir(QString::fromStdString(project_name));

			// Copy all files from src_project to new project dir.
			QDir src_dir(QString::fromStdString(src_project->second->GetPath()));
			auto src_files = src_dir.entryList(QDir::Files | QDir::Hidden);
			for (auto& src_file : src_files) {
				QFile::copy(QString::fromStdString(src_project->second->GetPath()) + '/' + src_file,
				            QString::fromStdString(m_workspace_model->Workspace()->GetPath()) + '/' + QString::fromStdString(project_name) + '/' + src_file);
			}

			// New project.
			auto new_project = m_workspace_model->Workspace()->Add("project", project_name);


			// Copy preferences from src_project.
			/*new_project->second->SetPreferences(src_project->second->GetPreferences());

			// Copy last file from src_project to new project.
			src_file = src_project->second->Back()->GetPath();
			dst_file = src_project->second->Back()->GetName();
			dst_file_path = new_project->second->GetPath() + '/' + dst_file;
			if (!QFile::copy(QString::fromStdString(src_file), QString::fromStdString(dst_file_path))) {
				throw Exception("Unable to copy \"" + src_file + "\" to project dir \"" + new_project->second->GetPath() + "\"");
			}

			// Add copied file to newly created project.
			new_project->second->Add(dst_file);*/
		}
		catch (Exception& e) {
			QMessageBox::warning(this, "Error", QString("Could not create project: ") + e.what());
		}
	}
}

bool GuiController::OpenProject(std::string const& project_name, std::string const& file_name)
{
	// If a project is open, it must be closed first.
	if (m_workspace_model && CloseProject()) {
		try {
			auto project_it = m_workspace_model->Workspace()->Find(project_name); // may throw

			Ws::IProject::ConstFileIterator file_it;
			if (file_name.empty()) {
				// No leaf given, use most recent one.
				file_it = --project_it->second->end(); // may throw
			} else {
				file_it    = project_it->second->Find(file_name); // may throw
			}

			// may throw
			m_project = m_shell_factory->OpenProject(m_workspace_model->Workspace(), project_it, file_it, this);

			// Go into opened state
			m_ws_project = project_it->second.Project();
			m_project_name = project_name;
			m_parameters->Open(m_project->GetParameters());
			m_project_preferences->Open(m_ws_project->GetPreferences());

			try {
				auto& gui_state = m_ws_project->GetUserData("gui_state");
				m_project_preferences->SetPTreeState(gui_state.get_child("project_prefs"));
				m_parameters->SetPTreeState(gui_state.get_child("parameters"));
			}
			// Ignore exceptions
			catch (ptree_bad_path&) {}
			catch (ptree_bad_data&) {}

			// Register project preferences with workspace project PreferencesChanged subject.
			auto handler = bind(&PTreeContainerPreferencesObserver::Update, m_project_preferences.get(), std::placeholders::_1);
			m_ws_project->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(m_project_preferences, handler);

			QString window_title_path
				= QString::fromStdString(m_ws_project->GetPath());
			setWindowTitle(QString::fromStdString(m_shell_factory->GetApplicationName()) + ": " + window_title_path);
			UserMessage("Successfully read leaf from file " + file_it->second->GetPath());

			// Create ProjectActions instance.
			m_project_actions = ProjectActions::Create(m_project, this);

			// Exporters menu.
			auto& exporter_actions = m_project_actions->GetExporterActions();
			if (exporter_actions.size()) {
				m_menu_exporters->setEnabled(true);
				for (auto action : exporter_actions)
					m_menu_exporters->addAction(action);
			}
			// Simulation menu.
			m_menu_simulation->setEnabled(true);
			for (auto action : m_project_actions->GetSimActions())
				m_menu_simulation->addAction(action);
			// Viewers menu.
			QMenu* viewer_menu = m_project_actions->GetViewerMenu();
			if (viewer_menu) {
				menuBar()->insertMenu(m_menu_viewers->menuAction(), viewer_menu);
				menuBar()->removeAction(m_menu_viewers->menuAction());
				if (viewer_menu->actions().empty())
					viewer_menu->setEnabled(false);
			}

			// Enable menus and menu actions
			m_project_enabled_actions.Enable();

			return true;
		} catch (Exception& e) {
			QMessageBox::warning(this, "Error", QString("Could not open project: ") + e.what());
		}
	}
	return false;
}

bool GuiController::OpenWorkspace(std::string const& path)
{
	bool status = false;
	if (CloseWorkspace()) {
		try {
			m_workspace_model = WorkspaceQtModel::Create(m_workspace_factory, path); // Will throw exception if invalid path.
			m_workspace_enabled_actions.Enable();
			m_workspace_preferences->Open(m_workspace_model->Workspace()->GetPreferences());
			m_workspace_view->setModel(m_workspace_model.get());

			try {
				auto& gui_state = m_workspace_model->Workspace()->GetUserData("gui_state");
				m_workspace_view->SetPTreeState(gui_state.get_child("workspace_view"));
				m_project_preferences->SetPTreeState(gui_state.get_child("project_prefs"));
				m_workspace_preferences->SetPTreeState(gui_state.get_child("workspace_prefs"));
				m_parameters->SetPTreeState(gui_state.get_child("parameters"));

				auto& gui_controller_state = gui_state.get_child("gui_controller");

				// Get main window size and position
				PTreeQtState::SetWidgetState(this, gui_controller_state.get_child("widget"));

				// Get dock window areas
				PTreeQtState::SetDockWidgetArea(this, m_project_preferences->GetDock(), gui_controller_state.get_child("project_prefs_dock"));
				PTreeQtState::SetDockWidgetArea(this, m_workspace_preferences->GetDock(), gui_controller_state.get_child("workspace_prefs_dock"));
				PTreeQtState::SetDockWidgetArea(this, m_parameters->GetDock(), gui_controller_state.get_child("parameters_dock"));

			}
			// Ignore non-existing values.
			catch (ptree_bad_path&) {}
			catch (ptree_bad_data&) {}

			// Register workspace preferences container with workspace PreferencesChanged subject.
			auto handler = bind(&PTreeContainerPreferencesObserver::Update, m_workspace_preferences.get(), std::placeholders::_1);
			m_workspace_model->Workspace()->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(m_workspace_preferences, handler);

			m_settings.setValue("workspace", QString::fromStdString(path));

			status = true;
		}
		catch (Exception&) {
			m_workspace_model = nullptr;
			status = false;
		}
	}

	return status;
}

void GuiController::RefreshWorkspace()
{
	if (m_workspace_model) {
		// Remember names of projects that were expanded in the view.
		std::set<std::string> expanded;
		for (int i = 0; i < m_workspace_model->rowCount(); i++) {
			QModelIndex index = m_workspace_model->index(i, 0);
			if (m_workspace_view->isExpanded(index)) {
				expanded.insert(m_workspace_model->GetName(index));
			}
		}

		// Actual refresh
		std::string path = m_workspace_model->Workspace()->GetPath();
		OpenWorkspace(path);
		m_workspace_view->setModel(m_workspace_model.get());

		// Re-expand projects that were expanded.
		for (int i = m_workspace_model->rowCount() - 1; i >= 0; i--) {
			QModelIndex index = m_workspace_model->index(i, 0);
			if (expanded.find(m_workspace_model->GetName(index)) != expanded.end()) {
				m_workspace_view->setExpanded(index, true);
			}
		}
	}
}

void GuiController::RunWorkspaceWizard()
{
	SetRunning(false);

	WorkspaceWizard wizard(m_workspace_factory);
	if (wizard.exec() == QDialog::Accepted) {
		if (CloseProject()) {
			InitWorkspace(wizard.GetWorkspaceDir());
		}
	}
}

void GuiController::SetRunning(bool running)
{
	m_action_run->setChecked(running);
}

void GuiController::SigQtHandler(QtMsgType type, const char* msg)
{
	switch (type) {
	case QtDebugMsg:
		UserMessage("Qt debug: " + string(msg));
		break;
	case QtWarningMsg:
		UserMessage("Qt warning: " + string(msg));
		break;
	case QtCriticalMsg:
		UserError("Qt critical: " + string(msg));
		break;
	case QtFatalMsg:
		UserError("Qt fatal: " + string(msg));
		abort();
		break;
	}
}

void GuiController::TimeStep()
{
	try {
		bool state = m_project->TimeStep();
		if (state) {
			UserMessage(m_project->GetStatusMessage());
		} else {
			SetRunning(false);
			UserMessage("Termination condition satisfied. " +  m_project->GetStatusMessage());
		}
	}
	catch (Exception& e) {
		UserError(e.what());
		throw e;
	}
}

void GuiController::ToggleRunning(bool running)
{
	if (running) {
		m_timer->start(0);
		string msg("Simulation started. ");
		if (m_project) {
			msg += m_project->GetStatusMessage();
		}
		UserMessage(msg);
	} else {
		m_timer->stop();
		string msg("Simulation stopped. ");
		if (m_project) {
			msg += m_project->GetStatusMessage();
		}
		UserMessage(msg);
	}
	m_action_single_step->setEnabled(!running);
}

void GuiController::UserError(string message)
{
	QMessageBox::warning(this, "Error", QString::fromStdString(message));
}

void GuiController::UserMessage(string message, int timeout)
{
	statusBar()->showMessage(QString::fromStdString(message), timeout);
}

} // namespace Gui
} // namesapce VirtualLeaf
