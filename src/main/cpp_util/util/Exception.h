#ifndef INC_UTIL_EXCEPTION_H
#define INC_UTIL_EXCEPTION_H
/*
 *  This file is part of the gobelijn software.
 *  Gobelijn is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or any later
 *  version. Gobelijn is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for details. You should have received
 *  a copy of the GNU General Public License along with the software. If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Jan Broeckhove.
 */
/**
 * @file
 * Implementation for exception.
 */
#include <string>
#include <stdexcept>

namespace UA_CoMP {
namespace Util {

/// Extremely simple Exception root class.
class Exception: public std::exception
{
public:
	/// Virtual destructor for abstract class.
	virtual ~Exception() noexcept {}

	/// Constructor initializes message for the exception.
	Exception(std::string const& m) : m_msg(m) { }

	/// Return error message
	virtual const char* what() const noexcept { return m_msg.c_str(); }

private:
	std::string m_msg;
};

} // namespace Util
} // namespace UA_CoMP

#endif // end-of-include-guard

