#ifndef SUBJECT_H_INCLUDED
#define SUBJECT_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Subject in Observer pattern.
 */

#include <functional>
#include <map>
#include <memory>
#include <string>

namespace UA_CoMP {
namespace Util {

template <typename E, typename K = void>
class Subject {
public:
	typedef E                                      EventType;
	typedef const K*                               KeyType;
	typedef std::function<void(const EventType&)>  CallbackType;

	virtual ~Subject()
	{
		UnregisterAll();
	}

	template<typename U>
	void Register(const U*, CallbackType);

	template <typename U>
	void Unregister(const U*);

	void UnregisterAll();

	void Notify(const EventType&);

private:
	typedef std::less<KeyType>  Compare;

	std::map<KeyType, CallbackType, Compare> m_observers;
};


//template<typename E, typename K>
//template<typename U>
//void Subject<E, K>::Register(const U* u, CallbackType f)
//{
//	m_observers.insert(make_pair(static_cast<KeyType>(u), f));
//}
//
//template <typename E, typename K>
//template <typename U>
//void Subject<E, K>::Unregister(const U* u)
//{
//	m_observers.erase(static_cast<KeyType>(u));
//}
//
//template<typename E, typename K>
//void Subject<E, K>::UnregisterAll()
//{
//	m_observers.clear();
//}
//
//template<typename E, typename K>
//void Subject<E, K>::Notify(const EventType& e)
//{
//	for (auto const& o : m_observers) {
//		(o.second)(e);
//	}
//}


/**
 * Partial specialization for weak_ptr<void> key type.
 */
template <typename E>
class Subject<E, std::weak_ptr<const void>>
{
public:
	typedef E                                      EventType;
	typedef std::weak_ptr<const void>              KeyType;
	typedef std::function<void(const EventType&)>  CallbackType;


	virtual ~Subject()
	{
		UnregisterAll();
	}

	template <typename U>
	void Register(const std::shared_ptr<U>&, CallbackType);

	template <typename U>
	void Unregister(const std::shared_ptr<U>&);

	void UnregisterAll();

	void Notify(const EventType&);

private:
	typedef std::owner_less<KeyType>  Compare;

	std::map<KeyType, CallbackType, Compare> m_observers;
};


template<typename E>
template<typename U>
void Subject<E, std::weak_ptr<const void>>::Register(const std::shared_ptr<U>& u, CallbackType f)
{
	m_observers.insert(make_pair(std::static_pointer_cast<const void>(u), f));
}

template <typename E>
template <typename U>
void Subject<E, std::weak_ptr<const void>>::Unregister(const std::shared_ptr<U>& u)
{
	m_observers.erase(std::static_pointer_cast<const void>(u));
}

template<typename E>
void Subject<E, std::weak_ptr<const void>>::UnregisterAll()
{
	m_observers.clear();
}

template<typename E>
void Subject<E, std::weak_ptr<const void>>::Notify(const EventType& e)
{
	for (auto const& o : m_observers) {
		auto spt = o.first.lock();
		if (spt) {
			(o.second)(e);
		} else {
			// JB the statement below causes frequent crashes on Mac OS X
			// As it's just a clean up, I've commented it out
			//auto it = m_observers.erase(o.first);
		}
	}
}

} // namespace Util
} // namespace UA_CoMP

#endif
