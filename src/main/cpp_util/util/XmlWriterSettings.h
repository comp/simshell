#ifndef PTREE_SETTINGS_H_INCLUDED
#define PTREE_SETTINGS_H_INCLUDED
/*
 *  This file is part of the gobelijn software.
 *  Gobelijn is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or any later
 *  version. Gobelijn is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for details. You should have received
 *  a copy of the GNU General Public License along with the software. If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Jan Broeckhove.
 */
/**
 * @file.
 * Print (argc, argv) info.
 */

#include <boost/property_tree/xml_parser.hpp>
#include <boost/version.hpp>

namespace UA_CoMP {
namespace Util {

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

class XmlWriterSettings
{
public:
#if BOOST_VERSION >= 105600
	static xml_writer_settings<typename ptree::key_type> GetTab()
	{
		return xml_writer_make_settings<ptree::key_type>('\t', 1);
	}
#else
	static xml_writer_settings<char> GetTab()
	{
		return xml_writer_make_settings('\t', 1);
	}
#endif
};

} // namespace Util
} // namespace UA_CoMP

#endif // end-of-include-guard
