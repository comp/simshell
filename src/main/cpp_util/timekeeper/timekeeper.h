#ifndef TIMEKEEPER_CLOCKS_H_INCLUDED
#define TIMEKEEPER_CLOCKS_H_INCLUDED
/*
 *  This file is part of the gobelijn software.
 *  Gobelijn is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or any later
 *  version. Gobelijn is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for details. You should have received
 *  a copy of the GNU General Public License along with the software. If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Jan Broeckhove, Dirk De Vos.
 */
/**
 * @file
 * Utility header file for clock related classes.
 */

#include <chrono>
#include "timekeeper/ClockCLib.h"
#include "timekeeper/CumulativeRecords.h"
#include "timekeeper/Records.h"
#include "timekeeper/Stopwatch.h"
#include "timekeeper/Timeable.h"
#include "timekeeper/TimeStamp.h"
#include "timekeeper/Utils.h"

#endif // end-of-include-guard
