#ifndef TIMEKEEPER_CLOCK_TRAITS_H_INCLUDED
#define TIMEKEEPER_CLOCK_TRAITS_H_INCLUDED
/*
 *  This file is part of the gobelijn software.
 *  Gobelijn is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or any later
 *  version. Gobelijn is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for details. You should have received
 *  a copy of the GNU General Public License along with the software. If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Jan Broeckhove.
 */
/**
 * @file
 * Types related to timekeeping.
 */

#include <chrono>
#include "ClockCLib.h"
#include "CumulativeRecords.h"
#include "IndividualRecords.h"
#include "Stopwatch.h"

namespace UA_CoMP {
namespace Timekeeper {

/**
 * Types related to timekeeping.
 */
template<typename C = ClockCLib, typename D = typename C::duration>
class ClockTraits
{
public:
	/// Type for clock.
	using Clock = C;

	/// Stopwatch to measure time durations.
	using Stopclock = UA_CoMP::Timekeeper::Stopwatch<C>;

	/// Type for time duration units.
	using Duration = D;

	/// Records with cumulative timing info.
	using CumulativeTimings = UA_CoMP::Timekeeper::CumulativeRecords<D>;

	/// Records with timing info.
	using IndividualTimings = UA_CoMP::Timekeeper::IndividualRecords<D>;

};

} // namespace Timekeeper
} // namespace UA_CoMP

#endif // end-of-include-guard
