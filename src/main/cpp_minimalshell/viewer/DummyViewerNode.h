#ifndef VIEWER_DUMMYVIEWERNODE_H_INCLUDED
#define VIEWER_DUMMYVIEWERNODE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Dummy ViewerNode.
 */

#include "util/Subject.h"
#include "viewer/ViewerNode.h"

namespace SimShell {
namespace Event {

	class DummyEventType;

} // namespace Event
} // namespace SimShell

namespace UA_CoMP {
namespace Util {

		extern template class Subject<SimShell::Event::DummyEventType, std::weak_ptr<const void>>;

} //namespace Util
 // namespace UA_CoMP
}

namespace SimShell {

	using DummySubject = UA_CoMP::Util::Subject<Event::DummyEventType, std::weak_ptr<const void>>;

namespace Event {

	/**
	 * Empty event, for usage with SimShell::DummySubject.
	 */
	class DummyEventType {};

} // namespace Event
} // namespace SimShell


namespace SimShell {
namespace Viewer {

	/**
	 *
	 */
	class DummyViewer
	{
	public:
		DummyViewer(std::shared_ptr<Ws::MergedPreferences>) {}

		template <typename EventType>
		void Update(const EventType&) {};
	};

	extern template class ViewerNode<DummyViewer, DummySubject>;

	/**
	 * General purpose viewer node that doesn't represent a viewer itself in the tree of viewers,
	 * but still it can have a number of children.
	 *
	 * If it is derived by your own class, it can own a number of subjects that
	 * are observed by its children also defined in the derived class.
	 */
	using DummyViewerNode = ViewerNode<DummyViewer, DummySubject>;

} // namespace Viewer
} // namespace SimShell

#endif
