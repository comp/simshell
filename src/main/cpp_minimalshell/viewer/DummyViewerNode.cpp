/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Dummy ViewerNode.
 */

#include "DummyViewerNode.h"

using namespace std;

namespace UA_CoMP {
namespace Util {

	// Explicit instantiation.
	template class Subject<SimShell::Event::DummyEventType, std::weak_ptr<const void>>;

}
}

namespace SimShell {
namespace Viewer {

		// Explicit instantiation.
		template class ViewerNode<DummyViewer, DummySubject>;

} // namespace Viewer
} // namespace SimShell
