#ifndef FACTORY_MINIMAL_PROJECTFACTORY_H_INCLUDED
#define FACTORY_MINIMAL_PROJECTFACTORY_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MinimalProjectFactory.
 */

#include "gui/factory/IFactory.h"
#include "workspace/MinimalWorkspaceFactory.h"
#include "session/MinimalSession.h"

namespace Minimal {

/**
 * @see IFactory
 * @see Project
 */
class MinimalFactory : public SimShell::Gui::IFactory
{
public:
	/// @see SimShell::Gui::IFactory.
	virtual std::shared_ptr<SimShell::Ws::IWorkspaceFactory> CreateWorkspaceFactory() const
	{
		return std::make_shared<MinimalWorkspaceFactory>();
	}

	/// @see SimShell::Gui::IFactory.
	virtual std::string GetAbout() const
	{
		return "<h3>Minimal Shell</h3><p>Part of VirtualLeaf 2.</p>";
	}

	/// @see SimShell::Gui::IFactory.
	virtual std::string GetApplicationName() const
	{
		return "Minimal Shell";
	}

	/// @see SimShell::Gui::IFactory.
	virtual std::string GetOrganizationName() const
	{
		return "VirtualLeaf 2 Consortium";
	}
};

} // end namespace Minimal

#endif
