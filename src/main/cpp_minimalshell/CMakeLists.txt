#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################

#============================================================================
# Build static library (gets used building, no need to install).
#============================================================================
#---------------------------- set variables ---------------------------------
set( LIB	minimalshell )
set( SRC
#---------
	viewer/DummyViewerNode.cpp
#---------
	workspace/MinimalFile.cpp
	workspace/MinimalProject.cpp
	workspace/MinimalWorkspace.cpp
	workspace/MinimalWorkspaceFactory.cpp	
)

set( MOC_HEADERS
	session/MinimalSession.h
)
set( MOC_OUTFILES )

#------------------------------ build ---------------------------------------
if ( ${VLEAF2_QT_VERSION} EQUAL 4 )
	qt4_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
elseif( ${VLEAF2_QT_VERSION} EQUAL 5 )
	qt5_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
endif()
add_library( ${LIB} ${SRC} ${MOC_OUTFILES} )
if( APPLE )
	set_target_properties( ${LIB}  PROPERTIES INSTALL_NAME_DIR  "@loader_path" )
endif( APPLE )
target_link_libraries( ${LIB} simshell ${LIBS} )

#------------------------------ install -------------------------------------
if (NOT ${CMAKE_PROJECT_NAME}_MAKE_PACKAGE)
	install( TARGETS ${LIB}  DESTINATION  ${LIB_INSTALL_LOCATION} )
else()
	package_target_install( ${LIB} )
endif()

#------------------------------ unset ---------------------------------------
unset( MOC_HEADERS  )
unset( MOC_OUTFILES )
unset( SRC          )
unset( LIB          )

#############################################################################
