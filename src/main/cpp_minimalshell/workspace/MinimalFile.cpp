/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011-2014, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for MinimalFile.
 */

#include "MinimalFile.h"

#include "session/MinimalSession.h"
#include "workspace/MergedPreferences.h"

namespace Minimal {

using namespace std;
using namespace SimShell::Ws;

MinimalFile::MinimalFile(const string& path)
	: m_path(path)
{
}

shared_ptr<SimShell::Session::ISession>
MinimalFile::CreateSession(
	std::shared_ptr<SimShell::Ws::IProject> proj,
	std::shared_ptr<SimShell::Ws::IWorkspace> ws) const
{
	auto prefs = MergedPreferences::Create(ws, proj);
	return make_shared<MinimalSession>(prefs);
}

vector<QAction*> MinimalFile::GetContextMenuActions() const
{
	return {};
}

const string& MinimalFile::GetPath() const
{
	return m_path;
}

const MinimalFile::ConstructorType MinimalFile::Constructor(
	[](const string& filename, const string& path) -> shared_ptr<IFile>
	{
		return make_shared<MinimalFile>(path);
	}
);

} // namespace Minimal
