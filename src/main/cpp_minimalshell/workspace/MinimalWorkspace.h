#ifndef MINIMALWORKSPACE_H_INCLUDED
#define MINIMALWORKSPACE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MinimalWorkspace.
 */

#include "workspace/Workspace.h"
#include "MinimalProject.h"
#include "MinimalWorkspaceFactory.h"

namespace SimShell { namespace Ws {
	extern template class Workspace<Minimal::MinimalProject,
		Minimal::MinimalWorkspaceFactory::g_workspace_index_file>;
}}

namespace Minimal {

class MinimalWorkspace :
	public SimShell::Ws::Workspace<MinimalProject, MinimalWorkspaceFactory::g_workspace_index_file>
{
public:
	MinimalWorkspace(const std::string& path);

	static void Init(const std::string& path);
};

} // namespace Minimal

#endif
