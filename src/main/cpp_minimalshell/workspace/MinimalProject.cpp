/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for MinimalProject.
 */
#include "workspace/Project_def.h"
#include "ptree/PTreeUtils.h"
#include "MinimalProject.h"

namespace SimShell { namespace Ws {
	template class Project<Minimal::MinimalFile, Minimal::MinimalWorkspaceFactory::g_project_index_file>;
}}

namespace Minimal {

MinimalProject::MinimalProject(const std::string& path, const std::shared_ptr<SimShell::Ws::IWorkspace>& w)
	: SimShell::Ws::Project<MinimalFile, MinimalWorkspaceFactory::g_project_index_file>(path, ".minimal_preferences", w)
{
	MinimalWorkspaceFactory f;
	const string default_prefs_file = f.GetWorkspaceTemplatePath() + "/.minimal-preferences";

	ptree default_preferences;
	try {
		read_xml(default_prefs_file, default_preferences, trim_whitespace);
	} catch (xml_parser_error& e) {
		throw Exception("Could not open \"" + default_prefs_file + "\": Exception xml_parser_error: " + e.what());
	}

	try {
		default_preferences = default_preferences.get_child("preferences");
	} catch (ptree_bad_path& e) {
		throw Exception("File \"" + default_prefs_file + "\": Exception ptree_bad_path: " + e.what());
	}

	try {
		// Make sure this workspace's preferences contains all the keys from default-preferences.xml.
		PTreeUtils::CopyStructure(default_preferences, m_preferences, "$WORKSPACE$", {"<xmlcomment>"});
		PTreeUtils::RemoveNonExistingChildren(default_preferences, m_preferences, {"<xmlcomment>"});
	}
	catch (ptree_bad_path &) {
		// No preferences subtree? Not a problem: Copy default preferences quietly.
		m_preferences = default_preferences;
	}

}

const MinimalProject::ConstructorType MinimalProject::Constructor(
	[](const string& type, const string& path, const string& prefs_file, const shared_ptr<SimShell::Ws::IWorkspace>& w) -> std::shared_ptr<SimShell::Ws::IProject>
	{
		if (type == "project") {
			return make_shared<MinimalProject>(path, w);
		} else {
			return nullptr;
		}
	}
);

vector<QAction*> MinimalProject::GetContextMenuActions() const
{
	return {};
}

vector<MinimalProject::WidgetCallback> MinimalProject::GetWidgets()
{
	return {};
}

}
