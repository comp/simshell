/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for MinimalWorkspace.
 */
#include "MinimalWorkspace.h"

#include "MinimalProject.h"
#include "common/FileSys.h"
#include "ptree/PTreeUtils.h"
#include "util/XmlWriterSettings.h"
#include "workspace/Workspace_def.h"

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace UA_CoMP::Util;

namespace SimShell { namespace Ws {
	template class Workspace<Minimal::MinimalProject,
		Minimal::MinimalWorkspaceFactory::g_workspace_index_file>;
}}

namespace Minimal {

MinimalWorkspace::MinimalWorkspace(const string& path)
	: SimShell::Ws::Workspace<MinimalProject, MinimalWorkspaceFactory::g_workspace_index_file>(
		path, ".minimal-preferences")
{
	MinimalWorkspaceFactory f;
	const string default_prefs_file = f.GetWorkspaceTemplatePath() + "/.minimal-preferences";

	ptree default_prefs;
	try {
		read_xml(default_prefs_file, default_prefs, trim_whitespace);
	} catch (xml_parser_error& e) {
		throw Exception("Could not open \"" + default_prefs_file + "\": Exception xml_parser_error: " + e.what());
	}

	try {
		default_prefs = default_prefs.get_child("preferences");
	} catch (ptree_bad_path& e) {
		throw Exception("File \"" + default_prefs_file + "\": Exception ptree_bad_path: " + e.what());
	}

	try {
		// Make sure this workspace's preferences contains all the keys from default-preferences.xml.
		PTreeUtils::CopyNonExistingChildren(default_prefs, m_preferences, {"<xmlcomment>"});
		PTreeUtils::RemoveNonExistingChildren(default_prefs, m_preferences, {"<xmlcomment>"});
	}
	catch (ptree_bad_path &) {
		// No preferences subtree? Not a problem: Copy default preferences quietly.
		m_preferences = default_prefs;
	}
}

void MinimalWorkspace::Init(const string& p)
{
	string path = p;

	// make sure path doesn't end with a '/'
	if (path.length() > 0 && path.rfind("/") == path.length() - 1) {
		path = path.erase(path.length() - 1);
	}
	string index_filename = path + '/' + MinimalWorkspaceFactory::g_workspace_index_file;

	if (QFile::exists(QString::fromStdString(index_filename))) {
		if (!QFile::remove(QString::fromStdString(index_filename))) {
			throw Exception("Unable to overwrite \"" + path + '/' + MinimalWorkspaceFactory::g_workspace_index_file + '"');
		}
	}
	ptree pt_workspace;
	pt_workspace.put("workspace.projects", "");

	try {
		write_xml(index_filename, pt_workspace, std::locale(), XmlWriterSettings::GetTab());
	}
	catch (xml_parser_error& e) {
		throw Exception("Could not initialize workspace \"" + path + '"');
	}
}

} // namespace Minimal
