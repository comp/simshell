/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * MinimalWorkspaceFactory.
 */

#include "common/FileSys.h"
#include "MinimalWorkspaceFactory.h"
#include "MinimalWorkspace.h"
#include "MinimalProject.h"

namespace Minimal {

using namespace std;
using namespace SimShell;
using namespace SimShell::Ws;

shared_ptr<IWorkspace> MinimalWorkspaceFactory::CreateWorkspace(const string& path)
{
	return make_shared<MinimalWorkspace>(path);
}

string MinimalWorkspaceFactory::GetWorkspaceTemplatePath() const
{
	return InstallDirs::GetDataDir() + "/workspace_template";
}

string MinimalWorkspaceFactory::GetDefaultWorkspaceName() const
{
	return "minimal_workspace";
}

void MinimalWorkspaceFactory::InitWorkspace(const string& path)
{
	MinimalWorkspace::Init(path);
}

const string MinimalWorkspaceFactory::g_project_index_file(".minimal-project");

const string MinimalWorkspaceFactory::g_workspace_index_file(".minimal-workspace");

} // namespace Minimal
