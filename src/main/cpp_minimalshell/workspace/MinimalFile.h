#ifndef MINIMALFILE_H_INCLUDED
#define MINIMALFILE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2014, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MinimalFile.
 */
#include "workspace/IFile.h"

namespace Minimal {

class MinimalFile : public SimShell::Ws::IFile
{
public:
	/// Constructor.
	/// @param path  Path to file (in workspace).
	MinimalFile(const std::string& path);

	/// @see SimShell::Ws::IFile::CreateSession()
	virtual std::shared_ptr<SimShell::Session::ISession>
	CreateSession(std::shared_ptr<SimShell::Ws::IProject> proj,
			std::shared_ptr<SimShell::Ws::IWorkspace> ws) const;

	/// @see SimShell::Ws::IFile::GetContextMenuActions()
	virtual std::vector<QAction*> GetContextMenuActions() const;

	/// @see SimShell::Ws::IFile::GetPath()
	virtual const std::string& GetPath() const;

	static const ConstructorType Constructor;

private:
	std::string              m_path;
};

} // namespace Minimal

#endif
