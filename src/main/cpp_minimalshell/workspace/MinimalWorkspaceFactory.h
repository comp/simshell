#ifndef MINIMALWORKSPACEFACTORY_H_INCLUDED
#define MINIMALWORKSPACEFACTORY_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MinimalWorkspaceFactory.
 */

#include "workspace/IWorkspaceFactory.h"

namespace Minimal {

using namespace SimShell::Ws;

class MinimalWorkspaceFactory : public IWorkspaceFactory
{
public:
	/// Virtual destructor.
	virtual ~MinimalWorkspaceFactory() {}

	virtual std::shared_ptr<IWorkspace> CreateWorkspace(const std::string& path);

	virtual std::string GetWorkspaceTemplatePath() const;

	virtual std::string GetDefaultWorkspaceName() const;

	virtual void InitWorkspace(const std::string& path);

	static const std::string                          g_project_index_file;
	static const std::string                          g_workspace_index_file;
};

} // namespace Minimal

#endif
