#ifndef MINIMALPROJECT_H_INCLUDED
#define MINIMALPROJECT_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MinimalProject.
 */

#include "workspace/Project.h"
#include "MinimalFile.h"
#include "MinimalWorkspaceFactory.h"

namespace SimShell { namespace Ws {
	extern template class Project<Minimal::MinimalFile,
		Minimal::MinimalWorkspaceFactory::g_project_index_file>;
}}

namespace Minimal {
	class MinimalProject : public SimShell::Ws::Project<MinimalFile, MinimalWorkspaceFactory::g_project_index_file>
	{
	public:
		MinimalProject(const std::string& path, const std::shared_ptr<SimShell::Ws::IWorkspace>&);

		static const ConstructorType Constructor;

		virtual std::vector<QAction*> GetContextMenuActions() const;

		virtual std::vector<WidgetCallback> GetWidgets();
	};
}

#endif
