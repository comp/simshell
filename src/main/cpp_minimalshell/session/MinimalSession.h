#ifndef MINIMAL_FACTORY_PROJECT_H_INCLUDED
#define MINIMAL_FACTORY_PROJECT_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MinimalProject.
 */

#include <cassert>
#include <sstream>
#include <QTimer>

#include "gui/controller/AppController.h"
#include "session/ISession.h"
#include "viewer/DummyViewerNode.h"

namespace Minimal {

/**
 * For testing purposes.
 * AbstractProject implementation with no viewers, exporters or specific simulation actions.
 * Running the "simulation" causes a timer to count to infinity.
 */
class MinimalSession : public SimShell::Session::ISession
{
	Q_OBJECT;
public:
	MinimalSession(std::shared_ptr<SimShell::Ws::MergedPreferences> p)
		: counter(0), working_dir(), parameters(), preferences(p), steps_limit(0), timer(new QTimer(this))
	{
		connect(timer, SIGNAL(timeout()), this, SLOT(TimeStep()));
	}

	virtual ~MinimalSession() {}

	std::shared_ptr<RootViewerType> CreateRootViewer(SimShell::Gui::Controller::AppController* parent = nullptr) {
		std::shared_ptr<SimShell::Viewer::IViewerNodeWithParent<SimShell::DummySubject>> result
			= std::make_shared<SimShell::Viewer::DummyViewerNode>(preferences->GetChild("viewers.root"));
		result->ParentEnabled(std::make_shared<SimShell::DummySubject>());
		return result;
	}

	virtual void ForceExport() {}

	virtual ExportersType GetExporters() {
		return {};
	}

	virtual SimActionsType GetSimActions() {
		return {};
	}

	virtual const boost::property_tree::ptree& GetParameters() const {
		return parameters;
	}

	virtual void SetParameters(const boost::property_tree::ptree& p) {
		parameters = p;
	}

	virtual Timings GetTimings() const {
		return Timings();
	}

public slots:
	virtual void KillSimulation()
	{
		steps_limit = -1;
		timer->stop();
		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Terminated);
	}

	virtual void StartSimulation(int steps = -1)
	{
		assert(steps >= -1 && "Steps should be positive or -1");

		steps_limit = steps;
		timer->start(0);
		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Started);
	}

	virtual void StopSimulation()
	{
		steps_limit = -1;
		timer->stop();
		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Stopped);
	}

	virtual void TimeStep() {
		if (steps_limit-- == 0) {
			timer->stop();
			emit InfoMessage(GetStatusMessage(), InfoMessageReason::Stopped);
			return;
		}

		counter++;

		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Stepped);
	}

private:
	std::string GetStatusMessage() const {
		std::stringstream s;
		s << "pom pom, counter = " << counter;
		return s.str();
	}

private:
	int counter;
	std::string working_dir;
	boost::property_tree::ptree parameters;
	std::shared_ptr<SimShell::Ws::MergedPreferences> preferences;
	int steps_limit;
	QTimer *timer;
};

} // end namespace Minimal

#endif
