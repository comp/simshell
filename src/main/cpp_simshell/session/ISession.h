#ifndef ISESSION_H_INCLUDED
#define ISESSION_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for ISession.
 */

#include <functional>
#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include <QObject>
#include <QMetaType>

#include "timekeeper/Timeable.h"
#include "viewer/IViewerNode.h"

class QAction;
class QWidget;
Q_DECLARE_METATYPE(std::string)

namespace SimShell {

namespace Gui {namespace Controller {class AppController;}}

namespace Session {

/**
 * Generic session interface.
 * A session is a collection of instances associated with an opened/running project.
 *
 * As this interface is designed to also be implemented by an asynchronously running
 * simulation inside a project, take care to also use it like that. This is why, for
 * example, there a signals for info and errors, rather than return values/getters for
 * the information.
 */
class ISession : public QObject, public UA_CoMP::Timekeeper::Timeable<>
{
	Q_OBJECT
public:
	template <class T>
	using NamedType = std::pair<std::string, T>;

	using SimpleCallbackType = std::function<void()>;
	using ExportCallbackType = std::function<void(const std::string&)>;
	using SimActionsType = std::vector<NamedType<SimpleCallbackType>>   ;

	struct ExporterType {
		std::string         extension;
		ExportCallbackType  callback;
	};

	using ExportersType = std::vector<NamedType<ExporterType>>;
	using RootViewerType = Viewer::IViewerNode  ;

public:
	/**
	 * Virtual destructor for interface class.
	 */
	virtual ~ISession() {}

	/**
	 * Return root viewer, i.e. the viewer that owns all other viewers.
	 */
	virtual std::shared_ptr<RootViewerType>
	CreateRootViewer(Gui::Controller::AppController* parent = nullptr) = 0;

	/**
	 * Force enabled viewers to export.
	 * This is useful when e.g. parameters or preferences are changed and we need a
	 * "checkpoint" in simulation history for this event.
	 */
	virtual void ForceExport() = 0;

	/**
	 * Return list of callbacks for exporting simulation state to a file.
	 */
	virtual ExportersType GetExporters() = 0;

	/**
	 * Get simulation parameters.
	 * @return	const ptree&	A reference to the parameters of the simulation.
	 * 				This reference will stay valid as long as the user
	 * 				of this function doesn't return to the Qt event loop.
	 */
	virtual const boost::property_tree::ptree& GetParameters() const = 0;

	/**
	 * Return list of callbacks for triggering certain simulation-specific actions.
	 */
	virtual SimActionsType GetSimActions() = 0;

	/**
	 * Set simulation parameters.
	 */
	virtual void SetParameters(const boost::property_tree::ptree&) = 0;

	/**
	 * Starts doing time steps until termination or until stopped
	 * @param	steps		The maximum number of steps to simulate (-1 for unlimited)
	 * 				The previous number of steps is overwritten by this
	 */
	virtual void StartSimulation(int steps = -1) = 0;

	/**
	 * Stops taking simulation time steps
	 */
	virtual void StopSimulation() = 0;

	/**
	 * Perform 1 simulation step, bringing simulation into the "next" state.
	 * This method cannot return a result, as the time step may be performed in another thread.
	 * Instead, an InfoMessage or ErrorMessage signal will be emitted.
	 * @throws Exception if no time step could be performed due to other reasons.
	 */
	virtual void TimeStep() = 0;


	/**
	 * Enumeration for the reason of an emitted InfoMessage from the project
	 */
	enum class InfoMessageReason {
		Stepped,	// A time step was done
		Started,	// The simulation has been started
		Stopped,	// The simulation has been stopped
		Terminated	// The simulation terminated (because of the termination condition)
	};

signals:
	/**
	 * Emitted when an event with an accompanying info message has been done/occurred
	 * (Multithreading cannot return bool directly in ProjectBase::TimeStep(), as the
	 * computation is queued for another thread)
	 *
	 * @param	message		The simulation info message
	 * @param	reason		The reason why an info message was emitted
	 */
	void InfoMessage(const std::string &message, const InfoMessageReason &reason);

	/**
	 * Emitted when an error in a time step occurred
	 * (Multithreading cannot return bool directly in ProjectBase::TimeStep(), as the
	 * computation is queued for another thread)
	 *
	 * @param	error		The error message
	 */
	void ErrorMessage(const std::string &error);
};

} // end namespace Session
} // end namespace SimShell

#endif
