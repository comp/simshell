#ifndef WS_IPREFERENCES_H_INCLUDED
#define WS_IPREFERENCES_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for IPreferences.
 */

#include <boost/property_tree/ptree.hpp>

#include "event/PreferencesChanged.h"
#include "util/Subject.h"

namespace SimShell {
namespace Ws {

/**
 * Interface expressing the ability of an object to have a ptree of preferences stored in it.
 */
class IPreferences : public UA_CoMP::Util::Subject<Event::PreferencesChanged, std::weak_ptr<const void>>
{
public:
	virtual ~IPreferences() {}

	virtual const boost::property_tree::ptree& GetPreferences() const = 0;

	virtual void SetPreferences(const boost::property_tree::ptree&) = 0;
};

} // namespace Ws
} // namespace SimShell

#endif
