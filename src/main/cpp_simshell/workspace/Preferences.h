#ifndef WS_PREFERENCES_H_INCLUDED
#define WS_PREFERENCES_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for Preferences.
 */

#include <string>
#include <boost/property_tree/ptree.hpp>
#include "IPreferences.h"

namespace SimShell {
namespace Ws {

/**
 * Implementation of IPreferences.
 *
 * An object that contains a tree of preferences, and reads/writes those from/to a file.
 */
class Preferences : public virtual IPreferences
{
public:
	virtual ~Preferences() {}

	Preferences(const std::string& file);

	Preferences(Preferences&&);

	virtual const boost::property_tree::ptree& GetPreferences() const;

	virtual void SetPreferences(const boost::property_tree::ptree&);

protected:
	boost::property_tree::ptree   m_preferences;

private:
	/// Forbid copy constructor.
	Preferences(const Preferences&);

	/// Forbid copy assignment.
	Preferences& operator=(const Preferences&);

private:
	const std::string             m_file;
};

} // namespace Ws
} // namespace SimShell

#endif
