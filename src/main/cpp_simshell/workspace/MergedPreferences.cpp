/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for MergedPreferences.
 */

#include "MergedPreferences.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Ws {

shared_ptr<MergedPreferences> MergedPreferences::Create(const shared_ptr<Ws::IWorkspace>& w,
                                            const shared_ptr<Ws::IProject>& p)
{
	auto result = shared_ptr<MergedPreferences>(new MergedPreferences(w,p));
	w->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));
	p->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));

	return result;
}

MergedPreferences::MergedPreferences(const shared_ptr<Ws::IWorkspace>& w,
                         const shared_ptr<Ws::IProject>& p)
	: m_path(), m_workspace(w), m_project(p)
{
}

MergedPreferences::MergedPreferences(const boost::property_tree::ptree::path_type& p, const MergedPreferences& other)
	: m_path(p), m_workspace(other.m_workspace), m_project(other.m_project)
{
}

MergedPreferences::~MergedPreferences()
{
}

shared_ptr<MergedPreferences> MergedPreferences::GetChild(const boost::property_tree::ptree::path_type& p) const
{
	auto result = shared_ptr<MergedPreferences>(new MergedPreferences(m_path / p, *this));

	m_workspace->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));
	m_project->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));

	return result;
}

shared_ptr<MergedPreferences> MergedPreferences::GetGlobal() const
{
	boost::property_tree::ptree::path_type empty_path;
	auto result = shared_ptr<MergedPreferences>(new MergedPreferences(empty_path, *this));

	m_workspace->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));
	m_project->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));

	return result;
}

const std::string& MergedPreferences::GetPath() const
{
	return m_project->GetPath();
}

const boost::property_tree::ptree& MergedPreferences::GetProjectPreferencesTree()
{

	return m_project->GetPreferences();

}

void MergedPreferences::ListenPreferencesChanged(const Ws::Event::PreferencesChanged&)
{
	Notify({shared_from_this()});
}

} // namespace Ws
} // namespace SimShell
