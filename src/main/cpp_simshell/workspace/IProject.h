#ifndef WS_IPROJECT_H_INCLUDED
#define WS_IPROJECT_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for IProject.
 */

#include <map>
#include <vector>
#include <boost/property_tree/ptree.hpp>

#include "event/PreferencesChanged.h"
#include "event/ProjectChanged.h"
#include "util/Subject.h"
#include "IWorkspace.h"
#include "IPreferences.h"

class QAction;
class QWidget;

namespace SimShell {

namespace Session {
class ISession;
}

namespace Ws {

class IFile;
class MergedPreferences;

/**
 * Interface for project-like behavior.
 *
 * A project is a container of files, indexed by name.
 * Files can be added/removed.
 *
 * Additionally, a project can be in "closed" or "opened" state.
 * If a project resides in "opened" state, it keeps a session internally.
 *
 * A project has associated with it a tree of preferences and a tree of user data.
 *
 * @see Open Close IPreferences IUserData
 */
class IProject : public virtual IPreferences,
                 public IUserData,
                 public UA_CoMP::Util::Subject<Event::ProjectChanged, std::weak_ptr<const void>>
{
public:
	/**
	 * First argument  : type of project to create.
	 * Second argument : path of project.
	 * Third argument  : preferences file name.
	 * Fourth argument : pointer to workspace to which project belongs.
	 */
	using ConstructorType = std::function<std::shared_ptr<IProject>(
		const std::string&, const std::string&, const std::string&, const std::shared_ptr<IWorkspace>&)>;
	using FileMap = std::map<std::string, std::shared_ptr<IFile>>;
	using FileIterator = typename FileMap::iterator;
	using ConstFileIterator = typename FileMap::const_iterator;

	struct WidgetCallback {
		typedef std::function<std::shared_ptr<QWidget>(std::shared_ptr<MergedPreferences>, QWidget*)> Type;

		std::string    name;     ///< Name of callback
		Type           callback; ///< Callback to create widget. Argument is parent widget, returns created widget.
	};

public:
	/// Virtual destructor.
	virtual ~IProject() {}

	/// Add existing file to project.
	/// @return Iterator pointing to added file.
	/// @throw Exception if no such file.
	virtual FileIterator Add(const std::string& name) = 0;

	/// Get last file.
	/// @return Last file or nullptr if no files in project.
	virtual std::shared_ptr<IFile> Back() = 0;

	/// Get last file.
	/// @return Last file or nullptr if no files in project.
	virtual std::shared_ptr<const IFile> Back() const = 0;

	/// Get iterator pointing to first file.
	virtual FileIterator begin() = 0;

	/// Get iterator pointing to first file.
	virtual ConstFileIterator begin() const = 0;

	/// Get iterator pointing to one position after last file.
	virtual FileIterator end() = 0;

	/// Get iterator pointing to one position after file.
	virtual ConstFileIterator end() const = 0;

	/// Close project.
	/// This only has effect if we are in 'opened' state.
	/// @see IsOpened()
	virtual void Close() = 0;

	/// Get iterator pointing to file with given name.
	/// @return Iterator pointing to file or Project::end() if not found.
	virtual FileIterator Find(const std::string& name) = 0;

	/// Get iterator pointing to file with given name.
	/// @return Iterator pointing to file or Project::end() if not found.
	virtual ConstFileIterator Find(const std::string& name) const = 0;

	/// Get first file.
	/// @return First file or nullptr if no files in project.
	virtual std::shared_ptr<IFile> Front() = 0;

	/// Get first file.
	/// @return First file or nullptr if no files in project.
	virtual std::shared_ptr<const IFile> Front() const = 0;

	/// Get file with given name.
	/// @throw Exception if no such file.
	virtual std::shared_ptr<IFile> Get(const std::string& name) = 0;

	/// Get file with given name.
	/// @throw Exception if no such file.
	virtual std::shared_ptr<const IFile> Get(const std::string& name) const = 0;

	/// Get Qt actions that can be triggered on this project.
	virtual std::vector<QAction*> GetContextMenuActions() const = 0;

	/// Get name of index file for this type of project.
	virtual const std::string& GetIndexFile() const = 0;

	/// Get path project was initialized with.
	virtual const std::string& GetPath() const = 0;

	/// Get session pointer. THIS IS A TEMPORARY HACK.
	virtual std::shared_ptr<Session::ISession> GetSession() const = 0;

	/// Get name of file used to initiate session.
	/// If project is not opened, will return empty string.
	/// @see IsOpened()
	virtual std::string GetSessionFileName() const = 0;

	/// Get map of widget creator callbacks.
	virtual std::vector<WidgetCallback> GetWidgets() = 0;

	/// Test whether file with given name exists in project.
	/// @param name  Name of file to test existence.
	/// @return Whether file with given name exists in project.
	virtual bool IsLeaf(const std::string& name) const = 0;

	/// Test whether project is in 'opened' state.
	/// @return Whether project is in 'opened' state.
	virtual bool IsOpened() const = 0;

	/// Test whether project is currently watching its directory for changes.
	/// Default is true.
	/// @return Whether project is currently watching its directory for changes.
	/// @see SetWatchingDirectory
	virtual bool IsWatchingDirectory() const = 0;

	/// Open most recent file in project.
	virtual void Open() = 0;

	/// Open given file in project.
	/// @param name  File to open.
	virtual void Open(const std::string& name) = 0;

	/// Open given file in project.
	/// @param it  Iterator pointing to file to open.
	virtual void Open(FileIterator it) = 0;

	/// Refresh project.
	virtual void Refresh() = 0;

	/// Remove file with given name.
	/// @param name  Name of file to remove.
	/// @throw Exception if no such file.
	virtual void Remove(const std::string& name) = 0;

	/// Remove file pointed to by iterator.
	/// @param it  Iterator pointing to file to remove.
	virtual void Remove(FileIterator it) = 0;

	/// Get session of opened project.
	/// If project is not opened, will throw exception.
	/// @see IsOpened()
	virtual Session::ISession& Session() const = 0;

	/// Set whether project must watch its directory for changes.
	/// Default is true.
	/// @see IsWatchingDirectory
	virtual void SetWatchingDirectory(bool) = 0;
};

} // namespace Ws
} // namespace SimShell

#endif
