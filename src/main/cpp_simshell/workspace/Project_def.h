#ifndef WS_PROJECT_DEF_H_INCLUDED
#define WS_PROJECT_DEF_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for Project.
 */

#include <sstream>
#include <boost/property_tree/xml_parser.hpp>
#include <QDir>
#include <QFileSystemWatcher>

#include "Project.h"
#include "IFile.h"
#include "util/Exception.h"
#include "util/XmlWriterSettings.h"

namespace SimShell {
namespace Ws {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace UA_CoMP::Util;

template <class FileType, const std::string& index_file>
Project<FileType, index_file>::Project(const string& path,
	const string& prefs_file, const shared_ptr<IWorkspace>& w)
	: Preferences(path + '/' + prefs_file),
	  m_path(path),
	  m_workspace(w),
	  m_filesystem_watcher(path, bind(&Project<FileType, index_file>::Refresh, this))
{
	// scan project directory for files
	QDir qd(QString::fromStdString(m_path));

	QStringList qfilters;
	QFileInfoList l = qd.entryInfoList(qfilters, QDir::Files);
	for (auto it : l) {
		string filename = it.fileName().toStdString();
		auto file = FileType::Constructor(filename, m_path + '/' + filename);
		if (file)
			m_files[filename] = file;
	}

	try {
		ptree p;
		read_xml(m_path + '/' + index_file, p, trim_whitespace);
		m_user_data = p.get_child("project.user_data");
	}
	// Ignore exceptions
	catch (xml_parser_error& e) {}
	catch (ptree_bad_path &) {}

}

template <class FileType, const std::string& index_file>
Project<FileType, index_file>::Project(Project<FileType, index_file>&& other)
	: Preferences(move(other)),
	  m_path(move(other.m_path)),
	  m_files(move(other.m_files)),
	  m_filesystem_watcher(move(other.m_filesystem_watcher))
{
}

template <class FileType, const std::string& index_file>
IProject::FileIterator Project<FileType, index_file>::Add(const string& name)
{
	auto file = FileType::Constructor(name, m_path + '/' + name);
	if (file) {
		FileMap::value_type v(name, file);
		auto result = m_files.insert(v).first;
		Subject<Event::ProjectChanged, std::weak_ptr<const void>>::Notify(
						{Event::ProjectChanged::LeafAdded, name});
		return result;
	} else {
		return m_files.end();
	}
}

template <class FileType, const std::string& index_file>
shared_ptr<IFile> Project<FileType, index_file>::Back()
{
	return (--m_files.end())->second;
}

template <class FileType, const std::string& index_file>
shared_ptr<const IFile> Project<FileType, index_file>::Back() const
{
	return (--m_files.end())->second;
}

template <class FileType, const std::string& index_file>
IProject::FileIterator
Project<FileType, index_file>::begin()
{
	return m_files.begin();
}

template <class FileType, const std::string& index_file>
IProject::ConstFileIterator
Project<FileType, index_file>::begin() const
{
	return m_files.begin();
}

template <class FileType, const std::string& index_file>
void
Project<FileType, index_file>::Close()
{
	if (m_session)
		m_session->StopSimulation();
	m_session.reset();
	m_session_name.clear();
}

template <class FileType, const std::string& index_file>
IProject::FileIterator
Project<FileType, index_file>::end()
{
	return m_files.end();
}

template <class FileType, const std::string& index_file>
IProject::ConstFileIterator
Project<FileType, index_file>::end() const
{
	return m_files.end();
}

template <class FileType, const std::string& index_file>
IProject::FileIterator
Project<FileType, index_file>::Find(const string& name)
{
	return m_files.find(name);
}

template <class FileType, const std::string& index_file>
IProject::ConstFileIterator
Project<FileType, index_file>::Find(const string& name) const
{
	auto it = m_files.begin();
	for (; it != m_files.end(); it++) {
		if (it->first == name)
			break;
	}
	return it;
}

template <class FileType, const std::string& index_file>
shared_ptr<IFile> Project<FileType, index_file>::Front()
{
	return m_files.begin()->second;
}

template <class FileType, const std::string& index_file>
shared_ptr<const IFile> Project<FileType, index_file>::Front() const
{
	return m_files.begin()->second;
}

template <class FileType, const std::string& index_file>
shared_ptr<IFile> Project<FileType, index_file>::Get(const string& name)
{
	auto it = m_files.find(name);
	if (it == m_files.end()) {
		stringstream ss;
		ss << "No file \"" << name << "\" in project \"" << m_path << "\"";
		throw Exception(ss.str());
	}
	return it->second;
}

template <class FileType, const std::string& index_file>
shared_ptr<const IFile> Project<FileType, index_file>::Get(const string& name) const
{
	auto it = m_files.find(name);
	if (it == m_files.end()) {
		stringstream ss;
		ss << "No file \"" << name << "\" in project \"" << m_path << "\"";
		throw Exception(ss.str());
	}
	return it->second;
}

template <class FileType, const std::string& index_file>
const string& Project<FileType, index_file>::GetIndexFile() const
{
	return index_file;
}

template <class FileType, const std::string& index_file>
const string& Project<FileType, index_file>::GetPath() const
{
	return m_path;
}

template <class FileType, const std::string& index_file>
std::shared_ptr<Session::ISession> Project<FileType, index_file>::GetSession() const
{
	if (!m_session)
		throw Exception("Called GetSession() while project was not opened.");
	return m_session;
}

template <class FileType, const std::string& index_file>
string Project<FileType, index_file>::GetSessionFileName() const
{
	return m_session_name;
}

template <class FileType, const std::string& index_file>
const ptree& Project<FileType, index_file>::GetUserData(const string& user) const
{
	auto result_optional = m_user_data.get_child_optional(user);
	if (result_optional) {
		return result_optional.get();
	} else {
		auto& result = m_user_data.put_child(user, ptree());
		return result;
	}
}

template <class FileType, const std::string& index_file>
bool Project<FileType, index_file>::IsLeaf(const string& name) const
{
	return m_files.find(name) != end();
}

template <class FileType, const std::string& index_file>
bool Project<FileType, index_file>::IsOpened() const
{
	return (bool) m_session;
}

template <class FileType, const std::string& index_file>
bool Project<FileType, index_file>::IsWatchingDirectory() const
{
	return m_filesystem_watcher.IsActive();
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::Open()
{
	Open(--end());
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::Open(const std::string& name)
{
	Open(Find(name));
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::Open(FileIterator it)
{
	assert(!IsOpened());
	//auto prefs = GetMergedPreferences();
	m_session = it->second->CreateSession(shared_ptr<IProject>(this, [](IProject*){}), m_workspace);
	m_session_name = it->first;
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::Refresh()
{
	// scan project directory for files
	QDir qd(QString::fromStdString(m_path));
	QStringList filters;
	QFileInfoList l = qd.entryInfoList(filters, QDir::Files);

	// Remove old files.
	vector<string> to_delete(m_files.size());
	for (auto& file : m_files) {
		bool found = false;
		for (auto it : l) {
			if (it.fileName().toStdString() == file.first) {
				found = true;
				break;
			}
		}
		if (!found) {
			to_delete.push_back(file.first);
		}
	}

	for (auto& filename : to_delete) {
		m_files.erase(filename);
		Subject<Event::ProjectChanged, std::weak_ptr<const void>>::Notify(
					{Event::ProjectChanged::LeafRemoved, filename});
	}

	// Add new files.
	for (auto it : l) {
		string filename = it.fileName().toStdString();

		if (m_files.find(filename) == m_files.end()) {
			auto file = FileType::Constructor(filename, m_path + '/' + filename);
			if (file) {
				m_files[filename] = file;
				Subject<Event::ProjectChanged, std::weak_ptr<const void>>::Notify(
							{Event::ProjectChanged::LeafAdded, filename});
			}
		}
	}
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::Remove(const string& name)
{
	auto it = m_files.find(name);
	if (it == m_files.end()) {
		stringstream ss;
		ss << "No file \"" << name << "\" in project \"" << m_path << "\"";
		throw Exception(ss.str());
	}
	Remove(it);
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::Remove(IProject::FileIterator it)
{
	// Iterator will be invalidated after remove, store name.
	string name = it->first;
	QFile::remove(QString::fromStdString(it->second->GetPath()));
	m_files.erase(it);
	Subject<Event::ProjectChanged, std::weak_ptr<const void>>::Notify(
				{Event::ProjectChanged::LeafRemoved, name});
}

template <class FileType, const std::string& index_file>
Session::ISession& Project<FileType, index_file>::Session() const
{
	if (!m_session) {
		throw Exception("Called Session() while project was not opened.");
	}
	return *m_session.get();
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::SetWatchingDirectory(bool a)
{
	m_filesystem_watcher.SetActive(a);
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::SetUserData(const string& user, const ptree& p)
{
	m_user_data.put_child(user, p);
	Store();
}

template <class FileType, const std::string& index_file>
void Project<FileType, index_file>::Store()
{
	ptree pt_project;
	pt_project.put_child("project.user_data", m_user_data);
	write_xml(m_path + '/' + index_file, pt_project, std::locale(), XmlWriterSettings::GetTab());
}

} // namespace Ws
} // namespace SimShell

#endif
