#ifndef WS_EVENT_WORKSPACECHANGED_H_INCLUDED
#define WS_EVENT_WORKSPACECHANGED_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2. VirtualLeaf 2 is free
 *  software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or any later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012 Joeri Exelmans, Jan Broeckhove.
 */
/**
 * @file
 * Definition for WorkspaceChanged.
 */

namespace SimShell {
namespace Ws {
namespace Event {

/**
 * Event used to inform some observer that the workspace has changed.
 *
 * Really a POD but packaged to force users to initialize all data members.
 */
class WorkspaceChanged
{
public:
	enum Type {
		ProjectAdded,
		ProjectRenamed,
		ProjectRemoved
	};

	WorkspaceChanged(Type type, const std::string& name)
		: m_name(name), m_old_name(""), m_type(type) {}

	WorkspaceChanged(const std::string& old_name, const std::string& name)
		: m_name(name), m_old_name(old_name), m_type(ProjectRenamed) {}

	~WorkspaceChanged() {}

	/// Get type.
	Type GetType() const { return m_type;}

	/// In case type is ProjectAdded or ProjectRemoved, get name of project.
	/// In case type is not ProjectAdded and not ProjectRemoved, behavior is undefined.
	const std::string GetName() const
	{
		return (m_type==Type::ProjectAdded || m_type==Type::ProjectRemoved) ? m_name : "";
	}

	/// In case type is ProjectRenamed, get old name of renamed project.
	/// If type is not ProjectRenamed, behavior is undefined.
	const std::string GetOldName() const
	{
		return (m_type==Type::ProjectRenamed) ? m_old_name : "";
	}

	/// In case type is ProjectRenamed, get new name of renamed project.
	/// If type is not ProjectRenamed, behavior is undefined.
	const std::string GetNewName() const
	{
		return (m_type==Type::ProjectRenamed) ? m_name : "";
	}

private:
	std::string    m_name;
	std::string    m_old_name;
	Type           m_type;
};

} // end namespace Event
} // end namespace Ws
} // end namespace SimShell

#endif
