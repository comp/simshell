#ifndef WS_EVENT_PREFERENCESCHANGED_H_INCLUDED
#define WS_EVENT_PREFERENCESCHANGED_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2. VirtualLeaf 2 is free
 *  software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or any later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012 Joeri Exelmans, Jan Broeckhove.
 */
/**
 * @file
 * Definition for PreferencesChanged.
 */

namespace SimShell {
namespace Ws {
namespace Event {

/**
 * Event used to inform some observer that preferences have changed.
 *
 * Really a POD but packaged to force users to initialize all data members.
 */
class PreferencesChanged
{
public:
	PreferencesChanged(const boost::property_tree::ptree& p)
		: m_preferences(p) {}

	/// Get preferences.
	const boost::property_tree::ptree& GetPreferences() const { return m_preferences;}

private:
	const boost::property_tree::ptree&  m_preferences;
};

} // end namespace Event
} // end namespace Ws
} // end namespace SimShell

#endif
