#ifndef WS_WORKSPACE_DEF_H_INCLUDED
#define WS_WORKSPACE_DEF_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for Workspace.
 */

#include <boost/property_tree/xml_parser.hpp>
#include <QDir>

#include "IProject.h"
#include "Workspace.h"
#include "ptree/PTreeUtils.h"
#include "util/Exception.h"
#include "util/XmlWriterSettings.h"

namespace SimShell {
namespace Ws {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace UA_CoMP::Util;

template <class ProjectType, const std::string& index_file>
Workspace<ProjectType, index_file>::Workspace(const string& path,
                                              const std::string& prefs_file)
	: Preferences(path + '/' + prefs_file),
	  m_path((path.length() > 0 && path.rfind('/') == path.length() - 1) ? path.substr(0, path.length()-1) : path),
	  m_prefs_file(prefs_file),
	  m_filesystem_watcher((m_path == "") ? index_file : m_path + '/' + index_file, bind(&Workspace<ProjectType, index_file>::Refresh, this))
{
	const string workspace_file = (m_path == "") ? index_file : m_path + '/' + index_file;

	ptree p;
	try {
		read_xml(workspace_file, p, trim_whitespace);
	} catch (xml_parser_error& e) {
		throw Exception("Could not open \"" + workspace_file + "\": Exception xml_parser_error: " + e.what());
	}

	try {
		m_user_data = p.get_child("workspace.user_data");
	} catch (ptree_bad_path &) {
		// No user_data subtree? Not a problem.
	}

	try {
		auto& projects = p.get_child("workspace.projects"); // may throw

		for (auto& value : projects) {
			auto& project_type = value.first;
			auto& project_name = value.second.data();
			auto  project_path = m_path + '/' + project_name;
			auto project       = ProjectType::Constructor(project_type, project_path,
						m_prefs_file, shared_ptr<IWorkspace>(this, [](IWorkspace *){}));

			if (project) {
				m_projects.insert({project_name, ProjectMapEntry(project, project_type)});
			} else {
				throw Exception("Could not create project of type \"" + project_type + "\".");
			}
		}

	} catch (ptree_bad_path& e) {
		throw Exception("File \"" + workspace_file + "\": Exception ptree_bad_path: " + e.what());
	}
}

template <class ProjectType, const std::string& index_file>
Workspace<ProjectType, index_file>::Workspace(Workspace<ProjectType, index_file>&& other)
	: Preferences(move(other)),
	  m_path(move(other.m_path)),
	  m_projects(move(other.m_projects)),
	  m_filesystem_watcher(move(other.m_filesystem_watcher))
{
}

template <class ProjectType, const std::string& index_file>
const string& Workspace<ProjectType, index_file>::GetIndexFile() const
{
	return index_file;
}

template <class ProjectType, const std::string& index_file>
const string& Workspace<ProjectType, index_file>::GetPath() const
{
	return m_path;
}

template <class ProjectType, const std::string& index_file>
const ptree& Workspace<ProjectType, index_file>::GetUserData(const string& user) const
{
	auto result_optional = m_user_data.get_child_optional(user);
	if (result_optional) {
		return result_optional.get();
	} else {
		auto& result = m_user_data.put_child(user, ptree());
		return result;
	}
}

template <class ProjectType, const std::string& index_file>
void Workspace<ProjectType, index_file>::SetUserData(const string& user, const ptree& p)
{
	m_user_data.put_child(user, p);
	Store();
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ProjectIterator
Workspace<ProjectType, index_file>::begin()
{
	return m_projects.begin();
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ConstProjectIterator
Workspace<ProjectType, index_file>::begin() const
{
	return m_projects.begin();
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ProjectIterator
Workspace<ProjectType, index_file>::end()
{
	return m_projects.end();
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ConstProjectIterator
Workspace<ProjectType, index_file>::end() const
{
	return m_projects.end();
}

template <class ProjectType, const std::string& index_file>
shared_ptr<IProject> Workspace<ProjectType, index_file>::Front()
{
	return begin()->second.Project();
}

template <class ProjectType, const std::string& index_file>
shared_ptr<const IProject> Workspace<ProjectType, index_file>::Front() const
{
	return begin()->second.Project();
}

template <class ProjectType, const std::string& index_file>
shared_ptr<IProject> Workspace<ProjectType, index_file>::Back()
{
	return (--end())->second.Project();
}

template <class ProjectType, const std::string& index_file>
shared_ptr<const IProject> Workspace<ProjectType, index_file>::Back() const
{
	return (--end())->second.Project();
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ProjectIterator
Workspace<ProjectType, index_file>::Find(const string& name)
{
	return m_projects.find(name);
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ConstProjectIterator
Workspace<ProjectType, index_file>::Find(const string& name) const
{
	return m_projects.find(name);
}


template <class ProjectType, const std::string& index_file>
bool
Workspace<ProjectType, index_file>::IsProject(const string& name) const
{
	return m_projects.find(name) != end();
}

template <class ProjectType, const std::string& index_file>
shared_ptr<const IProject> Workspace<ProjectType, index_file>::Get(const string& name) const
{
	auto it = m_projects.find(name);
	if (it == m_projects.end())
		throw Exception("No such project \"" + name + "\" in workspace.");
	return it->second.Project();
}

template <class ProjectType, const std::string& index_file>
shared_ptr<IProject> Workspace<ProjectType, index_file>::Get(const string& name)
{
	auto it = m_projects.find(name);
	if (it == m_projects.end())
		throw Exception("No such project \"" + name + "\" in workspace.");
	return it->second.Project();
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ProjectIterator
Workspace<ProjectType, index_file>::New(const std::string& type, const string& name)
{
	if (m_projects.find(name) != m_projects.end()) {
		throw Exception("Cannot create project \"" + name
			+ "\": Another project with the same name already exists.");
	}

	// create directory for project
	QDir d(QString::fromStdString(m_path));
	if (!d.mkdir(QString::fromStdString(name))) {
		//throw Exception("Could not create directory \"" + m_path + '/' + name + "\".");
	}

	return Add(type, name);
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ProjectIterator
Workspace<ProjectType, index_file>::Add(const std::string& project_type, const string& project_name)
{
	auto it = m_projects.find(project_name);
	if (it != m_projects.end()) {
		return it;
	}
	string project_path = m_path + '/' + project_name;
	auto result = m_projects.end();
	auto project = ProjectType::Constructor(project_type, project_path,
				m_prefs_file, shared_ptr<IWorkspace>(this, [](IWorkspace*){}));
	if (project) {
		result = m_projects.insert({project_name, ProjectMapEntry(project, project_type)}).first;
	} else {
		throw Exception("Could not create project of type \"" + project_type + "\".");
	}

	Store();
	Subject<Event::WorkspaceChanged, std::weak_ptr<const void>>::Notify(
			{Event::WorkspaceChanged::ProjectAdded, project_name});
	return result;
}

template <class ProjectType, const std::string& index_file>
void
Workspace<ProjectType, index_file>::Refresh()
{
	// TODO: refresh preferences?

	const string workspace_file = (m_path == "") ? index_file : m_path + '/' + index_file;

	ptree p;
	try {
		read_xml(workspace_file, p, trim_whitespace);
	} catch (xml_parser_error& e) {
		throw Exception("Could not open \"" + workspace_file + "\": Exception xml_parser_error: " + e.what());
	}

	try {
		auto& projects = p.get_child("workspace.projects"); // may throw

		// Remove old projects.
		vector<string> to_delete(m_projects.size());
		for (auto& value : m_projects) {
			bool found = false;
			for (auto& compare : projects) {
				if (compare.second.data() == value.first) {
					found = true;
					break;
				}
			}
			if (!found) {
				to_delete.push_back(value.first);
			}
		}
		for (auto& filename : to_delete) {
			m_projects.erase(filename);
			Subject<Event::WorkspaceChanged, std::weak_ptr<const void>>::Notify({Event::WorkspaceChanged::Type::ProjectRemoved, filename});
		}

		// Add new projects.
		for (auto& value : projects) {
			const string project_type = value.first;
			const string project_name = value.second.data();
			const string project_path = m_path + '/' + project_name;

			// Check if project already exists.
			auto existing_project_it = m_projects.find(project_name);
			if (existing_project_it == m_projects.end()) {
				// New project.
				auto project = ProjectType::Constructor(project_type, project_path,
					m_prefs_file, shared_ptr<IWorkspace>(this, [](IWorkspace*){}));
				if (project) {
					m_projects.insert({project_name, ProjectMapEntry(project, project_type)});
					Subject<Event::WorkspaceChanged, std::weak_ptr<const void>>::Notify({Event::WorkspaceChanged::Type::ProjectAdded, project_name});
				} else {
					throw Exception("Could not create project of type \"" + project_type + "\".");
				}
			} else {
				// Refresh Existing project.
				existing_project_it->second->Refresh();
			}
		}
	} catch (ptree_bad_path& e) {
		throw Exception("File \"" + workspace_file + "\": Exception ptree_bad_path: " + e.what());
	}
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ProjectIterator
Workspace<ProjectType, index_file>::Rename(const string& old_name, const string& new_name)
{
	auto it = m_projects.find(old_name);
	if (it == m_projects.end())
		throw Exception("No such project \"" + old_name + "\".");
	return Rename(it, new_name);
}

template <class ProjectType, const std::string& index_file>
IWorkspace::ProjectIterator
Workspace<ProjectType, index_file>::Rename(ProjectIterator it, const string& new_name)
{
	// Iterator will be invalidated after rename, store name.
	const string old_name = it->first;
	const string new_path = m_path + '/' + new_name;
	QDir dir;
	if (!dir.rename(QString::fromStdString(it->second->GetPath()), QString::fromStdString(new_path))) {
		throw Exception("Could not move directory \"" + it->second->GetPath() + "\" to \"" + new_path + "\".");
	}
	auto project_type = it->second.Type();
	m_projects.erase(it);
	ProjectMap::value_type v(new_name, ProjectMapEntry((ProjectType::Constructor)(project_type,
		new_path, m_prefs_file, shared_ptr<IWorkspace>(this, [](IWorkspace *){})), project_type));
	auto result = m_projects.insert(v);
	Store();
	Subject<Event::WorkspaceChanged, std::weak_ptr<const void>>::Notify({old_name, new_name});
	return result.first;
}

template <class ProjectType, const std::string& index_file>
void Workspace<ProjectType, index_file>::Remove(const string& name)
{
	auto it = m_projects.find(name);
	if (it == m_projects.end())
		throw Exception("No such project \"" + name + "\".");
	Remove(it);
}

template <class ProjectType, const std::string& index_file>
void Workspace<ProjectType, index_file>::Remove(ProjectIterator it)
{
	// Iterator will be invalidated after remove, store name.
	string name = it->first;

	QString dirname(QString::fromStdString(m_path + '/' + it->first));

	// Disable filesystem watcher for project.
	// If we don't do this, all files being removed in the following
	// step will cause unnecessary updates for the project, potentially
	// even leading to a segfault (because the filesystem watcher runs
	// in a separate thread and may cause updates when the project is
	// already removed)
	it->second.Project()->SetWatchingDirectory(false);

	// Next, remove all other files from project directory
	// TODO: Use Qt5 QDir::removeRecursively()
	function<bool(QString&)> remove;
	remove = [&remove](const QString& dirname)->bool {
		bool result = true;
		QDir d(dirname);
		if (d.exists(dirname)) {
			for (QFileInfo info : d.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
				if (info.isDir()) {
					QString dirname = info.absoluteFilePath();
					result = remove(dirname);
				}
				else {
					result = QFile::remove(info.absoluteFilePath());
				}
				if (!result) {
					return result;
				}
			}
			result = d.rmdir(dirname);
		}
		return result;
	};
	if (!remove(dirname))
		throw Exception("Could not remove directory \"" + m_path + '/' + it->first + "\".");
	m_projects.erase(it);
	Store();
	Subject<Event::WorkspaceChanged, std::weak_ptr<const void>>::Notify({Event::WorkspaceChanged::ProjectRemoved, name});
}

template <class ProjectType, const std::string& index_file>
void Workspace<ProjectType, index_file>::Store() const
{
	ptree pt_workspace;
	pt_workspace.put("workspace.projects", "");
	for (auto& project : *this) {
		pt_workspace.add("workspace.projects.project", project.first);
	}
	pt_workspace.put_child("workspace.user_data", m_user_data);
	write_xml(m_path + '/' + index_file, pt_workspace, std::locale(), XmlWriterSettings::GetTab());
}

} // namespace Ws
} // namespace SimShell

#endif
