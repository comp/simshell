#ifndef WS_IFILE_H_INCLUDED
#define WS_IFILE_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for IFile.
 */

#include <memory>
#include <string>
#include "session/ISession.h"
#include "MergedPreferences.h"

class QAction;

namespace SimShell {
namespace Ws {

/**
 * Interface for files in a project in a workspace.
 *
 * A file can serve as a starting point for a session (=opened state of a project).
 *
 * A file can also specify actions specific for its subtype.
 */
class IFile
{
public:
	/**
	 * First argument: filename.
	 * Second argument: path of file, including filename.
	 */
	using ConstructorType = std::function<std::shared_ptr<IFile>(const std::string&, const std::string&)>;

public:
	/// Virtual destructor.
	virtual ~IFile() {}

	/// Create simulation entities from file.
	virtual std::shared_ptr<Session::ISession>
	CreateSession(
		std::shared_ptr<SimShell::Ws::IProject> proj,
		std::shared_ptr<SimShell::Ws::IWorkspace> ws) const = 0;

	/// Get Qt actions that can be triggered on this type of file.
	virtual std::vector<QAction*> GetContextMenuActions() const = 0;

	/// Get file path.
	virtual const std::string& GetPath() const = 0;
};

} // namespace Ws
} // namespace SimShell

#endif
