#ifndef WS_MERGED_PREFERENCES_H_INCLUDED
#define WS_MERGED_PREFERENCES_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MergedPreferences.
 */

#include <type_traits>
#include <boost/property_tree/ptree.hpp>

#include "util/Subject.h"
#include "workspace/IWorkspace.h"
#include "workspace/IProject.h"

namespace SimShell
{
namespace Ws
{

class MergedPreferences;

namespace Event {
	struct MergedPreferencesChanged {
		const std::shared_ptr<MergedPreferences> source;
	};
}

/// Wrapper around workspace to have read and write access to merged (workspace + project) preferences.
///
/// Translates workspace + project preferences into merged preferences,
/// Listens for Ws::Event::PreferencesChanged events (coming from IWorskpace or IProject)
/// and translates them into (by being a subject of) Event::PreferencesChanged events.
///
/// This way, ISession and its viewers can read from and write to preferences without
/// being exposed to workspace details.
class MergedPreferences : public UA_CoMP::Util::Subject<Event::MergedPreferencesChanged, std::weak_ptr<const void>>,
                    public std::enable_shared_from_this<MergedPreferences>
{
public:
	static std::shared_ptr<MergedPreferences> Create(const std::shared_ptr<Ws::IWorkspace>&,
	                                           const std::shared_ptr<Ws::IProject>&);

	virtual ~MergedPreferences();

	/// @throw ptree_bad_path if node was not found in either project or workspace preferences.
	/// @throw ptree_bad_data if no conversion to type T. (see boost ptree doc for ptree::put_value)
	template <typename T>
	typename std::enable_if<std::is_same<T, std::string>::value, T>::type
	Get(const boost::property_tree::ptree::path_type&) const;

	/// @throw ptree_bad_path if node was not found in either project or workspace preferences.
	/// @throw ptree_bad_data if no conversion to type T. (see boost ptree doc for ptree::put_value)
	template <typename T>
	typename std::enable_if<!std::is_same<T, std::string>::value, T>::type
	Get(const boost::property_tree::ptree::path_type&) const;

	/// @throw ptree_bad_data if no conversion to type T. (see boost ptree doc for ptree::put_value)
	template <typename T>
	typename std::enable_if<std::is_same<T, std::string>::value, T>::type
	Get(const boost::property_tree::ptree::path_type&, const T&) const;

	/// @throw ptree_bad_data if no conversion to type T. (see boost ptree doc for ptree::put_value)
	template <typename T>
	typename std::enable_if<!std::is_same<T, std::string>::value, T>::type
	Get(const boost::property_tree::ptree::path_type&, const T&) const;

	/// Put value at given path in project preferences if another value at the same path exists,
	/// otherwise, put value in workspace preferences.
	/// This operation is rather slow since it also causes the internal workspace or project
	/// to write preferences to disk.
	/// @throw ptree_bad_data if no conversion to type T. (see boost ptree doc for ptree::put_value)
	template <typename T>
	void Put(const boost::property_tree::ptree::path_type&, const T&);

	/// Get MergedPreferences instance for working with preferences subtree.
	std::shared_ptr<MergedPreferences> GetChild(const boost::property_tree::ptree::path_type&) const;

	/// Get MergedPreferences instance pointing to global preferences. (undoes GetChild() operations)
	std::shared_ptr<MergedPreferences> GetGlobal() const;

	/// Get path of project.
	const std::string& GetPath() const;

	/// Get project preferences.
	const boost::property_tree::ptree& GetProjectPreferencesTree();

private:
	MergedPreferences(const std::shared_ptr<Ws::IWorkspace>&,
	            const std::shared_ptr<Ws::IProject>&);

	MergedPreferences(const boost::property_tree::ptree::path_type&, const MergedPreferences&);

	void ListenPreferencesChanged(const Ws::Event::PreferencesChanged&);

	boost::property_tree::ptree::path_type        m_path;
	std::shared_ptr<Ws::IWorkspace>               m_workspace;
	std::shared_ptr<Ws::IProject>                 m_project;
};

template <typename T>
typename std::enable_if<std::is_same<T, std::string>::value, T>::type
MergedPreferences::Get(const boost::property_tree::ptree::path_type& p) const
{
	auto str = m_project->GetPreferences().get<std::string>(m_path / p); // may throw ptree_bad_path
	if (str == "$WORKSPACE$") {
		return m_workspace->GetPreferences().get<T>(m_path / p); // may throw either ptree_bad_path or ptree_bad_data but probably won't
	} else {
		return str;
	}
}

template <typename T>
typename std::enable_if<!std::is_same<T, std::string>::value, T>::type
MergedPreferences::Get(const boost::property_tree::ptree::path_type& p) const
{
	auto str = m_project->GetPreferences().get<std::string>(m_path / p); // may throw ptree_bad_path
	if (str == "$WORKSPACE$") {
		return m_workspace->GetPreferences().get<T>(m_path / p); // may throw either ptree_bad_path or ptree_bad_data but probably won't
	} else {
		return m_project->GetPreferences().get<T>(m_path / p); // may throw ptree_bad_data
	}
}

template <typename T>
typename std::enable_if<std::is_same<T, std::string>::value, T>::type
MergedPreferences::Get(const boost::property_tree::ptree::path_type& p, const T& v) const
{
	auto str_optional = m_project->GetPreferences().get_optional<std::string>(m_path / p); // won't throw
	if (str_optional) {
		auto& str = str_optional.get();
		if (str == "$WORKSPACE$") {
			return m_workspace->GetPreferences().get<T>(m_path / p); // may throw either ptree_bad_path or ptree_bad_data but probably won't
		} else {
			return str;
		}
	} else {
		return v;
	}
}

template <typename T>
typename std::enable_if<!std::is_same<T, std::string>::value, T>::type
MergedPreferences::Get(const boost::property_tree::ptree::path_type& p, const T& v) const
{
	auto str_optional = m_project->GetPreferences().get_optional<std::string>(m_path / p); // won't throw
	if (str_optional) {
		auto& str = str_optional.get();
		if (str == "$WORKSPACE$") {
			return m_workspace->GetPreferences().get<T>(m_path / p); // may throw either ptree_bad_path or ptree_bad_data but probably won't
		} else {
			return m_project->GetPreferences().get<T>(m_path / p); // may throw ptree_bad_data
		}
	} else {
		return v;
	}
}

template <typename T>
inline void MergedPreferences::Put(const boost::property_tree::ptree::path_type& p, const T& v)
{
	auto prefs = m_project->GetPreferences(); // copy
	prefs.put<T>(m_path / p, v);
	m_project->SetPreferences(prefs);
}

} // namespace Ws
} // namespace SimShell

#endif
