#ifndef WS_IWORKSPACEFACTORY_H_INCLUDED
#define WS_IWORKSPACEFACTORY_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for IWorkspaceFactory.
 */
#include "IWorkspace.h"

namespace SimShell {
namespace Ws {

/**
 * Interface for factories creating an application-specific workspace.
 */
class IWorkspaceFactory
{
public:
	/// Virtual destructor.
	virtual ~IWorkspaceFactory() {}

	/// Get path to template workspace.
	/// The template workspace is a workspace containing example projects.
	virtual std::string GetWorkspaceTemplatePath() const = 0;

	/// Get default workspace name.
	/// This string will determine the default path shown in the "Switch Workspace..."-wizard:
	/// <user's home dir/<default workspace name>.
	virtual std::string GetDefaultWorkspaceName() const = 0;

	/// Open workspace with given path and return interface to it.
	virtual std::shared_ptr<IWorkspace> CreateWorkspace(const std::string& path) = 0;

	/// Initialize empty workspace at given path.
	virtual void InitWorkspace(const std::string& path) = 0;
};

} // namespace Ws
} // namespace SimShell

#endif
