#ifndef WS_WORKSPACE_H_INCLUDED
#define WS_WORKSPACE_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for Workspace.
 */
#include <functional>
#include "IWorkspace.h"
#include "Preferences.h"
#include "util/FileSystemWatcher.h"

namespace SimShell {
namespace Ws {

/**
 * Abstraction of workspace on file system.
 *
 *  * Template parameters:
 * 	- ProjectType: Any class that has a public static data member named 'Constructor' of (function) type IProject::ConstructorType in it.
 * 		The 'Constructor' function should return any IProject implementation based on the project type string (passed as argument).
 * 	- index_file: String containing the filename of the workspace index file. Possibly a hidden file to discourage manual editing.
 * 		The workspace index file is a file containing a list of projects, and the workspace user data (see IUserData).
 *
 */
template <class ProjectType, const std::string& index_file>
class Workspace : public IWorkspace, public Preferences
{
public:
	/// Constructor.
	/// @param path      Path of work space to initialize model with.
	/// @throw Exception if given path is not a valid workspace.
	Workspace(const std::string& path,
	          const std::string& prefs_file);

	/// Move constructor.
	Workspace(Workspace<ProjectType, index_file>&&);

	/// Virtual destructor.
	virtual ~Workspace() {}

	/// @see IWorkspace
	virtual ProjectIterator begin();

	/// @see IWorkspace
	virtual ConstProjectIterator begin() const;

	/// @see IWorkspace
	virtual ProjectIterator end();

	/// @see IWorkspace
	virtual ConstProjectIterator end() const;

	/// @see IWorkspace
	virtual ProjectIterator Add(const std::string& type, const std::string& name);

	/// @see IWorkspace
	virtual std::shared_ptr<IProject> Back();

	/// @see IWorkspace
	virtual std::shared_ptr<const IProject> Back() const;

	/// @see IWorkspace
	virtual ProjectIterator Find(const std::string& name);

	/// @see IWorkspace
	virtual ConstProjectIterator Find(const std::string& name) const;

	/// @see IWorkspace
	virtual bool IsProject(const std::string& name) const;

	/// @see IWorkspace
	virtual std::shared_ptr<IProject> Front();

	/// @see IWorkspace
	virtual std::shared_ptr<const IProject> Front() const;

	/// @see IWorkspace
	virtual std::shared_ptr<IProject> Get(const std::string& name);

	/// @see IWorkspace
	virtual std::shared_ptr<const IProject> Get(const std::string& name) const;

	/// @see IWorkspace
	virtual const std::string& GetIndexFile() const;

	/// @see IWorkspace
	virtual const std::string& GetPath() const;

	/// @see IWorkspace
	virtual const boost::property_tree::ptree& GetUserData(const std::string& user) const;

	/// @see IWorkspace
	virtual ProjectIterator New(const std::string& type, const std::string& name);

	/// @see IWorkspace
	virtual void Refresh();

	/// @see IWorkspace
	virtual ProjectIterator Rename(ProjectIterator, const std::string& new_name);

	/// @see IWorkspace
	virtual ProjectIterator Rename(const std::string& old_name, const std::string& new_name);

	/// @see IWorkspace
	virtual void Remove(ProjectIterator);

	/// @see IWorkspace
	virtual void Remove(const std::string& name);

	/// @see IWorkspace
	virtual void SetUserData(const std::string& user, const boost::property_tree::ptree&);

private:
	/// Forbid copy constructor.
	Workspace(const Workspace<ProjectType, index_file>&);

	/// Forbid copy assignment.
	Workspace<ProjectType, index_file>& operator=(const Workspace<ProjectType, index_file>&);

	/// Write state (list of projects and preferences)
	/// to index_file in workspace directory.
	void Store() const;

protected:
	std::string                           m_path;        ///< Path workspace was initialized with.
	std::string                           m_prefs_file;  ///< Name of preferences file.
	ProjectMap                            m_projects;    ///< Mapping from project name to project.
	mutable boost::property_tree::ptree   m_user_data;   ///< GetUserData() with non-existing argument creates an empty user data.
	Util::FileSystemWatcher               m_filesystem_watcher; ///< Watch workspace index file for changes.
};

} // namespace Ws
} // namespace SimShell

#endif
