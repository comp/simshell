#ifndef WS_IUSERDATE_H_INCLUDED
#define WS_IUSERDATE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for IUserData.
 */

namespace SimShell {
namespace Ws {

/**
 * Interface that expresses the ability to have user data, i.e. any data
 * that one wishes to be stored for a lifetime larger than the application's.
 */
class IUserData
{
public:
	virtual ~IUserData() {}

	virtual const boost::property_tree::ptree& GetUserData(const std::string& user) const = 0;

	virtual void SetUserData(const std::string& user, const boost::property_tree::ptree&) = 0;
};

} // namespace Ws
} // namespace SimShell

#endif
