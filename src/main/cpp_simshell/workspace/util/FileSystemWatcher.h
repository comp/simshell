#ifndef WS_UTIL_FILESYSTEMWATCHER_H_INCLUDED
#define WS_UTIL_FILESYSTEMWATCHER_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2. VirtualLeaf 2 is free
 *  software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or any later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012 Joeri Exelmans, Jan Broeckhove.
 */
/**
 * @file
 * Definition for FileSystemWatcher.
 */

#include <iostream>
#include <functional>
#include <QFileSystemWatcher>

namespace SimShell {
namespace Ws {
namespace Util {

/**
 * Utility class for being informed about changes to a file/directory on the filesystem.
 *
 * Main purpose is to translate Qt signals to a single callback function, so it can be used by
 * the Workspace/Project template classes. We can't use Qt signals/slots in templates.
 */
class FileSystemWatcher : public QObject
{
	Q_OBJECT
public:
	/**
	 * @param  path      Filesystem path to watch.
	 * @param  callback  Callback function to call whenever something changes at the given path.
	 */
	FileSystemWatcher(const std::string& path, std::function<void()> callback)
		: p(path),
		  c(callback),
		  active(false)
	{
		w.addPath(QString::fromStdString(p));

		SetActive(true);
	}

	/**
	 * Move constructor.
	 */
	FileSystemWatcher(FileSystemWatcher&& other)
		: p(move(other.p)),
		  c(move(other.c)),
		  active(false)
	{
		w.addPath(QString::fromStdString(p));

		SetActive(other.active);
	}

	/**
	 * Get 'active' property, i.e. whether the callback function is called when something changes at the path.
	 * Default for this property is true.
	 * @see SetActive
	 */
	bool IsActive() const
	{
		return active;
	}

	/**
	 * Set 'active' property, i.e. whether the callback function is called when something changes at the path.
	 * Default for this property is true.
	 * @see GetActive
	 */
	void SetActive(bool a)
	{
		if (active != a) {
			if (a) {
				connect(&w, SIGNAL(directoryChanged(const QString&)), this, SLOT(SLOT_Changed(const QString&)));
				connect(&w, SIGNAL(fileChanged(const QString&)), this, SLOT(SLOT_Changed(const QString&)));
			} else {
				disconnect(&w, 0, 0, 0); // disconnect everything connected to w's signals.
			}
			active = a;
		}
	}

private slots:
	void SLOT_Changed(const QString& ) {
		c();
	}

private:
	QFileSystemWatcher      w;
	std::string             p;
	std::function<void()>   c;
	bool                    active;
};

} // namespace Util
} // namespace Ws
} // namespace SimShell

#endif
