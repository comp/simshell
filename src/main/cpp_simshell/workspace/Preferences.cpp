/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for Preferences.
 */

#include "Preferences.h"
#include <boost/property_tree/xml_parser.hpp>
#include "util/XmlWriterSettings.h"

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace UA_CoMP::Util;

namespace SimShell {
namespace Ws {

Preferences::Preferences(const std::string& file)
	: m_file(file)
{
	ptree p;
	try {
		read_xml(m_file, p, trim_whitespace);
		m_preferences = p.get_child("preferences");
	}
	// Ignore errors, if preferences can't be found, leave them empty.
	catch (xml_parser_error&) {}
	catch (ptree_bad_path&) {}
}

Preferences::Preferences(Preferences&& other)
	: m_preferences(move(other.m_preferences)),
	  m_file(move(other.m_file))
{
}

const ptree& Preferences::GetPreferences() const
{
	return m_preferences;
}

void Preferences::SetPreferences(const ptree& prefs)
{
	m_preferences = prefs;
	ptree p;
	p.put_child("preferences", m_preferences);
	write_xml(m_file, p, std::locale(), XmlWriterSettings::GetTab());
	Notify({m_preferences});
}

} // namespace Ws
} // namespace SimShell
