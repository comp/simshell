/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for FileSys class.
 */

#include "FileSys.h"

#include <string>
#include <sstream>
#include <QDir>
#include <QFileInfo>
#include <QString>
#include <QApplication>
#include "util/Exception.h"

using namespace std;
using namespace UA_CoMP::Util;

namespace SimShell {

std::string 	InstallDirs::g_data_dir;
std::string 	InstallDirs::g_root_dir;
std::string 	InstallDirs::g_workspace_dir;
std::string     InstallDirs::g_tests_dir;
bool   		InstallDirs::g_initialized = false;
bool		InstallDirs::g_tests_initialized = false;


inline void InstallDirs::Check()
{
	if (!g_initialized) {
		Initialize();
	}
}

inline void InstallDirs::CheckTests()
{
	Check();
	if (!g_tests_initialized) {
		InitializeTests();
	}
}


void InstallDirs::InitializeTests()
{
	//------- Retrieving Install Tests Dir
	{
		QDir dir(QString::fromStdString(g_root_dir));
		const bool exists  = dir.cd("tests");
		const string path  = dir.absolutePath().toStdString();
		if (!exists) {
			const string msg = path + " does not exist or is not readable!";
			throw Exception("FileSys::GetInstallTestsDir> " + msg);
		}
		g_tests_dir = path;
	}
	g_tests_initialized = true;
}

void InstallDirs::Initialize()
{
	//------- Retrieving Install Root Path
	{
		QDir dir(QApplication::applicationDirPath());

	#if defined(__APPLE__)
		if (dir.dirName() == "MacOS") {
			//VirtualLeaf.app
			//	-Contents		<-Root Path
			//		-MacOS
			//		     -binaries
			dir.cdUp();
		} else
	#endif
		if (dir.dirName().toLower() == "debug" || dir.dirName().toLower() == "release") {
			//x/virtualleaf		<-Root Path
			//	-bin
			//		-release/debug
			//			-binaries

			dir.cdUp();
			dir.cdUp();
		} else 
	#if defined(__WIN32__)
		if (dir.dirName() != "bin") {
				// Do Nothing, binaries in root folder
		} else
	#endif
		{
			//x/virtualleaf		<-Root Path
			//	-bin
			//		-binaries
			dir.cdUp();
		}

		const bool  exists = dir.exists();
		const string path  = dir.absolutePath().toStdString();
		if (!exists) {
			const string msg = path + " does not connect to VirtualLeaf2 install root!";
			throw Exception("FileSys::GetInstallRootDir> " + msg);
		}
		g_root_dir = path;
	}
	//------- Retrieving Install Data Dir
	{
		QDir dir(QString::fromStdString(g_root_dir));
		const bool  exists     = dir.cd("data");
		const string path  = dir.absolutePath().toStdString();
		if (!exists) {
			const string msg = path + " does not exist or is not readable!";
			throw Exception("FileSys::GetInstallDataDir> " + msg);
		}
		g_data_dir = path;
	}
	//------- Retrieving Install Leaf Dir
	{
		QDir dir(QString::fromStdString(g_data_dir));
		const bool exists  = dir.cd("workspace_template");
		const string path  = dir.absolutePath().toStdString();
		if (!exists) {
			const string msg = path + " does not exist or is not readable!";
			throw Exception("FileSys::GetInstallLeafDir> " + msg);
		}
		g_workspace_dir = path;
	}

	g_initialized = true;
}

string InstallDirs::GetDataDir()
{
	Check();
	return g_data_dir;
}

string InstallDirs::GetWorkspaceDir()
{
	Check();
	return g_workspace_dir;
}

string InstallDirs::GetRootDir()
{
	Check();
	return g_root_dir;
}

string InstallDirs::GetTestsDir()
{
	CheckTests();
	return g_tests_dir;
}

} // VirtualLeaf


