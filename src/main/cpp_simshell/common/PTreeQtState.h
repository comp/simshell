#ifndef SIMSHELL_PTREEQTSTATE_H_INCLUDED
#define SIMSHELL_PTREEQTSTATE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeQtState.
 */

#include <map>
#include <boost/property_tree/ptree.hpp>
#include <QMainWindow>

class QDockWidget;
class QTreeView;
class QWidget;

namespace SimShell {

/**
 * Utility for representing state of various Qt widgets as a ptree.
 *
 * I'm not going into detail of what such a ptree looks like, you shouldn't construct one yourself.
 * Just call GetState() and use the result for SetState() at another point in time.
 *
 * @see IHasPTreeState
 */
class PTreeQtState
{
public:
	/**
	 * Get ptree representation of expand/collapsed state of items in given QTreeView.
	 */
	static boost::property_tree::ptree GetTreeViewState(const QTreeView*);

	/**
	 * Set expand/collapsed state of items in given QTreeView as represented by given ptree.
	 */
	static void SetTreeViewState(QTreeView*, const boost::property_tree::ptree&);

	/**
	 * Get ptree representation of typical widget properties (hidden/shown, size, position, ...)
	 */
	static boost::property_tree::ptree GetWidgetState(const QWidget*);

	/**
	 * Set state of given widget as represented by given ptree.
	 */
	static void SetWidgetState(QWidget*, const boost::property_tree::ptree&);

	/**
	 * Get ptree representatoin of dock widget-specific properties.
	 */
	static boost::property_tree::ptree GetDockWidgetState(const QDockWidget*);

	/**
	 * Set state of given dock widget as represented by given ptree.
	 */
	static void SetDockWidgetState(QDockWidget*, const boost::property_tree::ptree&);

	/**
	 * Get DockWidgetArea of dock widget in main window as a ptree.
	 */
	static boost::property_tree::ptree GetDockWidgetArea(const QMainWindow*, QDockWidget*);

	/*
	 * Set DockWidgetArea as a ptree of dock widget in main window.
	 */
	static void SetDockWidgetArea(QMainWindow*, QDockWidget*, const boost::property_tree::ptree&);

private:
	static const std::map<Qt::DockWidgetArea, std::string> g_dock_widget_area_to_string;
	static const std::map<std::string, Qt::DockWidgetArea> g_string_to_dock_widget_area;
};

}

#endif
