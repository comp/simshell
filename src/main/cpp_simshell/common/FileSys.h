#ifndef UTIL_FILE_SYS_H_INCLUDED
#define UTIL_FILE_SYS_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for FileSys.
 */

#include <string>

namespace SimShell {

/**
 * File system related elements for VirtualLeaf.
 */
class InstallDirs {
public:
	/// Utility method: get name of the directory for data files.
	static std::string GetDataDir();

	/// Utility method: get name of the directory for data files.
	static std::string GetWorkspaceDir();

	/// Utility method: get application installation root directory.
	static std::string GetRootDir();

	/// Utility method: get name of working directory for the tests.
	static std::string GetTestsDir();

private:
	/// Check initialization.
	static void Check();

	/// Initialize all paths.
	static void Initialize();

	/// Check tests initialization.
	static void CheckTests();

	/// Initialize test paths.
	static void InitializeTests();


private:
	static std::string 	g_data_dir;
	static bool	  	g_initialized;
	static bool		g_tests_initialized;
	static std::string 	g_workspace_dir;
	static std::string 	g_root_dir;
	static std::string      g_tests_dir;

};

}
#endif // end-of-include-guard
