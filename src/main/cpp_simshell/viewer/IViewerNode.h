#ifndef VIEWER_IVIEWERNODE_H_INCLUDED
#define VIEWER_IVIEWERNODE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for IViewerNode.
 */
#include "event/ViewerEvent.h"
#include "util/Subject.h"

namespace SimShell {
namespace Viewer {

/**
 * Interface for a node in a hierarchical tree of viewers.
 * A viewer node can be enabled and disabled at runtime.
 */
class IViewerNode : public UA_CoMP::Util::Subject<Event::ViewerEvent, std::weak_ptr<const void>>
{
public:
	typedef std::map<std::string, std::shared_ptr<IViewerNode>> ChildrenMap;

	/// Virtual destructor
	virtual ~IViewerNode() {}

	/// Disable node.
	virtual void Disable() = 0;

	/// Enable node.
	virtual void Enable() = 0;

	/// Test whether node is enabled.
	/// Note that an enabled node still won't do anything if its parent is disabled.
	/// @see IViewerNodeWithParent::IsParentEnabled
	virtual bool IsEnabled() const = 0;

	/// Test whether parent node is enabled.
	virtual bool IsParentEnabled() const = 0;

	/// Get iterator pointing to first child.
	virtual ChildrenMap::const_iterator begin() const = 0;

	/// Get iterator pointing to one position after last child.
	virtual ChildrenMap::const_iterator end() const = 0;
};

/**
 * A viewer node with a parent.
 */
template <class SubjectType>
class IViewerNodeWithParent : public IViewerNode
{
public:
	/// Virtual destructor
	virtual ~IViewerNodeWithParent() {}

	/// Enable entire subtree.
	virtual void ParentDisabled() = 0;

	/// Disable entire subtree.
	virtual void ParentEnabled(std::shared_ptr<SubjectType>) = 0;
};

} // namespace Viewer
} // namespace SimShell

#endif
