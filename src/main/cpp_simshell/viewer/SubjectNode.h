#ifndef VIEWER_SUBJECTNODE_H_INCLUDED
#define VIEWER_SUBJECTNODE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for SubjectNode.
 */
#include <iostream>
#include <type_traits>

#include "workspace/MergedPreferences.h"
#include "IViewerNode.h"

namespace SimShell {
namespace Viewer {

/**
 * A viewer node that does not represent a viewer at all.
 * It doesn't create and delete a viewer when being enabled/disabled.
 * It owns a subject (in the observer pattern) with which its children are registered.
 * Usually it serves as a base type for the root of the tree of viewers.
 *
 * Template parameters:
 * 	- SubjectType: The type of the subject this node owns and registers its children nodes with.
 */
template <class SubjectType>
class SubjectNode : public IViewerNode
{
public:
	SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
	            std::shared_ptr<SubjectType> s,
	            IViewerNode::ChildrenMap&& c);

	SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
	            std::shared_ptr<SubjectType> s);

	virtual ~SubjectNode() {}

	virtual void Disable();

	virtual void Enable();

	virtual bool IsEnabled() const;

	virtual bool IsParentEnabled() const;

	virtual IViewerNode::ChildrenMap::const_iterator begin() const;

	virtual IViewerNode::ChildrenMap::const_iterator end() const;

private:
	std::shared_ptr<Ws::MergedPreferences>        m_preferences;
	std::shared_ptr<SubjectType>                  m_subject;
	IViewerNode::ChildrenMap                      m_children;
	bool                                          m_enabled;
};

template <class FakeSubjectType>
class SubjectViewerNodeWrapper : public IViewerNodeWithParent<FakeSubjectType>
{
public:
	SubjectViewerNodeWrapper(std::shared_ptr<IViewerNode> node)
		: m_node(node) {}

	virtual ~SubjectViewerNodeWrapper() {}

	virtual void Disable() { m_node->Disable(); }

	virtual void Enable() { m_node->Enable(); }

	virtual bool IsEnabled() const { return m_node->IsEnabled(); }

	virtual bool IsParentEnabled() const { return m_node->IsParentEnabled(); }

	virtual IViewerNode::ChildrenMap::const_iterator begin() const { return m_node->begin(); }

	virtual IViewerNode::ChildrenMap::const_iterator end() const { return m_node->end(); }

protected:
	/// @see IViewerNodeWithParent
	virtual void ParentDisabled() {}

	/// @see IViewerNodeWithParent
	virtual void ParentEnabled(std::shared_ptr<FakeSubjectType>) {}

private:
	std::shared_ptr<IViewerNode> m_node;
};


template <class SubjectType>
SubjectNode<SubjectType>::SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
                                      std::shared_ptr<SubjectType> s,
                                      IViewerNode::ChildrenMap&& c)
	: m_preferences(p),
	  m_subject(s),
	  m_children(c),
	  m_enabled(m_preferences->Get<bool>("enabled_at_startup"))
{
	if (m_enabled)
		Enable();
}

template <class SubjectType>
SubjectNode<SubjectType>::SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
                                      std::shared_ptr<SubjectType> s)
	: m_preferences(p),
	  m_subject(s),
	  m_enabled(m_preferences->Get<bool>("enabled_at_startup"))
{
	if (m_enabled)
		Enable();
}

template <class SubjectType>
void SubjectNode<SubjectType>::Disable()
{
	// Disable children.
	for (auto& child: m_children) {
		std::static_pointer_cast<IViewerNodeWithParent<SubjectType>>
			(child.second)->ParentDisabled();
	}
	m_enabled = false;
}

template <class SubjectType>
void SubjectNode<SubjectType>::Enable()
{
	m_enabled = true;
	// Enable children.
	for (auto& child: m_children) {
		std::static_pointer_cast<IViewerNodeWithParent<SubjectType>>
			(child.second)->ParentEnabled(m_subject);
	}
}

template <class SubjectType>
bool SubjectNode<SubjectType>::IsEnabled() const
{
	return m_enabled;
}

template <class SubjectType>
bool SubjectNode<SubjectType>::IsParentEnabled() const
{
	return true;
}

template <class SubjectType>
IViewerNode::ChildrenMap::const_iterator SubjectNode<SubjectType>::begin() const
{
	return m_children.begin();
}

template <class SubjectType>
IViewerNode::ChildrenMap::const_iterator SubjectNode<SubjectType>::end() const
{
	return m_children.end();
}

} // namespace Viewer
} // namespace SimShell

#endif
