#ifndef VIEWEREVENT_H_INCLUDED
#define VIEWEREVENT_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011-2014, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for ViewerEvent.
 */

namespace SimShell {
namespace Viewer {

class IViewerNode;

namespace Event {

/**
 * An event transmitted by viewers to notify whether they are enabled or disabled.
 */
class ViewerEvent {
public:
	typedef std::shared_ptr<IViewerNode> Source;

	enum Type { Enabled, Disabled };

	ViewerEvent(Source s, Type t) : m_source(s), m_type(t) {}

	Source GetSource() const { return m_source; }

	Type GetType() const { return m_type; }

private:
	Source   m_source;
	Type     m_type;
};

} // namespace Event
} // namespace Viewer
} // namespace SimShell

#endif
