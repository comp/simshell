/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PTreeMenu.
 */

#include "PTreeMenu.h"

#include <QAction>
#include <QCloseEvent>
#include <QDockWidget>
#include <QSignalMapper>

#include "PTreeEditorWindow.h"

namespace SimShell {
namespace Gui {

using boost::property_tree::ptree;

PTreeMenu::PTreeMenu(QString const& t, QString const& path_prefix, QWidget* parent)
    : QMenu(t, parent), HasUnsavedChanges("Menu: " + t.toStdString()), m_opened(false), m_path(path_prefix)
{
    m_signal_mapper = new QSignalMapper(this);
    connect(m_signal_mapper, SIGNAL(mapped(QString const&)), this, SIGNAL(ItemTriggered(QString const&)));

    AddAllAction();
}

PTreeMenu::~PTreeMenu()
{
}

void PTreeMenu::AddAllAction()
{
    m_action_all = new QAction("All...", this);
    connect(m_action_all, SIGNAL(triggered()), m_signal_mapper, SLOT(map()));
    m_signal_mapper->setMapping(m_action_all, m_path);
    addAction(m_action_all);
}

void PTreeMenu::InternalForceClose()
{
	m_opened = false;

	for (auto action : actions()) {
		m_signal_mapper->removeMappings(action);
	}
	clear(); // also deletes action objects

	AddAllAction();
}

bool PTreeMenu::InternalIsClean() const
{
	return true;
}

bool PTreeMenu::InternalSave()
{
	return true;
}

bool PTreeMenu::IsOpened() const
{
	return m_opened;
}

bool PTreeMenu::OpenPTree(ptree const& pt, int depth)
{
    if (SaveAndClose()) {
	    OpenPTreeInternal(pt, depth);
	    m_opened = true;
	    return true;
    } else {
	    return false;
    }
}

void PTreeMenu::OpenPTreeInternal(boost::property_tree::ptree const& pt, int remaining_depth)
{
    addSeparator();

    // other actions for each subtree
	if (remaining_depth != 0) {
		for (auto const & subtree : pt) {
			if (subtree.second.empty()) {
				continue; // don't display single items in menu
			}
			QString item_name = QString::fromStdString(subtree.first);
			QString item_path = (m_path == "") ? item_name : m_path + '.' + item_name;

			if (remaining_depth > 1 || remaining_depth == -1) {
				PTreeMenu* menu = new PTreeMenu(item_name, item_path, this);
				menu->OpenPTreeInternal(subtree.second,
				                (remaining_depth == -1) ? -1 : remaining_depth - 1);
				addMenu(menu);

				// forward signals all the way up to the root menu
				connect(menu, SIGNAL(ItemTriggered(QString const&)),
						this, SIGNAL(ItemTriggered(QString const&)));
			} else {
				QAction* action = new QAction(item_name + "...", this);
				connect(action, SIGNAL(triggered()), m_signal_mapper, SLOT(map()));
				m_signal_mapper->setMapping(action, item_path);
				addAction(action);
			}
		}
	}
}

} // end of namespace Gui
} // end of namespace SimShell
