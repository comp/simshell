#ifndef PTREE_EDITOR_WINDOW_H_INCLUDED
#define PTREE_EDITOR_WINDOW_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeEditorWindow.
 */

#include <QMainWindow>
#include <boost/property_tree/ptree.hpp>

#include "gui/HasUnsavedChanges.h"
#include "IHasPTreeState.h"

class QAction;
class QCloseEvent;
class QStatusBar;
class QToolBar;

namespace SimShell {
namespace Gui {

class PTreeView;
class PTreeModel;

/**
 * Small editor widget for a Boost Property Tree object.
 * Each manipulation by the user immediately results in an Apply() signal.
 */
class PTreeEditorWindow : public QMainWindow, public HasUnsavedChanges, public IHasPTreeState
{
	Q_OBJECT
public:
	/// @param parent   Parent widget.
	PTreeEditorWindow(QWidget* parent = nullptr);

	/// Virtual destructor.
	virtual ~PTreeEditorWindow() {}

	/**
	 * Get the current edit path.
	 * The edit path is the subtree that is shown in the editor.
	 * The rest of the ptree is hidden.
	 * If this value is empty, the entire ptree is shown.
	 */
	std::string GetEditPath() const;

	/// @see IHasPTreeState::GetPTreeState().
	virtual boost::property_tree::ptree GetPTreeState() const;

	/**
	 * Test whether the "only edit data" option is set.
	 * If true, the user can only edit values in the 'data' column.
	 * The user cannot add/move/remove/rename keys.
	 */
	bool IsOnlyEditData() const;

	/// @see HasUnsavedChanges
	virtual bool IsOpened() const;

	/// Redo last manipulation by the user.
	void Redo();

	/// Reimplemented from QWidget.
	virtual QSize sizeHint() const;

	/**
	 * Set the "only edit values" option.
	 * @see IsOnlyEditValues()
	 */
	void SetOnlyEditData(bool);

	/// @see IHasPTreeState::SetPTreeState().
	virtual void SetPTreeState(const boost::property_tree::ptree&);

	/// Undo last manipulation by the user.
	void Undo();


public slots:
	/// Write changes to ptree object. Clean state is achieved as a side effect.
	void Apply();

	/**
	 * Set subtree to show in editor.
	 * If the edit path is different from the current one,
	 * a dialog asking to apply unapplied changes will be shown.
	 * @return true if path was set successfully
	 */
	bool OpenPath(const QString & edit_path);

	/**
	 * Set ptree to show in editor.
	 * A confirmation dialog is shown if there are unapplied changes.
	 * @return true if we are in opened state.
	 */
	bool OpenPTree(const boost::property_tree::ptree&, QString const& edit_path = "");

signals:
	/// Emitted when the window has information available, for e.g. a statusbar.
	void StatusChanged(const QString&);

	/// Emitted when changes are applied to ptree.
	void ApplyTriggered(const boost::property_tree::ptree& pt);

protected:
	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private:
	boost::property_tree::ptree    m_pt;
	std::string                    m_edit_path;
	bool                           m_only_edit_data;

private:
	PTreeModel*       m_model;               // equal to zero <=> unopened state
	PTreeView*        m_treeview;
	QToolBar*         m_toolbar_main;

	QAction*          m_action_show_toolbar_main;

private:
	static int const  g_column_width;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif
