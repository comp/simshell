/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation of ViewerDockWidget.
 */

#include "ViewerDockWidget.h"

#include <QAction>
#include <QCloseEvent>
#include <QMainWindow>


using namespace std;

namespace SimShell {
namespace Gui {

ViewerDockWidget::ViewerDockWidget(Qt::DockWidgetArea area, const shared_ptr<Ws::MergedPreferences>& p,
		QWidget* parent, std::function<void()> on_close)
	: QDockWidget(parent), m_preferences(p), m_on_close(on_close)
{
	setWindowModality(Qt::NonModal);
	static_cast<QMainWindow*>(parent)->addDockWidget(area, this);

	const auto pos_x   = m_preferences->Get<int>("position.x");
	const auto pos_y   = m_preferences->Get<int>("position.y");
	const auto size_x  = m_preferences->Get<int>("size.x");
	const auto size_y  = m_preferences->Get<int>("size.y");
	setGeometry(pos_x, pos_y, size_x, size_y);
}

ViewerDockWidget::~ViewerDockWidget()
{
	auto g = geometry();
	m_preferences->Put("position.x", g.x());
	m_preferences->Put("position.y", g.y());
	m_preferences->Put("size.x", g.width());
	m_preferences->Put("size.y", g.height());
}

void ViewerDockWidget::closeEvent(QCloseEvent* )
{
	if (m_on_close) {
		m_on_close();
	}
}

} // end namespace Gui
} // end namespace SimShell
