#ifndef NEWPROJECTDIALOG_H_INCLUDED
#define NEWPROJECTDIALOG_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012 Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for NewProjectDialog.
 */

#include <memory>
#include <QComboBox>
#include <QDialog>
#include <QFileDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include "workspace/IWorkspace.h"

namespace SimShell {

namespace Ws {
	class IWorkspaceFactory;
}

namespace Gui {

/**
 * Dialog that asks user about information for setting up a new project.
 *
 * User has possibility to copy and existing project from the workspace template (see Ws::IWorkspaceFactory::GetWorkspaceTemplatePath()),
 * or using an existing file as basis for the new project.
 */
class NewProjectDialog : public QDialog
{
	Q_OBJECT
public:
	NewProjectDialog(const std::shared_ptr<Ws::IWorkspaceFactory>&, QWidget* parent = nullptr);

	/**
	 * Get project name string entered by user.
	 */
	std::string GetProjectName() const;

	/**
	 * Get path of custom file select by user.
	 */
	std::string GetSrcPath() const;

	/**
	 * Get project selected by user.
	 */
	Ws::IWorkspace::ConstProjectIterator GetSrcProject() const;

	/**
	 * Whether user wants to copy an existing project from template workspace or just copy a single leaf file.
	 */
	bool IsCopyProject() const;

private slots:
	/**
	 * Called when user clicks Cancel button.
	 * Sets dialog result to QDialog::Rejected.
	 */
	void Cancel();

	/**
	 * Called when user selected another option for leaf file.
	 * This is used to validate the form input.
	 */
	void FileChanged(bool use_default);

	/**
	 * Called when path to leaf file is changed by user.
	 * This is used to validate the form input.
	 */
	void FileChanged(QString const& filename);

	/**
	 * Called when project name entered in text box changed.
	 * This is used to validate the form input.
	 */
	void ProjectNameChanged(QString const& name);

	/**
	 * Called when user clicks OK button.
	 * Sets dialog result to QDialog::Accepted.
	 */
	void Ok();

	/**
	 * Displays dialog to browse for a leaf file.
	 * Called when user clicks browse button.
	 */
	void ShowBrowseDialog();

private:
	/**
	 * Checks if form input is valid and enables/disables Ok button accordingly.
	 */
	void ValidateForm();

private:
	bool             project_name_ok;
	bool             file_ok;

	std::shared_ptr<Ws::IWorkspace>  m_workspace_model;

	QPushButton*     m_button_cancel;
	QPushButton*     m_button_ok;
	QComboBox*       m_combo_models;
	QFileDialog*     m_dialog;
	QLineEdit*       m_edit_custom;
	QLineEdit*       m_edit_name;
	QRadioButton*    m_radio_custom;
	QRadioButton*    m_radio_default;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif // end_of_include_guard
