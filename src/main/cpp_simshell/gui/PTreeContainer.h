#ifndef PTREE_CONTAINER_H_INCLUDED
#define PTREE_CONTAINER_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeContainer.
 */

#include <QWidget>
#include <boost/property_tree/ptree.hpp>

#include "gui/HasUnsavedChanges.h"
#include "IHasPTreeState.h"

class QDockWidget;

namespace SimShell {
namespace Gui {

class MyDockWidget;
class PTreeEditorWindow;
class PTreeMenu;

/**
 * For a given ptree, constructs PTreeMenu, QDockWidget, PTreeEditorWindow.
 * The ptree menu will reflect the hierarchical structure of the ptree.
 * For each subtree in the ptree, the user can select it in the menu to
 * open it in the editor.
 * By default, the entire ptree is shown in the editor. The editor is
 * encapsulated in a dock widget.
 */
class PTreeContainer : public QWidget, public HasUnsavedChanges, public IHasPTreeState
{
	Q_OBJECT
public:
	PTreeContainer(QString const & title, QWidget* parent = 0);
	virtual ~PTreeContainer();

	/**
	 * Open ptree with given edit path. The edit path is a path pointing to a subtree.
	 * If the edit path is not empty, only the designated subtree will be shown in the editor.
	 */
	bool Open(boost::property_tree::ptree const & pt, QString const & edit_path = "");

	void Undo();
	void Redo();

	/**
	 * Get the associated dock widget, containing the ptree editor.
	 */
	QDockWidget* GetDock() const;

	/**
	 * Get the current edit path.
	 * The edit path is the current subtree that is shown in the associated ptree editor.
	 * If the entire ptree is shown in the editor (=default behavior), the edit path is empty.
	 */
	std::string GetEditPath() const;

	/**
	 * Get the associated menu.
	 */
	PTreeMenu* GetMenu() const;

	/**
	 * If true, the user can't insert, move, delete or edit keys.
	 * This is the same as a read-only property: the user can still edit values in the 'data' column.
	 */
	void SetOnlyEditData(bool);

	/// @see IHasPTreeState::GetPTreeState().
	virtual boost::property_tree::ptree GetPTreeState() const;

	/// @see HasUnsavedChanges
	virtual bool IsOpened() const;

	/// @see IHasPTreeState::SetPTreeState().
	virtual void SetPTreeState(const boost::property_tree::ptree&);

signals:
	void Applied(boost::property_tree::ptree const &);
	void StatusChanged(QString const &);

protected:
	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private slots:
	void OpenPath(QString const & path);
	void RefreshMenu(boost::property_tree::ptree const & pt);

private:
	bool                m_opened;
	MyDockWidget*       m_dock;
	PTreeMenu*          m_menu;
	QString             m_title;
	PTreeEditorWindow*  m_window;
};

} // namespace Gui
} // namespace SimShell

#endif
