/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * EnabledActions implementation.
 */

#include "EnabledActions.h"

#include <QAction>
#include <QMenu>

using namespace std;

namespace SimShell {
namespace Gui {

void EnabledActions::Add(QAction* a)
{
	actions.push_back(a);
}

void EnabledActions::Add(QMenu* m)
{
	menus.push_back(m);
}

void EnabledActions::Set(vector<QAction*> a)
{
	actions = move(a);
}

void EnabledActions::Enable()
{
	for (auto a : actions) a->setEnabled(true);
	for (auto m : menus) m->setEnabled(true);
}

void EnabledActions::Disable()
{
	for (auto a : actions) a->setEnabled(false);
	for (auto m : menus) m->setEnabled(false);
}

} // namespace Gui
} // namespace SimShell
