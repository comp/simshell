#ifndef VIEWER_WINDOW_H_INCLUDED
#define VIEWER_WINDOW_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for ViewerWindow.
 */

#include <functional>
#include <QMainWindow>

#include "workspace/MergedPreferences.h"

namespace SimShell {
namespace Gui {

/**
 * A window containing visual output of a viewer. (e.g. Qt viewer)
 * A callback can be performed when the window is closed.
 */
class ViewerWindow : public QMainWindow
{
	Q_OBJECT
public:
	/**
	 * @param parent    Parent widget.
	 * @param on_close  Callback to be performed when window is closed.
	 */
	ViewerWindow(const std::shared_ptr<Ws::MergedPreferences>&,
	             QWidget* parent = nullptr,
	             std::function<void()> on_close = std::function<void()>());
	virtual ~ViewerWindow();

protected:
	/// overrides QWidget::closeEvent
	/// @see QWidget::closeEvent
	virtual void closeEvent(QCloseEvent*);

	std::shared_ptr<Ws::MergedPreferences>   m_preferences;

private:
	QAction*                                 m_toggle;
	std::function<void()>                    m_on_close;
};

} // end namespace Gui
} // end namespace SimShell

#endif
