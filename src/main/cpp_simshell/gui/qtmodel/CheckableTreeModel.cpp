/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * CheckableTreeModel implementation.
 */

#include "CheckableTreeModel.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

CheckableTreeModel::Item::~Item() {
	for (auto c : children) {
		delete c;
	}
}


CheckableTreeModel::CheckableTreeModel(const ptree& tree, QObject* parent)
	: QAbstractItemModel(parent)
{
	m_root = new Item({nullptr, {}, QString(), false});
	m_root->children.reserve(tree.size());

	function<void(const ptree&, Item*)> work_recursively;
	(work_recursively = [&](const ptree& tree, Item* parent) {
		for (auto it = tree.ordered_begin(); it != tree.not_found(); it++) {
			auto& node = *it;
			auto checked = node.second.get<bool>("checked");
			auto item = new Item({parent, {}, QString::fromStdString(node.first), checked});
			parent->children.push_back(item);

			auto children_optional = node.second.get_child_optional("children");
			if (children_optional) {
				auto& children = children_optional.get();
				item->children.reserve(children.size());
				work_recursively(children, item);
			}
		}
	})(tree, m_root);
}

CheckableTreeModel::~CheckableTreeModel() {
	if (m_root) {
		delete m_root;
		m_root = nullptr;
	}
}

ptree CheckableTreeModel::ToPTree()
{
	function<ptree(Item*)> work_recursive;
	return (work_recursive = [&](Item* root) -> ptree {
		ptree result;
		for (auto c : root->children) {
			ptree child_pt;
			child_pt.put("checked", c->checked);
			child_pt.put_child("children", work_recursive(c));
			result.put_child(c->data.toStdString(), child_pt);
		}
		return result;
	})(m_root);
}

int CheckableTreeModel::columnCount(QModelIndex const&) const {
	return 1;
}

QVariant CheckableTreeModel::data(const QModelIndex& index, int role) const
{
	if (!index.isValid()) {
		return QVariant();
	}

	auto item = static_cast<Item*>(index.internalPointer());

	if (role == Qt::CheckStateRole) {
		return item->checked? Qt::Checked : Qt::Unchecked;
	}

	if (role == Qt::DisplayRole) {
		return item->data;
	}

	return QVariant();
}

Qt::ItemFlags CheckableTreeModel::flags(const QModelIndex& index) const
{
	if (!index.isValid()) {
		return 0;
	}

	return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
}

QVariant CheckableTreeModel::headerData(int , Qt::Orientation , int ) const
{
	return QVariant();
}

QModelIndex CheckableTreeModel::index(int row, int column, QModelIndex const& parent) const
{
	if (!hasIndex(row, column, parent)) {
		return QModelIndex();
	}

	Item* parent_item;
	if (!parent.isValid()) {
		parent_item = m_root;
	} else {
		parent_item = static_cast<Item*>(parent.internalPointer());
	}

	return createIndex(row, column, parent_item->children[row]);
}

QModelIndex CheckableTreeModel::parent(QModelIndex const& index) const
{
	if (!index.isValid()) {
		return QModelIndex();
	}
	Item* child = static_cast<Item*>(index.internalPointer());
	Item* parent = child->parent;
	if (parent != nullptr) {
		Item* grandparent = parent->parent;
		if (grandparent != nullptr) {
			auto it = find(grandparent->children.begin(), grandparent->children.end(), parent);
			return createIndex(it - grandparent->children.begin(), 0, parent);
		}
	}
	return QModelIndex();
}

int CheckableTreeModel::rowCount(QModelIndex const& parent) const
{
	Item* parent_item;
	if (!parent.isValid()) {
		parent_item = m_root;
	} else {
		parent_item = static_cast<Item*>(parent.internalPointer());
	}
	return parent_item->children.size();
}

bool CheckableTreeModel::setData(QModelIndex const& index, const QVariant& value, int role)
{
	if (!index.isValid()) {
		return false;
	}

	Item* item = static_cast<Item*>(index.internalPointer());
	if (role == Qt::CheckStateRole) {
		if (index.column() == 0) {
			item->checked = value.toBool();
		}
		emit dataChanged(index, index);
		return true;
	}
	return false;
}

} // namespace Gui
} // namespace SimShell
