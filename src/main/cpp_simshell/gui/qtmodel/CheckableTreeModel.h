#ifndef GUI_QTMODEL_CHECKABLETREEMODEL_H_INCLUDED
#define GUI_QTMODEL_CHECKABLETREEMODEL_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * CheckableTreeModel header.
 */

#include <vector>
#include <boost/property_tree/ptree.hpp>
#include <QAbstractItemModel>

namespace SimShell {
namespace Gui {

/**
 * A hierarchical tree-like model of checkable items.
 * After construction, the structure remains fixed; The only part of state that can be modified by the user is the checked/unchecked state of items.
 */
class CheckableTreeModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	/**
	 * Create model. The items are constructed from a ptree with the following structure:
	 *
	 * \code
	 * 	<some_item0>
	 * 		<checked>false</checked>
	 * 		<children>
	 * 			<some_item1>
	 * 				<checked>true</checked>
	 * 			</some_item1>
	 * 		</children>
	 * 	</some_item0>
	 * 	<some_item2>
	 * 		<checked>true</checked>
	 * 		<children/>
	 * 	</some_other_item2>
	 * \endcode
	 * The <checked> node must be present for every item, the <children> node is optional.
	 *
	 * At a later moment, the checked/unchecked state of items can be retrieved by the method ToPTree().
	 * The returned ptree will have the same structure, only the (boolean) values in the <checked> nodes may differ.
	 *
	 * @param tree    A Ptree of form discussed above.
	 * @param parent  Qt parent object that will take ownership of this object.
	 * @see ToPTree()
	 * @throw ptree_bad_path if malformed ptree.
	 * @throw ptree_bad_data if malformed ptree.
	 */
	CheckableTreeModel(const boost::property_tree::ptree& tree, QObject* parent = nullptr);

	virtual ~CheckableTreeModel();

	/// @see QAbstractItemModel
	virtual int columnCount(QModelIndex const&) const;

	/// @see QAbstractItemModel
	virtual QVariant data(const QModelIndex& index, int role) const;

	/// @see QAbstractItemModel
	virtual Qt::ItemFlags flags(const QModelIndex& index) const;

	/// @see QAbstractItemModel
	virtual QVariant headerData(int section, Qt::Orientation o, int role) const;

	/// @see QAbstractItemModel
	virtual QModelIndex index(int row, int column, QModelIndex const& parent) const;

	/// @see QAbstractItemModel
	virtual QModelIndex parent(QModelIndex const& index) const;

	/// @see QAbstractItemModel
	virtual int rowCount(QModelIndex const& parent) const;

	/// @see QAbstractItemModel
	virtual bool setData(QModelIndex const& index, const QVariant& value, int role);

	/**
	 * Get checked/unchecked state of items, represented by a ptree of the form discussed in the constructor.
	 * @see CheckableTreeModel()
	 */
	boost::property_tree::ptree ToPTree();

private:
	struct Item {
		~Item();

		Item*               parent;
		std::vector<Item*>  children;
		QString             data;
		bool                checked;
	};

	Item* m_root;
};

} // namespace Gui
} // namespace SimShell

#endif
