#ifndef WORKSPACEQTMODEL_H_INCLUDED
#define WORKSPACEQTMODEL_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for WorkspaceQtModel.
 */

#include <vector>
#include <memory>
#include <QAbstractItemModel>
#include <boost/property_tree/ptree.hpp>
#include "gui/controller/ProjectController.h"
#include "workspace/IWorkspace.h"
#include "workspace/IProject.h"

namespace SimShell {
namespace Ws {
	class IFile;
}

namespace Gui {

using namespace Ws;
using namespace Ws::Event;

/**
 * Abstraction of workspace on the filesystem.
 * Intended to be used with WorkspaceView widget.
 * Once initiated, will 'watch' filesystem for changes.
 *
 * Uses Workspace template classes internally.
 *
 * @see WorkspaceView
 */
class WorkspaceQtModel : public QAbstractItemModel,
                         public std::enable_shared_from_this<WorkspaceQtModel>
{
	Q_OBJECT
public:
	static std::shared_ptr<WorkspaceQtModel>
	Create(const std::shared_ptr<IWorkspace>&,
	       Controller::ProjectController* c,
	       QObject* parent = 0);

	virtual ~WorkspaceQtModel();

	// Implementation of QAbstractItemModel methods.
	virtual int columnCount(QModelIndex const& parent = QModelIndex()) const;
	virtual QVariant data(QModelIndex const& index, int role = Qt::DisplayRole) const;
	virtual Qt::ItemFlags flags(QModelIndex const& index) const;
	virtual QVariant headerData(int section, Qt::Orientation, int role) const;
	virtual QModelIndex index(int row, int column, QModelIndex const& parent = QModelIndex()) const;
	virtual bool insertRow(int row, QModelIndex const& parent = QModelIndex());
	virtual bool insertRows(int row, int count, QModelIndex const& parent = QModelIndex());
	virtual QModelIndex parent(QModelIndex const& index) const;
	virtual bool removeRow(int row, QModelIndex const& parent = QModelIndex());
	virtual bool removeRows(int row, int count, QModelIndex const& parent = QModelIndex());
	virtual int rowCount(QModelIndex const& parent = QModelIndex()) const;

	enum ItemType { RootType, ProjectType, FileType };

	/**
	 * Close a project if opened.
	 * @return true if we are in closed state.
	 */
	bool Close();

	/**
	 * Close project with given index.
	 * @param index  Index of project to close.
	 * @return true if we are in closed state.
	 */
	bool Close(const QModelIndex& index);

	/**
	 * Get additional context menu actions specific to the selected index.
	 * Index must be valid and pointing to a project or file or an error will be thrown.
	 * @param index     VALID index of which to get context menu actions.
	 * @return          List of QActions specific to selected index.
	 * @throw           Exception if index is invalid. See QModelIndex::isValid().
	 */
	std::vector<QAction*> GetContextMenuActions(const QModelIndex& index) const;

	/**
	 * Get name of given index. Index must be valid and pointing to a project or file or
	 * an error will be thrown.
	 * Root is never visible and should not be tested for by external users of this class.
	 * @param index     VALID index of which we want to know the type.
	 * @return          Name of project or file index pointing to.
	 * @throw           Exception if index is invalid. See QModelIndex::isValid().
	 */
	const std::string& GetName(QModelIndex const& index) const;

	/**
	 * Get type of given index. Index must be valid or an error will be thrown.
	 * An item of this model can be of three types: Root, Project and Leaf.
	 * Root is never visible and should not be tested for by external users of this class.
	 * @param index     VALID index of which we want to know the type.
	 * @return          Type of given index.
	 * @throw           Exception if index is invalid. See QModelIndex::isValid().
	 */
	ItemType GetType(QModelIndex const& index) const;

	bool IsOpened(const QModelIndex& index) const;

	bool Open(const QModelIndex& index);

	std::shared_ptr<IWorkspace> Workspace() { return m_workspace; }

private:
	/**
	 * Throws a UA_CoMP::Util::Exception if given path is not a valid workspace.
	 * @param path      Path of work space to initialize model with.
	 * @param c         Project controller to use for opening projects.
	 * @param parent    Parent of window.
	 */
	WorkspaceQtModel(const std::shared_ptr<IWorkspace>&,
	                 Controller::ProjectController* c,
	                 QObject* parent = 0);

	/// Workspace base class already has a mapping from project name to project objects,
	/// but Qt requires us to keep an ordered, randomly accessible container (i.e. vector) of all
	/// displayed items.
	struct Item
	{
		ItemType  type; ///< Item type.
		int       row;  ///< Position of item in list of parent's children.

		// Various constructors initialize different structs in anonymous union.

		Item()
			: type(RootType), row(0), workspace() {}

		Item(int r, const std::string& name, const std::shared_ptr<IProject>& obj)
			: type(ProjectType), row(r), project({name, obj, {}}) {}

		Item(int r, const std::string& name, Item* parent, const std::shared_ptr<IFile>& obj)
			: type(FileType), row(r), file({name, parent, obj}) {}

		struct WS {
			std::vector<Item*>         children;
		};
		struct PR {
			std::string                name;
			std::shared_ptr<IProject>  obj;
			std::vector<Item*>         children;
		};
		struct FI {
			std::string                name;
			Item*                      parent;
			std::shared_ptr<IFile>     obj;
		};
		union {
			WS workspace;
			PR project;
			FI file;
		};

		~Item() {
			// Destructor deletes item's children.
			switch (type) {
			case RootType:
				for (auto child : workspace.children)
					delete child;
				break;
			case ProjectType:
				for (auto child : project.children)
					delete child;
				break;
			default:
				break;
			}
		}
	};


	/// WorkspaceQtModel is an observer of its data member m_workspace.
	void ListenWorkspaceEvent(const Ws::Event::WorkspaceChanged&);

	/// WorkspaceQtModel is an observer of its data member m_workspace.
	void ListenProjectEvent(Item* project_item, const Ws::Event::ProjectChanged&);

	int FindProjectRow(const std::string& name);
	int FindProjectRowInsert(IWorkspace::ConstProjectIterator);
	int FindFileRow(Item* project_item, const std::string& name);
	int FindFileRowInsert(Item* project_item, IProject::ConstFileIterator);

	Item*                           m_root;
	std::shared_ptr<IWorkspace>     m_workspace;

	Controller::ProjectController*  m_project_controller;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif
