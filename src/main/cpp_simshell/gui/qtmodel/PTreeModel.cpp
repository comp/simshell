/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PTreeModel.
 */

#include "PTreeModel.h"

#include <sstream>
#include <QMimeData>
#include <QStringList>
#include <QUndoStack>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "PTreeUndoCommands.h"

namespace SimShell {
namespace Gui {

using boost::property_tree::ptree;

PTreeModel::PTreeModel(ptree const& tree, QObject* parent)
		: QAbstractItemModel(parent)
{
	root = new Item;
	root->Load(tree);
	undo_stack = 0;
	only_edit_data = false;
}

PTreeModel::PTreeModel(const PTreeModel& other)
{
	root               = new Item(*other.root);
	undo_stack         = 0;       // two models shouldn't use the same undo stack!
	only_edit_data     = other.only_edit_data;
}

PTreeModel & PTreeModel::operator=(PTreeModel const& other)
{
	if (this != &other) {
		// copy entire tree structure
		root = new Item(*other.root);
		undo_stack = 0; // two models shouldn't use the same undo stack!
	}
	return *this;
}

PTreeModel::~PTreeModel()
{
}

int PTreeModel::columnCount(QModelIndex const& ) const
{
	return 2;
}

QVariant PTreeModel::data(QModelIndex const& index, int role) const
{
	if (!index.isValid())
		return QVariant();

	Item* item = static_cast<Item*>(index.internalPointer());

	if (role == Qt::DisplayRole || role==Qt::EditRole) {
		switch (index.column()) {
		case 0:
			return item->key;
		case 1:
			return item->data;
		default:
			return QVariant();
		}
	} else if (role == Qt::BackgroundRole) {
		return item->background;
	} else {
		return QVariant();
	}
}

bool PTreeModel::dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column,
                const QModelIndex & parent)
{
	if (only_edit_data) {
		return false;
	}
	if (action == Qt::IgnoreAction) {
		return true;
	}
	if (action == Qt::LinkAction) {
		return false;
	}
	if (!data->hasFormat("text/plain")) {
		return false;
	}
	if (column > 1) {
		return false;
	}

	// always act as if data was dropped in first column
	column = 0;

	if (row == -1) {
		row = rowCount(parent);
	}

	QString text = data->text();

	Item* parent_item;
	if (parent.isValid()) {
		parent_item = static_cast<Item*>(parent.internalPointer());
	} else {
		parent_item = root;
	}

	std::stringstream sstream(text.toStdString());
	ptree pt;
	try {
		boost::property_tree::xml_parser::read_xml(sstream, pt, boost::property_tree::xml_parser::trim_whitespace);
	}
	catch (boost::property_tree::xml_parser::xml_parser_error& e) {
		return false;
	}
	undo_stack->push(new InsertRowsCommand(this, parent_item, parent, row, pt));
	return true;
}

Qt::ItemFlags PTreeModel::flags(QModelIndex const& index) const
{
	if (!index.isValid()) {
		return Qt::ItemIsDropEnabled;
	}

	// only 2nd column items that have no children can be edited
	if (index.column() == 0) {
		if (only_edit_data) {
			return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled;
		} else {
			return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable
					| Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
		}
	} else {
		if (static_cast<Item*>(index.internalPointer())->GetChildrenCount() == 0) {
			return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsDragEnabled;
		} else {
			return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled;
		}
	}
}

QVariant PTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
		if (section == 0) {
			return QVariant(QString("Key"));
		} else if (section == 1) {
			return QVariant(QString("Data"));
		}
	}
	return QVariant();
}

QModelIndex PTreeModel::index(int row, int column, QModelIndex const& parent) const
{
	if (!hasIndex(row, column, parent)) {
		return QModelIndex();
	}

	Item* parent_tree;
	if (!parent.isValid()) {
		parent_tree = root;
	} else {
		parent_tree = static_cast<Item*>(parent.internalPointer());
	}

	Item* child = parent_tree->GetChild(row);

	if (child) {
		return createIndex(row, column, child);
	} else {
		return QModelIndex();
	}
}

bool PTreeModel::insertRow(int row, QModelIndex const& parent)
{
	return insertRows(row, 1, parent);
}

bool PTreeModel::insertRows(int row, int count, QModelIndex const& parent)
{
	if (only_edit_data) {
		return false;
	}

	Item* parent_item;
	if (parent.isValid()) {
		parent_item = static_cast<Item*>(parent.internalPointer());
	} else {
		parent_item = root;
	}

	if (row < 0) {
		return false;
	}

	if (row > parent_item->GetChildrenCount()) {
		return false;
	}

	undo_stack->push(new InsertRowsCommand(this, parent_item, parent, row, count));
	return true;
}

bool PTreeModel::IsOnlyEditData() const
{
	return only_edit_data;
}

QMimeData* PTreeModel::mimeData(const QModelIndexList& indexes) const
{
	QMimeData* mime = new QMimeData();
	QString text;

	for (auto const& index : indexes) {
		if (index.column() == 0) {
			Item* item = static_cast<Item*>(index.internalPointer());
			std::stringstream sstream;
			ptree pt;
			pt.add_child(item->key.toString().toStdString(), item->Store());
			boost::property_tree::xml_parser::write_xml(sstream, pt);
			text = sstream.str().c_str();
		}
	}
	mime->setText(text);
	return mime;
}

bool PTreeModel::MoveRow(int old_row, const QModelIndex & old_parent, int new_row, const QModelIndex & new_parent)
{
	return MoveRows(old_row, old_parent, new_row, new_parent, 1);
}

bool PTreeModel::MoveRows(int old_row, QModelIndex const& old_parent, int new_row, QModelIndex const& new_parent,
                int count)
{
	if (only_edit_data) {
		return false;
	}

	Item* old_parent_item;
	if (old_parent.isValid()) {
		old_parent_item = static_cast<Item*>(old_parent.internalPointer());
	} else {
		old_parent_item = root;
	}

	Item* new_parent_item;
	if (new_parent.isValid()) {
		new_parent_item = static_cast<Item*>(new_parent.internalPointer());
	} else {
		new_parent_item = root;
	}

	if (old_row < 0) {
		return false;
	}
	if (old_row + count > old_parent_item->GetChildrenCount()) {
		return false;
	}
	if (new_row < 0) {
		return false;
	}
	if (new_row + count > new_parent_item->GetChildrenCount()) {
		return false;
	}
	undo_stack->push(new MoveRowsCommand(this, old_parent_item, old_parent, old_row,
	                		new_parent_item, new_parent, new_row, count));
	return true;
}

QStringList PTreeModel::mimeTypes() const
{
	QStringList types;
	types << "text/plain";
	return types;
}

QModelIndex PTreeModel::parent(QModelIndex const& index) const
{
	if (!index.isValid()) {
		return QModelIndex();
	}

	Item* child = static_cast<Item*>(index.internalPointer());
	Item* parent = child->GetParent();
	if (parent == root) {
		return QModelIndex();
	}

	return createIndex(parent->row, 0, parent);
}

bool PTreeModel::removeRow(int row, QModelIndex const& parent)
{
	return removeRows(row, 1, parent);
}

bool PTreeModel::removeRows(int row, int count, QModelIndex const& parent)
{
	if (only_edit_data) {
		return false;
	}

	Item* parent_item;
	if (parent.isValid()) {
		parent_item = static_cast<Item*>(parent.internalPointer());
	} else {
		parent_item = root;
	}

	if (row < 0) {
		return false;
	}

	if (row + count > parent_item->GetChildrenCount()) {
		return false;
	}

	undo_stack->push(new RemoveRowsCommand(this, parent_item, parent, row, count));
	return true;
}

int PTreeModel::rowCount(QModelIndex const& parent) const
{
	Item* item;
	if (parent.column() > 0) {
		return 0;
	}

	if (!parent.isValid()) {
		item = root;
	} else {
		item = static_cast<Item*>(parent.internalPointer());
	}
	return item->GetChildrenCount();
}

bool PTreeModel::setData(QModelIndex const& index, const QVariant& value, int role)
{
	if (!index.isValid()) {
		return false;
	}

	Item* item = static_cast<Item*>(index.internalPointer());
	if (role == Qt::EditRole) {
		if (index.column() == 0) {
			if (only_edit_data) {
				return false;
			}

			if (value.toString().isEmpty()) {
				return false; // changing key to empty string is probably a user mistake and illegal!
			}
			undo_stack->push(new EditKeyCommand(this, item, index, item->key, value));
		} else if (index.column() == 1) {
			undo_stack->push(new EditDataCommand(this, item, index, item->data, value));
		}
		// no need to emit dataChanged() signal, undo command will do this for us
		return true;
	} else if (role == Qt::BackgroundRole) {
		item->background = value;
		// emit dataChanged() for entire row
		QModelIndex key_index = index.sibling(index.row(), 0);
		QModelIndex data_index = index.sibling(index.row(), 1);
		emit dataChanged(key_index, data_index);
		return true;
	} else {
		return false;
	}
}

void PTreeModel::SetOnlyEditData(bool b)
{
	only_edit_data = b;
}

void PTreeModel::SetUndoStack(QUndoStack* s)
{
	undo_stack = s;
}

ptree PTreeModel::Store() const
{
	return root->Store();
}

Qt::DropActions PTreeModel::supportedDragActions() const
{
	if (only_edit_data) {
		return Qt::CopyAction;
	} else {
		return Qt::CopyAction | Qt::MoveAction;
	}
}

Qt::DropActions PTreeModel::supportedDropActions() const
{
    if (only_edit_data) {
        return Qt::CopyAction;
    }
    else {
        return Qt::CopyAction | Qt::MoveAction;
    }
}

// --- PTreeModel::Item implementation ---

PTreeModel::Item::Item(Item* p, QVariant const& k, int r) : key(k), row(r), parent(p) {}

PTreeModel::Item::Item(Item const& other)
		: key(other.key), data(other.data), background(other.background),
		  row(other.row), parent(other.parent)
{
	for (auto const& child : other.children) {
		children.push_back(new Item(*child));
	}
}

PTreeModel::Item& PTreeModel::Item::operator=(Item const& other)
{
	if (this != &other) {
		// forget about our own children
		for (auto const& child : children) {
			delete child;
		}

		parent = other.parent;
		key = other.key;
		data = other.data;
		background = other.background;
		row = other.row;

		for (auto const& child : other.children) {
			children.push_back(new Item(*child));
		}
	}
	return *this;
}

PTreeModel::Item::~Item()
{
	for (auto const& child : children) {
		delete child;
	}
}

PTreeModel::Item* PTreeModel::Item::GetChild(unsigned int i)
{
	if (i < children.size()) {
		return children[i];
	} else {
		return 0;
	}
}

PTreeModel::Item const* PTreeModel::Item::GetChild(unsigned int i) const
{
	if (i < children.size()) {
		return children[i];
	} else {
		return 0;
	}
}

int PTreeModel::Item::GetChildrenCount() const
{
	return children.size();
}

PTreeModel::Item* PTreeModel::Item::GetParent()
{
	return parent;
}

PTreeModel::Item const* PTreeModel::Item::GetParent() const
{
	return parent;
}

void PTreeModel::Item::InsertChild(unsigned int row, Item* item)
{
	children.insert(children.begin() + row, item);

	// correct row numbers of subsequent items
	for (auto it = children.begin() + row; it != children.end(); it++) {
		(*it)->row = it - children.begin();
	}
}

void PTreeModel::Item::Load(ptree const& tree)
{
	for (auto const& child : children) {
		delete child;
	}
	children.clear();

	// load tree contents
	data = QVariant(QString(tree.data().c_str()));
	children.reserve(tree.size());
	for (ptree::value_type const & v : tree) {
		Item* item = new Item(this, QVariant(QString(v.first.c_str())), children.size());
		item->Load(v.second);
		children.push_back(item);
	}
}

void PTreeModel::Item::RemoveChild(unsigned int row)
{
	children.erase(children.begin() + row);

	// correct row numbers of subsequent items
	for (auto it = children.begin() + row; it != children.end(); it++) {
		(*it)->row = it - children.begin();
	}
}

ptree PTreeModel::Item::Store() const
{
	ptree t;

	if (data.type() == QVariant::String) {
		t.data() = ptree::data_type(data.toString().toStdString());
	}
	for (Item* v : children) {
		ptree child_tree = v->Store();
		t.push_back(ptree::value_type(ptree::key_type(v->key.toString().toStdString()), child_tree));
	}
	return ptree(t);
}

} // end of namespace Gui
} // end of namespace SimShell
