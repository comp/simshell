/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for WorkspaceQtModel.
 */

#include "WorkspaceQtModel.h"

#include <functional>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QDir>
#include <QIcon>
#include <QFont>

#include "util/Exception.h"
#include "workspace/IWorkspace.h"
#include "workspace/IProject.h"
#include "workspace/IFile.h"


namespace SimShell {
namespace Gui {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace UA_CoMP::Util;
using namespace Ws;
using namespace Ws::Event;

//--------------------------------------------------------------------------
// QAbstractItemModel methods at the end of this file.
//--------------------------------------------------------------------------

shared_ptr<WorkspaceQtModel> WorkspaceQtModel::Create(const shared_ptr<IWorkspace>& w,
                                                      Controller::ProjectController* c,
                                                      QObject* parent)
{
	auto result = shared_ptr<WorkspaceQtModel>(new WorkspaceQtModel(w, c, parent));

	// Register model with its base class VleafWorkspace.
	auto handler = bind(&WorkspaceQtModel::ListenWorkspaceEvent, result.get(), _1);
	w->Subject<Ws::Event::WorkspaceChanged, std::weak_ptr<const void>>::Register(result, handler);

	// Register model with all projects in its base class VleafWorkspace.
	auto it = result->m_root->workspace.children.begin(); // Children in m_root->workspace.children should be in the same order.
	for (auto& project : *w) {
		auto handler = bind(&WorkspaceQtModel::ListenProjectEvent, result.get(), *it, _1);
		project.second->Subject<Ws::Event::ProjectChanged, std::weak_ptr<const void>>::Register(result, handler);
		it++;
	}

	return result;
}

WorkspaceQtModel::WorkspaceQtModel(const shared_ptr<IWorkspace>& w,
                                   Controller::ProjectController* c,
                                   QObject* parent)
	: QAbstractItemModel(parent),
	  m_workspace(w),
	  m_project_controller(c)
{
	// Construct Item tree from Workspace-inherited protected members.
	m_root = new Item();
	for (auto& project : *m_workspace) {
		auto project_item = new Item(m_root->workspace.children.size(), project.first, project.second.Project());
		for (auto& file : *(project.second)) {
			auto file_item = new Item(project_item->project.children.size(), file.first, project_item, file.second);
			project_item->project.children.push_back(file_item);
		}
		m_root->workspace.children.push_back(project_item);
	}
}

WorkspaceQtModel::~WorkspaceQtModel()
{
	if (m_root) {
		delete m_root;
	}
}

bool WorkspaceQtModel::Close()
{
	return m_project_controller->PromptClose();
}

bool WorkspaceQtModel::Close(const QModelIndex& index)
{
	if (!index.isValid()) {
		throw Exception("WorkspaceQtModel::Close> Invalid index given!");
	}

	if (IsOpened(index)) {
		return m_project_controller->PromptClose();
	}

	return (bool) m_project_controller;
}

void WorkspaceQtModel::ListenWorkspaceEvent(const Ws::Event::WorkspaceChanged& e)
{
	switch (e.GetType())
	{
	case Ws::Event::WorkspaceChanged::Type::ProjectAdded:
	{
		// Get data from event object.
		const string name = e.GetName();

		// Get iterator to added project.
		auto project_it = m_workspace->Find(name);

		// Insert new row.
		int row = FindProjectRowInsert(project_it);
		insertRow(row);

		// Create new project item.
		Item* project_item = new Item(row, project_it->first, project_it->second.Project());
		m_root->workspace.children[row] = project_item;

		// Register added project with this.
		project_it->second->Subject<Ws::Event::ProjectChanged, std::weak_ptr<const void>>::Register(shared_from_this(), bind(&WorkspaceQtModel::ListenProjectEvent, this, project_item, _1));

		// Add children (files) to project item.
		for (auto& file : *(project_it->second)) {
			auto file_item = new Item(project_item->project.children.size(), file.first, project_item, file.second);
			project_item->project.children.push_back(file_item);
		}

		// Emit dataChanged() signal.
		auto index = createIndex(row, 0, project_item);
		emit dataChanged(index, index);

		break;
	}
	case Ws::Event::WorkspaceChanged::Type::ProjectRenamed:
	{
		// Get data from event object.
		const string old_name = e.GetOldName();
		const string new_name = e.GetNewName();

		// No need to unregister this from renamed project: the shared_ptr pointing to it was already reset, destructing the (inherited) subject.

		// Remove old row.
		int old_row = FindProjectRow(old_name);
		removeRow(old_row);

		// Insert new row.
		auto new_it = m_workspace->Find(new_name);
		int new_row = FindProjectRowInsert(new_it);
		insertRow(new_row);

		// Create project item in new row
		Item* new_item = new Item(new_row, new_it->first, new_it->second.Project());
		m_root->workspace.children[new_row] = new_item;

		// Register renamed project with this.
		new_it->second->Subject<Ws::Event::ProjectChanged, std::weak_ptr<const void>>::Register(shared_from_this(), bind(&WorkspaceQtModel::ListenProjectEvent, this, new_item, _1));

		// Add children (files) to project item.
		for (auto& file : *(new_it->second)) {
			auto file_item = new Item(new_item->project.children.size(), file.first, new_item, file.second);
			new_item->project.children.push_back(file_item);
		}

		// Emit dataChanged() signal.
		auto new_index = createIndex(new_row, 0, new_item);
		emit dataChanged(new_index, new_index);

		break;
	}
	case Ws::Event::WorkspaceChanged::Type::ProjectRemoved:
	{
		// Get data from event object.
		string name = e.GetName();

		// No need to unregister this from removed project: the shared_ptr pointing to it was already reset, destructing the (inherited) subject.

		// Remove row.
		int row = FindProjectRow(name);
		removeRow(row);

		break;
	}
	default:
		break;
	}
}

void WorkspaceQtModel::ListenProjectEvent(Item* project_item, const Ws::Event::ProjectChanged& e)
{
	switch (e.GetType())
	{
	case Ws::Event::ProjectChanged::Type::LeafAdded:
	{
		// Get data from event object.
		const string name = e.GetName();

		// Get iterator to added file.
		auto file_it = project_item->project.obj->Find(name);

		// Insert new row.
		int row = FindFileRowInsert(project_item, file_it);
		insertRow(row, index(project_item->row, 0));

		// Create new project item.
		Item* file_item = new Item(row, file_it->first, project_item, file_it->second);
		project_item->project.children[row] = file_item;

		// Emit dataChanged() signal.
		auto index = createIndex(row, 0, file_item);
		emit dataChanged(index, index);

		break;
	}
	case Ws::Event::ProjectChanged::Type::LeafRemoved:
	{
		// Get data from event object.
		const string name = e.GetName();

		// Remove row.
		int row = FindFileRow(project_item, name);
		removeRow(row, index(project_item->row, 0));

		break;
	}
	default:
		break;
	}
}

vector<QAction*> WorkspaceQtModel::GetContextMenuActions(const QModelIndex& index) const
{
	if (!index.isValid()) {
		throw Exception("WorkspaceQtModel::GetContextMenuActions> Invalid index given!");
	}
	Item* item = static_cast<Item*>(index.internalPointer());
	switch (item->type)
	{
	case ProjectType:
		return item->project.obj->GetContextMenuActions();
	case FileType:
		return item->file.obj->GetContextMenuActions();
	default:
		throw Exception("WorkspaceQtModel::GetContextMenuActions() should only be called on items of ProjectType or FileType.");
	}
}

string const& WorkspaceQtModel::GetName(QModelIndex const& index) const
{
	if (!index.isValid()) {
		throw Exception("WorkspaceQtModel::GetName> Invalid index given!");
	}
	Item* item = static_cast<Item*>(index.internalPointer());
	switch (item->type)
	{
	case ProjectType:
		return item->project.name;
	case FileType:
		return item->file.name;
	default:
		throw Exception("WorkspaceQtModel::GetName() should only be called on items of ProjectType or FileType.");
	}
}

WorkspaceQtModel::ItemType WorkspaceQtModel::GetType(QModelIndex const& index) const
{
	if (!index.isValid()) {
		throw Exception("WorkspaceQtModel::GetType> Invalid index given!");
	}
	return static_cast<Item*>(index.internalPointer())->type;
}

int WorkspaceQtModel::FindProjectRow(const string& name)
{
	auto& children = m_root->workspace.children;
	for (auto it = children.begin(); it != children.end(); it++) {
		if ((*it)->project.name == name) {
			return it - children.begin();
		}
	}
	return -1;
}

int WorkspaceQtModel::FindProjectRowInsert(IWorkspace::ConstProjectIterator project_it)
{
	auto next_it = project_it;
	next_it++;
	if (next_it == m_workspace->end()) {
		return m_root->workspace.children.size();
	} else {
		return FindProjectRow(next_it->first);
	}
}

int WorkspaceQtModel::FindFileRow(Item* project_item, const string& name)
{
	auto& children = project_item->project.children;
	for (auto it = children.begin(); it != children.end(); it++) {
		if ((*it)->file.name == name) {
			return it - children.begin();
		}
	}
	return -1;
}

int WorkspaceQtModel::FindFileRowInsert(Item* project_item, IProject::ConstFileIterator file_it)
{
	auto next_it = file_it;
	next_it++;
	if (next_it == project_item->project.obj->end()) {
		return project_item->project.children.size();
	} else {
		return FindFileRow(project_item, next_it->first);
	}
}

bool WorkspaceQtModel::IsOpened(const QModelIndex& index) const
{
	Item* item = static_cast<Item*>(index.internalPointer());
	switch (item->type) {
	case ProjectType:
		if (item->project.obj->IsOpened()) {
			return true;
		}
		break;
	case FileType:
		if (item->file.parent->project.obj->IsOpened()) {
			return true;
		}
		break;
	default:
		break;
	}

	return false;
}

bool WorkspaceQtModel::Open(const QModelIndex& index)
{
	if (!index.isValid()) {
		throw Exception("WorkspaceQtModel::Open> Invalid index given!");
	}

	if (!m_project_controller->PromptClose())
		return false;

	Item* item = static_cast<Item*>(index.internalPointer());
	switch (item->type) {
	case ProjectType:
	{
		Ws::IWorkspace::ProjectIterator it_p = m_workspace->Find(item->project.name);
		auto project = it_p->second.Project();
		project->Open();
		return m_project_controller->Set(item->project.name, project);
	}
	case FileType:
	{
		Item* project_item = item->file.parent;
		Ws::IWorkspace::ProjectIterator it_p = m_workspace->Find(project_item->project.name);
		auto project = it_p->second.Project();
		project->Open(item->file.name);
		return m_project_controller->Set(project_item->project.name, project);
	}
	default:
		return false;
	}
}

//--------------------------------------------------------------------------
// Implementation of QAbstractItemModel methods.
//--------------------------------------------------------------------------

int WorkspaceQtModel::columnCount(QModelIndex const& ) const
{
	return 1;
}

QVariant WorkspaceQtModel::data(QModelIndex const& index, int role) const
{
	if (index.isValid()) {
		Item* item = static_cast<Item*>(index.internalPointer());

		bool opened = IsOpened(index);

		switch (item->type) {
		case ProjectType:
			if (role == Qt::DisplayRole) {
				return QString::fromStdString(item->project.name);
			} else if (role == Qt::DecorationRole) {
				if (opened) {
					return QIcon::fromTheme("folder-open");
				} else {
					return QIcon::fromTheme("folder");
				}
			} else if (role == Qt::FontRole) {
				QFont f;
				if (opened)
					f.setItalic(true);
				return f;
			}
			break;
		case FileType:
			if (role == Qt::DisplayRole) {
				return QString::fromStdString(item->file.name);
			} else if (role == Qt::DecorationRole) {
				return QIcon::fromTheme("text-x-generic");
			} else if (role == Qt::FontRole) {
				QFont f;
				if (opened)
					f.setItalic(true);
				return f;
			}
			break;
		default:
			return QVariant();
		}
	}
	return QVariant();
}

Qt::ItemFlags WorkspaceQtModel::flags(QModelIndex const & index) const
{
	Qt::ItemFlags flag = 0;
	if (index.isValid()) {
		flag = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}
	return flag;
}

QVariant WorkspaceQtModel::headerData(int , Qt::Orientation , int ) const
{
	return QVariant();
}

QModelIndex WorkspaceQtModel::index(int row, int column, QModelIndex const& parent) const
{
	if (!hasIndex(row, column, parent)) {
		return QModelIndex();
	}

	Item* parent_item;
	if (!parent.isValid()) {
		parent_item = m_root;
	} else {
		parent_item = static_cast<Item*>(parent.internalPointer());
	}

	switch (parent_item->type) {
	case RootType:
		return createIndex(row, column, parent_item->workspace.children[row]);
	case ProjectType:
		return createIndex(row, column, parent_item->project.children[row]);
	default:
		return QModelIndex();
	}
}

bool WorkspaceQtModel::insertRow(int row, QModelIndex const& parent)
{
	return insertRows(row, 1, parent);
}

bool WorkspaceQtModel::insertRows(int row, int count, QModelIndex const& parent)
{
	Item* item;
	if (parent.isValid()) {
		item = static_cast<Item*>(parent.internalPointer());
	} else {
		item = m_root;
	}

	if (row < 0) {
		return false;
	}

	vector<Item*>* children = nullptr;

	switch (item->type) {
	case RootType:
		children = &item->workspace.children;
		break;
	case ProjectType:
		children = &item->project.children;
		break;
	default:
		// Other types cannot have children.
		return false;
	}

	if (row > (int) children->size()) {
		return false;
	}

	beginInsertRows(parent, row, row + count - 1);
	for (int i = 0; i < count; i++) {
		children->insert(children->begin() + row + i, 0);
	}
	// Update row numbers of items following inserted item.
	for (int i = row + count; i < static_cast<int>(children->size()); i++) {
		(*children)[i]->row += count;
	}
	endInsertRows();

	return true;
}

QModelIndex WorkspaceQtModel::parent(QModelIndex const& index) const
{
	if (!index.isValid()) {
		return QModelIndex();
	}
	Item* child = static_cast<Item*>(index.internalPointer());
	if (child->type == FileType) {
		Item* project = child->file.parent;
		auto it = find(m_root->workspace.children.begin(), m_root->workspace.children.end(), project);
		return createIndex(it - m_root->workspace.children.begin(), 0, project);
	} else {
		return QModelIndex();
	}
}

bool WorkspaceQtModel::removeRow(int row, QModelIndex const& parent)
{
	return removeRows(row, 1, parent);
}

bool WorkspaceQtModel::removeRows(int row, int count, QModelIndex const& parent)
{
	Item* item;
	if (parent.isValid()) {
		item = static_cast<Item*>(parent.internalPointer());
	} else {
		item = m_root;
	}
	if (row < 0) {
		return false;
	}

	vector<Item*>* children = nullptr;

	switch (item->type) {
	case RootType:
		children = &item->workspace.children;
		break;
	case ProjectType:
		children = &item->project.children;
		break;
	default:
		return false;
	}

	if (row + count > (int) children->size()) {
		return false;
	}

	beginRemoveRows(parent, row, row + count - 1);
	for (int i = 0; i < count; i++) {
		delete (*children)[row + i];
		children->erase(children->begin() + row + i);
	}
	endRemoveRows();

	return true;
}

int WorkspaceQtModel::rowCount(QModelIndex const& parent) const
{
	Item* item;
	if (!parent.isValid()) {
		item = m_root;
	} else {
		item = static_cast<Item*>(parent.internalPointer());
	}
	switch (item->type) {
	case RootType:
		return item->workspace.children.size();
	case ProjectType:
		return item->project.children.size();
	default:
		return 0;
	}
}

} // end of namespace Gui
} // end of namespace SimShell
