#ifndef PTREEMODEL_H_INCLUDED
#define PTREEMODEL_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeModel.
 */

#include <vector>
#include <boost/property_tree/ptree_fwd.hpp>
#include <QAbstractItemModel>

class QUndoStack;

namespace SimShell {
namespace Gui {

/**
 * Qt model reflecting hierarchical structure of a ptree.
 * To be used with PTreeView.
 *
 * @see PTreeView
 */
class PTreeModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	/// Constructs an empty PTreeModel.
	PTreeModel(boost::property_tree::ptree const& tree, QObject* parent = 0);

	///
	PTreeModel(PTreeModel const&);

	///
	PTreeModel & operator=(PTreeModel const&);

	/// Virtual destructor
	virtual ~PTreeModel();

	/**
	 * Test whether the "only edit data" option set.
	 * "Only edit data" means the user cannot modify key names, delete, add, move subtrees.
	 * Default = false
	 */
	bool IsOnlyEditData() const;

	/**
	 * Moves a single row from one place to another.
	 * @param old_row     Index of row to move.
	 * @param old_parent  Model index of parent of row to move.
	 * @param new_row     New index for row.
	 * @param new_parent  New parent for row.
	 * @return true if successful.
	 */
	bool MoveRow(int  old_row, QModelIndex const& old_parent, int new_row, QModelIndex const& new_parent);

	/**
	 * Moves a block of contagious rows from one place to another.
	 * @param old_row     Index of first row to move.
	 * @param old_parent  Model index of parent of row to move.
	 * @param new_row     New index for first row.
	 * @param new_parent  New parent for row.
	 * @param count       Number of rows to move.
	 * @return true if successful.
	 */
	bool MoveRows(int old_row, QModelIndex const& old_parent, int new_row, QModelIndex const& new_parent, int count);

	/**
	 * Set the "only edit values" option.
	 * @see IsOnlyEditValues()
	 */
	void SetOnlyEditData(bool);

	/**
	 * Set this model to use a given undo stack.
	 * All actions that change the model (e.g. insertRow(), setData()) will be pushed onto that stack.
	 *
	 * Before using this model, it is necessary to call this method!
	 */
	void SetUndoStack(QUndoStack*);

	// Notice: Load() method is gone. If you want to load a different ptree,
	// simply construct a new model!

	/**
	 * Creates a new property tree from the current model state.
	 */
	boost::property_tree::ptree Store() const;

	// Implementation of QAbstractItemModel, see QAbstractItemModel documentation for details.
	virtual int columnCount(QModelIndex const& parent = QModelIndex()) const;
	virtual QVariant data(QModelIndex const& index, int role = Qt::DisplayRole) const;
	virtual Qt::ItemFlags flags(QModelIndex const& index) const;
	virtual QVariant headerData(int section, Qt::Orientation, int role) const;
	virtual QModelIndex index(int row, int column, QModelIndex const& parent = QModelIndex()) const;
	virtual bool insertRow(int row, QModelIndex const& parent = QModelIndex());
	virtual bool insertRows(int row, int count, QModelIndex const& parent = QModelIndex());
	virtual bool removeRow(int row, QModelIndex const& parent = QModelIndex());
	virtual bool removeRows(int row, int count, QModelIndex const& parent = QModelIndex());
	virtual QModelIndex parent(QModelIndex const& index) const;
	virtual int rowCount(QModelIndex const& parent = QModelIndex()) const;
	virtual bool setData(QModelIndex const& index, QVariant const& value, int role = Qt::EditRole);
	virtual Qt::DropActions supportedDragActions() const;
	virtual Qt::DropActions supportedDropActions() const;
	virtual QStringList mimeTypes() const;
	virtual QMimeData* mimeData(QModelIndexList const& indexes) const;
	virtual bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, QModelIndex const& parent);

private:
	struct Item
	{
	public:
		Item(Item* parent = 0, QVariant const& key = QVariant(), int row = 0);
		Item(Item const&);
		Item& operator=(Item const&);
		~Item();

		///
		Item* GetParent();

		///
		Item* GetChild(unsigned int i);

		///
		Item const* GetChild(unsigned int i) const;

		///
		int GetChildrenCount() const;

		///
		Item const* GetParent() const;

		///
		void InsertChild(unsigned int row, Item*);

		/**
		 * Construct a new tree structure with this item as root.
		 * @param t  Object to construct tree from.
		 */
		void Load(boost::property_tree::ptree const& t);

		///
		void RemoveChild(unsigned int row);

		/**
		 * Creates a property tree that represents the subtree with this item as the root.
		 * @return property tree with this item as root.
		 */
		boost::property_tree::ptree Store() const;

		QVariant   key;
		QVariant   data;
		QVariant   background;
		int        row;

	private:
		Item*               parent;   // read-only
		std::vector<Item*>  children; // use public methods!
	};

	QUndoStack*   undo_stack;
	Item*         root;
	bool          only_edit_data;

	class EditKeyCommand;
	class EditDataCommand;
	class InsertRowsCommand;
	class RemoveRowsCommand;
	class MoveRowsCommand;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif
