#ifndef PTREE_VIEW_H_INCLUDED
#define PTREE_VIEW_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeView.
 */

#include <QUndoStack>
#include "MyTreeView.h"

class QAction;

namespace SimShell {
namespace Gui {

class MyFindDialog;

/**
 * TreeView widget that presents an editable ptree to the user.
 * Actions exist for finding keys/data, undo/redo, cut/copy/paste, etc.
 * Context menus are provided.
 */
class PTreeView : public MyTreeView
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 * @param   parent  Parent of the window.
	 */
	PTreeView(QWidget* parent = 0);

	/**
	 * Virtual Destructor.
	 */
	virtual ~PTreeView() {}

	/**
	 * Test whether the view's internal undo stack is in a clean state.
	 * @return Whether the internal undo stack is in a clean state.
	 */
	bool IsClean() const;

	/**
	 * Set the view's internal undo stack's current state to be the clean state.
	 * IsClean() will test positive after this.
	 */
	void SetClean();

	bool IsOnlyEditData() const;

	void SetOnlyEditData(bool);

	/**
	 * Reimplemented from QTreeView, also enables/disables certain actions
	 */
	virtual void setModel(QAbstractItemModel* model);

	// any window can put these actions in its menu / toolbar / ...
	QAction* GetUndoAction() const;
	QAction* GetRedoAction() const;
	QAction* GetCutAction() const;
	QAction* GetCopyAction() const;
	QAction* GetPasteAction() const;
	QAction* GetMoveUpAction() const;
	QAction* GetMoveDownAction() const;
	QAction* GetInsertBeforeAction() const;
	QAction* GetInsertChildAction() const;
	QAction* GetRemoveAction() const;
	QAction* GetFindDialogAction() const;
	QAction* GetClearHighlightAction() const;
	QAction* GetExpandAllAction() const;
	QAction* GetExpandNoneAction() const;

private slots:
	/**
	 * Cut operation on selected item.
	 * Has no effect if there is no selection or no model is set.
	 */
	void Cut();

	/**
	 * Copy operation on selected item.
	 * Has no effect if there is no selection or no model is set.
	 */
	void Copy();

	/**
	 * Paste operation on selected item.
	 * Has no effect if there is no selection or no model is set.
	 */
	void Paste();

	/**
	 * Move selected item by one postion up.
	 * Has no effect if there is no selection or no model is set.
	 */
	void MoveUp();

	/**
	 * Move selected item by one postion down.
	 * Has no effect if there is no selection or no model is set.
	 */
	void MoveDown();

	/**
	 * Insert empty row before selected item.
	 * Has no effect if there is no selection or no model is set.
	 */
	void Insert();

	/**
	 * Insert child into selected item.
	 * Has no effect if there is no selection or no model is set.
	 */
	void InsertChild();

	/**
	 * Remove selected item.
	 * Has no effect if there is no selection or no model is set.
	 */
	void Remove();

	/**
	 * Display find dialog.
	 */
	void FindDialog();

	/**
	 * Clear highlighted items after search.
	 * Has no effect if no model is set.
	 */
	void ClearHighlight();

	/**
	 * Cut operation on selected item.
	 * Has no effect if no model is set.
	 */
	void FindNext(QString const &, bool match_case);

	/**
	 * Used internally to enable / disable paste action
	 * when clipboard changes.
	 */
	void SetClipboardChanged();

	/**
	 * Used internally to enable / disable actions
	 * when a selection is made / selection is cleared.
	 */
	void SetSelectionChanged(QItemSelection const &, QItemSelection const &);

signals:
	/**
	 * Emitted whenever the view's internal undo stack state changes
	 * from clean to non-clean, or from non-clean to clean.
	 * @param clean   Whether the internal undo stack is clean.
	 */
	void CleanChanged(bool clean);

	/**
	 * Emitted whenever the data contained in the model changes.
	 * This may happen when the user edits the data, or when an operation
	 * is un/redone.
	 */
	void Edited();

	/**
	 * Emitted when view has some message to share with, for example, a statusbar.
	 * @param message  Text message that view wants to make available.
	 */
	void StatusChanged(QString const & message);

protected:
	/**
	 * Reimplemented from QWidget.
	 * Generates a context menu with some trivial actions,
	 * e.g. inserting a new item, moving, removing.
	 */
	virtual void contextMenuEvent(QContextMenuEvent*);

private:
	Q_DISABLE_COPY(PTreeView)

	QUndoStack      m_undo_stack;
	MyFindDialog*   m_find_dialog;
	bool            m_only_edit_data;

	Qt::MatchFlags             m_flags;     ///< For search state.
	QString                    m_query;     ///< For search state.
	QModelIndexList            m_results;   ///< For search state.
	QModelIndexList::iterator  m_result;    ///< For search state.

	QAction*    m_action_undo;
	QAction*    m_action_redo;
	QAction*    m_action_cut;
	QAction*    m_action_copy;
	QAction*    m_action_paste;
	QAction*    m_action_move_up;
	QAction*    m_action_move_down;
	QAction*    m_action_insert;
	QAction*    m_action_insert_child;
	QAction*    m_action_remove;
	QAction*    m_action_find_dialog;
	QAction*    m_action_clear_highlight;
	QAction*    m_action_expand_all;
	QAction*    m_action_expand_none;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif
