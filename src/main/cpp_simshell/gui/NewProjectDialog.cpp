/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2).
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for NewProjectDialog.
 */

#include <QLabel>
#include <QGridLayout>
#include <QHBoxLayout>
#include "workspace/IWorkspaceFactory.h"
#include "workspace/IWorkspace.h"
#include "workspace/IProject.h"
#include "workspace/IFile.h"
#include "NewProjectDialog.h"

namespace SimShell {
namespace Gui {

using namespace std;
using namespace Ws;

NewProjectDialog::NewProjectDialog(const shared_ptr<IWorkspaceFactory>& f, QWidget* parent)
	: QDialog(parent),
	  m_workspace_model(f->CreateWorkspace(f->GetWorkspaceTemplatePath()))
{
	setWindowTitle("New Project");

	m_dialog = 0; // browse dialog

	// "global" layout
	QVBoxLayout* layout = new QVBoxLayout;

	QHBoxLayout* layout_name = new QHBoxLayout;
	QLabel* label_name = new QLabel("Name:");
	m_edit_name = new QLineEdit;
	m_edit_name->setText("untitled");
	m_edit_name->selectAll();
	label_name->setBuddy(m_edit_name);
	layout_name->addWidget(label_name);
	layout_name->addWidget(m_edit_name);
	layout->addLayout(layout_name);

	QLabel* label_info = new QLabel("Please specify a file to initialize project with:");
	layout->addWidget(label_info);

	QVBoxLayout* layout_leaf = new QVBoxLayout;
	QHBoxLayout* layout_default = new QHBoxLayout;
	m_radio_default = new QRadioButton("Copy project from template workspace:");
	m_radio_default->setChecked(true);
	m_combo_models = new QComboBox;
	vector<string> project_list;
	for (auto& project : *m_workspace_model) {
		project_list.push_back(project.first);
	}
	for (vector<string>::const_iterator it = project_list.begin();
			it != project_list.end(); it++)
		m_combo_models->addItem(it->c_str());
	layout_default->addWidget(m_radio_default);
	layout_default->addWidget(m_combo_models);
	layout_leaf->addLayout(layout_default);
	QHBoxLayout* layout_custom = new QHBoxLayout;
	m_radio_custom = new QRadioButton("Use the following file:");
	m_edit_custom = new QLineEdit;
	m_edit_custom->setEnabled(false);
	QPushButton* button_browse = new QPushButton("Browse...");
	button_browse->setEnabled(false);
	layout_custom->addWidget(m_radio_custom);
	layout_custom->addWidget(m_edit_custom);
	layout_custom->addWidget(button_browse);
	layout_leaf->addLayout(layout_custom);
	layout->addLayout(layout_leaf);

	layout->addStretch();

	QHBoxLayout* layout_buttons = new QHBoxLayout;
	m_button_ok = new QPushButton("OK");
	m_button_ok->setDefault(true);
	m_button_cancel = new QPushButton("Cancel");
	layout_buttons->addStretch();
	layout_buttons->addWidget(m_button_cancel);
	layout_buttons->addWidget(m_button_ok);
	layout->addLayout(layout_buttons);

	setLayout(layout);

	connect(m_edit_name, SIGNAL(textChanged(const QString &)), this, SLOT(ProjectNameChanged(const QString &)));
	connect(m_radio_custom, SIGNAL(toggled(bool)), m_edit_custom, SLOT(setEnabled(bool)));
	connect(m_radio_custom, SIGNAL(toggled(bool)), button_browse, SLOT(setEnabled(bool)));
	connect(m_radio_default, SIGNAL(toggled(bool)), this, SLOT(FileChanged(bool)));
	connect(m_edit_custom, SIGNAL(textChanged(const QString &)), this, SLOT(FileChanged(const QString &)));
	connect(button_browse, SIGNAL(clicked()), this, SLOT(ShowBrowseDialog()));
	connect(m_button_ok, SIGNAL(clicked()), this, SLOT(Ok()));
	connect(m_button_cancel, SIGNAL(clicked()), this, SLOT(Cancel()));

	project_name_ok = true;
	file_ok = true;
}

void NewProjectDialog::Cancel()
{
	reject();
}

string NewProjectDialog::GetSrcPath() const
{
	return m_edit_custom->text().toStdString();
}

IWorkspace::ConstProjectIterator NewProjectDialog::GetSrcProject() const
{
	std::string project_name = m_combo_models->currentText().toStdString();
	return m_workspace_model->Find(project_name);
}

string NewProjectDialog::GetProjectName() const
{
	return m_edit_name->text().toStdString();
}

void NewProjectDialog::FileChanged(bool use_default)
{
	file_ok = use_default || m_edit_custom->text().length();
	ValidateForm();
}

void NewProjectDialog::FileChanged(QString const& leaf)
{
	file_ok = m_radio_default->isChecked() || leaf.length();
	ValidateForm();
}

bool NewProjectDialog::IsCopyProject() const
{
	return m_radio_default->isChecked();
}

void NewProjectDialog::ProjectNameChanged(QString const & name)
{
	project_name_ok = name.length();
	ValidateForm();
}

void NewProjectDialog::Ok()
{
	accept();
}

void NewProjectDialog::ShowBrowseDialog()
{
	if (m_dialog) {
		m_dialog->exec();
	}
	else
	{
		m_dialog = new QFileDialog(this, "Browse", QDir::homePath(), "XML files (*.xml)");
		m_dialog->setAcceptMode(QFileDialog::AcceptOpen);
		m_dialog->setFileMode(QFileDialog::ExistingFile);
		connect(m_dialog, SIGNAL(fileSelected(const QString &)), m_edit_custom, SLOT(setText(const QString &)));
		m_dialog->exec();
	}
}

// TODO: use more complex ways to check for valid name and valid file
void NewProjectDialog::ValidateForm()
{
	m_button_ok->setEnabled(project_name_ok && file_ok);
}

} // end of namespace Gui
} // end of namespace SimShell
