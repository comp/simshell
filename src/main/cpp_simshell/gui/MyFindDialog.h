#ifndef MY_FINDDIALOG_H_INCLUDED
#define MY_FINDDIALOG_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MyFindDialog.
 */

#include <QCheckBox>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

namespace SimShell {
namespace Gui {

/**
 * Dialog window for searching strings.
 *
 * Also has a 'match case' checkbox.
 */
class MyFindDialog : public QDialog
{
	Q_OBJECT
public:
	MyFindDialog(QWidget* parent = 0);

	/**
	 * Sets focus to the text input box so the user can start typing right away.
	 */
	void CorrectFocus();

private slots:
	void Find();

signals:
	/**
	 * Emitted when user performs a search query by clicking 'Search'
	 * or pressing the Enter button.
	 * @param text         Text the user searches for.
	 * @param match_case   Search to be case-sensitive or not.
	 */
	void FindNext(QString const & text, bool match_case);

private:
	QPushButton*  m_close_button;
	QPushButton*  m_find_button;
	QLineEdit*    m_input;
	QLabel*       m_label;
	QCheckBox*    m_match_case;
};

} // namespace Gui
} // namespace SimShell

#endif
