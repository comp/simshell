#ifndef WORKSPACE_VIEW_H_INCLUDED
#define WORKSPACE_VIEW_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for WorkspaceView.
 */

#include <QAction>
#include <QItemSelectionModel>
#include "MyTreeView.h"
#include "workspace/IProject.h"

namespace SimShell {
namespace Gui {

class WorkspaceQtModel;

/**
 * TreeView widget that uses WorkspaceQtModel as a model.
 *
 * Owner of a number of actions concerning selected items.
 * Also displays a context menu for selected items.
 */
class WorkspaceView : public MyTreeView
{
	Q_OBJECT
public:
	WorkspaceView(QWidget* parent = 0);
	virtual ~WorkspaceView() {}

	/**
	 * Set the model.
	 *
	 * Must be of dynamic type WorkspaceQtModel or the model is set to zero.
	 */
	virtual void setModel(QAbstractItemModel*);

	/**
	 * Get 'open' action object.
	 *
	 * A 'trigger' signal coming from the action object will cause the selected item to be opened.
	 */
	QAction* GetOpenAction() const;

	/**
	 * Get 'rename' action object.
	 *
	 * This action will only be enabled if a project item is selected in the view; leaf files cannot be renamed.
	 */
	QAction* GetRenameAction() const;

	/**
	 * Get 'remove' action object.
	 *
	 * A 'trigger' signal coming from the action object will cause the selected item to be removed.
	 */
	QAction* GetRemoveAction() const;

private slots:
	/**
	 * Close selected item.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_Close();

	/**
	 * Open selected leaf file.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_Open();

	void SLOT_ProjectWidgetAction();

	/**
	 * Display dialog asking for a new name for the selected project or leaf.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_RenameDialog();

	/**
	 * Remove selected item from workspace.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_Remove();

	/**
	 * Handler called when this view's current item has changed.
	 * @param selected    Selected items.
	 * @param deselected  Deselected items.
	 */
	void SLOT_SelectionChanged(QItemSelection const & selected, QItemSelection const & deselected);

signals:
	/**
	 * Emitted when user renamed a project
	 */
	void ProjectRenamed(const std::string& old_name, const std::string& new_name);

	/**
	 * Emitted when user selected "delete" option on a project.
	 */
	void ProjectRemoved(const std::string& project);

protected:
	/**
	 * Reimplemented from QWidget.
	 * Displays a context menu with trivial actions for the selected item.
	 */
	virtual void contextMenuEvent(QContextMenuEvent*);

	/**
	 * Reimplemented from QTreeView.
	 * If double click happened on a leaf file, it is opened,
	 * otherwise, the handler of the base class is called
	 * (for expanding/collapsing project items)
	 */
	virtual void mouseDoubleClickEvent(QMouseEvent*);

private:
	WorkspaceQtModel* model; // this is here so i don't have to cast model() every time.

	QAction* action_open;
	QAction* action_rename;
	QAction* action_remove;
	QAction* action_close;

	std::shared_ptr<QWidget> project_widget;
	std::string project_widget_project_name;
	struct CallbackMapEntry {
		Ws::IProject::WidgetCallback::Type  callback;
		std::string                         project_name;
	};
	std::map<QObject*, CallbackMapEntry> object_to_callback_map;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif
