#ifndef GUI_HASUNSAVEDCHANGESPROMPT_H_INCLUDED
#define GUI_HASUNSAVEDCHANGESPROMPT_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * HasUnsavedChangesPrompt interface.
 */

#include "HasUnsavedChanges.h"

class QWidget;

namespace SimShell {
namespace Gui {

/**
 * HasUnsavedChanges with the ability of displaying a "save-discard-cancel" dialog to the user before closing, if there are unsaved changes.
 */
class HasUnsavedChangesPrompt : public HasUnsavedChanges
{
public:
	HasUnsavedChangesPrompt(std::string&& title);

	/// Display dialog window containing a list (or tree) of widgets that contain unsaved changes.
	/// The user can select which widgets to save and which to discard, or to cancel the close operation.
	/// If there are no unsaved changes (in this widget and in its children), then no dialog is displayed and the method immediately returns true.
	/// @return Whether we are in unopened state, i.e. close operation was successful (regardless whether user saved or discarded changes)
	bool PromptClose(QWidget* parent = nullptr);
};

} // namespace Gui
} // namespace SimShell

#endif
