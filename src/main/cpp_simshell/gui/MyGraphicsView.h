#ifndef MYGRAPHICSVIEW_H_INCLUDED
#define MYGRAPHICSVIEW_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2).
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for MyGraphicsView.
 */

#include <memory>
#include <QGraphicsView>

class QWheelEvent;

namespace SimShell {
namespace Gui {

/**
 * Scrollable QGraphicsView with zoom functionality. Also takes co-ownership of graphics scene.
 */
class MyGraphicsView: public QGraphicsView
{
	Q_OBJECT
public:
	/**
	 * @param g       Graphics scene. Will take co-ownership.
	 * @param parent  Parent widget.
	 */
	MyGraphicsView(std::shared_ptr<QGraphicsScene> g, QWidget* parent = nullptr);

	/**
	 * @param parent  Parent widget.
	 */
	MyGraphicsView(QWidget* parent = nullptr);

protected:
	void wheelEvent(QWheelEvent* event);

private:
	std::shared_ptr<QGraphicsScene> m_scene;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif // end_of_include_guard
