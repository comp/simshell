/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * WorkspaceController implementation.
 */

#include "WorkspaceController.h"

#include <QAction>
#include <QMessageBox>

#include "common/PTreeQtState.h"
#include "gui/NewProjectDialog.h"
#include "gui/PTreeContainer.h"
#include "gui/PTreeContainerPreferencesObserver.h"
#include "gui/PTreeMenu.h"
#include "gui/WorkspaceView.h"
#include "gui/qtmodel/WorkspaceQtModel.h"
#include "util/Exception.h"
#include "AppController.h"


using namespace std;
using namespace boost::property_tree;
using namespace UA_CoMP::Util;

namespace SimShell {
namespace Gui {
namespace Controller {

WorkspaceController::WorkspaceController(shared_ptr<Ws::IWorkspaceFactory> f, AppController* m)
	: QWidget(m),
	  HasUnsavedChangesPrompt("Workspace"),
	  m_factory(f),
	  m_main_controller(m),
	  m_project_controller(m)
{
	qRegisterMetaType< std::string >();
	InitActions();
	InitWidgets();
	InitBaseClasses();
}

QAction* WorkspaceController::GetActionNewProject() const
{
	return m_a_new_project;
}

QAction* WorkspaceController::GetActionRefreshWorkspace() const
{
	return m_a_refresh_workspace;
}

QDockWidget* WorkspaceController::GetPreferencesDock() const
{
	return m_container_preferences->GetDock();
}

QMenu* WorkspaceController::GetPreferencesMenu() const
{
	return m_container_preferences->GetMenu();
}

ptree WorkspaceController::GetPTreeState() const
{
	ptree state;
	state.put_child("project_controller", m_project_controller.GetPTreeState());
	state.put_child("view", m_view->GetPTreeState());
	state.put_child("container_preferences", m_container_preferences->GetPTreeState());
	return state;
}

WorkspaceView* WorkspaceController::GetView() const
{
	return m_view;
}

void WorkspaceController::InitActions()
{
	m_a_new_project = new QAction(QIcon::fromTheme("document-new"), "New Project...", this);
	m_a_new_project->setShortcut(QKeySequence("Ctrl+N"));
	m_a_new_project->setEnabled(false);
	connect(m_a_new_project, SIGNAL(triggered()), this, SLOT(SLOT_NewProjectDialog()));

	m_a_refresh_workspace = new QAction(QIcon::fromTheme("view-refresh"), "Refresh Workspace", this);
	m_a_refresh_workspace->setShortcut(QKeySequence("F5"));
	m_a_refresh_workspace->setEnabled(false);
	connect(m_a_refresh_workspace, SIGNAL(triggered()), this, SLOT(SLOT_RefreshWorkspace()));

	m_enabled_actions.Set({ m_a_new_project, m_a_refresh_workspace });
}

void WorkspaceController::InitBaseClasses()
{
	HasUnsavedChanges::SetChildren({
		&m_project_controller,
		m_container_preferences.get()
	});
}

void WorkspaceController::InitWidgets()
{
	m_view = new WorkspaceView(this);

	m_container_preferences = make_shared<PTreeContainerPreferencesObserver>("Workspace Preferences");
	m_container_preferences->SetOnlyEditData(true);
	connect(m_container_preferences.get(), SIGNAL(Applied(const boost::property_tree::ptree&)),
		this, SLOT(SLOT_ApplyPreferences(const boost::property_tree::ptree&)));
}

void WorkspaceController::InternalForceClose()
{
	m_enabled_actions.Disable();
	m_view->setModel(nullptr);
	m_model.reset();
}

bool WorkspaceController::InternalIsClean() const
{
	return true;
}

void WorkspaceController::InternalPreForceClose()
{
	m_model->Workspace()->SetUserData("main_controller", m_main_controller->GetPTreeState());
}

bool WorkspaceController::InternalSave()
{
	return true;
}

bool WorkspaceController::IsOpened() const
{
	return (bool) m_model;
}

bool WorkspaceController::Open(const std::string& path)
{
	if (!PromptClose())
		return false;
	try {
		// Create workspace model.
		auto workspace = m_factory->CreateWorkspace(path);
		m_model = WorkspaceQtModel::Create(workspace, &m_project_controller);

		// Open preferences ptree in dock and menu.
		m_container_preferences->Open(m_model->Workspace()->GetPreferences());

		auto handler = bind(&PTreeContainerPreferencesObserver::Update, m_container_preferences.get(), std::placeholders::_1);
		m_model->Workspace()->Subject<Ws::Event::PreferencesChanged, std::weak_ptr<const void>>::Register(m_container_preferences, handler);

		// Open workspace in central view.
		m_view->setModel(m_model.get());

		m_enabled_actions.Enable();

		m_main_controller->SetPTreeState(m_model->Workspace()->GetUserData("main_controller"));

		return true;
	}
	catch (Exception&) {
		return false;
	}
}

ProjectController& WorkspaceController::Project()
{
	return m_project_controller;
}

const ProjectController& WorkspaceController::Project() const
{
	return m_project_controller;
}

void WorkspaceController::SetPTreeState(const ptree& state)
{
	auto container_preferences_state = state.get_child_optional("container_preferences");
	if (container_preferences_state) {
		m_container_preferences->SetPTreeState(container_preferences_state.get());
	}

	auto view_state = state.get_child_optional("view");
	if (view_state) {
		m_view->SetPTreeState(view_state.get());
	}

	auto project_controller_state = state.get_child_optional("project_controller");
	if (project_controller_state) {
		m_project_controller.SetPTreeState(project_controller_state.get());
	}
}

WorkspaceController::operator bool() const
{
	return IsOpened();
}

void WorkspaceController::SLOT_ApplyPreferences(const ptree& p)
{
	m_model->Workspace()->SetPreferences(p);
}

void WorkspaceController::SLOT_NewProjectDialog()
{
	NewProjectDialog dialog(m_factory);
	if (dialog.exec() == QDialog::Accepted) {
		auto project_name = dialog.GetProjectName();

		if (dialog.IsCopyProject()) {
			auto src_project = dialog.GetSrcProject();

			try {
				QDir dst_dir(QString::fromStdString(m_model->Workspace()->GetPath()));
				dst_dir.mkdir(QString::fromStdString(project_name));

				// Copy all files from src_project to new project dir.
				QDir src_dir(QString::fromStdString(src_project->second->GetPath()));
				auto src_files = src_dir.entryList(QDir::Files | QDir::Hidden);
				for (auto& src_file : src_files) {
					QFile::copy(QString::fromStdString(src_project->second->GetPath()) + '/' + src_file,
						    QString::fromStdString(m_model->Workspace()->GetPath()) + '/' + QString::fromStdString(project_name) + '/' + src_file);
				}

				// New project.
				m_model->Workspace()->Add("project", project_name);
			}
			catch (Exception& e) {
				QMessageBox::warning(this, "Error", QString("Could not create project: ") + e.what());
			}
		} else {
			auto src_path = dialog.GetSrcPath();
			auto src_file = QFileInfo(QString::fromStdString(src_path)).fileName().toStdString();

			try {
				QDir dst_dir(QString::fromStdString(m_model->Workspace()->GetPath()));
				dst_dir.mkdir(QString::fromStdString(project_name));

				// Copy selected file
				QFile::copy(QString::fromStdString(src_path),
					    QString::fromStdString(m_model->Workspace()->GetPath()) + '/' + QString::fromStdString(project_name) + '/' + QString::fromStdString(src_file));

				// New project.
				m_model->Workspace()->Add("project", project_name);
			}
			catch (Exception& e) {
				QMessageBox::warning(this, "Error", QString("Could not create project: ") + e.what());
			}
		}
	}
}

void WorkspaceController::SLOT_RefreshWorkspace()
{
	m_model->Workspace()->Refresh();
}

Ws::IWorkspace* WorkspaceController::operator->()
{
	return m_model->Workspace().get();
}

const Ws::IWorkspace* WorkspaceController::operator->() const
{
	return m_model->Workspace().get();
}

shared_ptr<Ws::IWorkspace> WorkspaceController::operator*()
{
	return m_model->Workspace();
}

shared_ptr<const Ws::IWorkspace> WorkspaceController::operator*() const
{
	return m_model->Workspace();
}

} // namespace Controller
} // namespace Gui
} // namespace SimShell
