#ifndef GUI_WORKSPACECONTROLLER_H_INCLUDED
#define GUI_WORKSPACECONTROLLER_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * WorkspaceController header.
 */
#include <memory>
#include <QWidget>

#include "gui/EnabledActions.h"
#include "gui/HasUnsavedChanges.h"
#include "gui/IHasPTreeState.h"
#include "workspace/IWorkspace.h"
#include "workspace/IWorkspaceFactory.h"
#include "ProjectController.h"

namespace SimShell {
namespace Gui {

class PTreeContainerPreferencesObserver;
class WorkspaceView;
class WorkspaceQtModel;

namespace Controller {

class AppController;

/**
 * Controller for workspace.
 *
 * Simple state machine that has 2 states: "opened" and "closed".
 * In opened state, it is owner of a workspace object.
 * Owner of WorkspaceView and a dock windows containing workspace preferences.
 *
 * The "opened" state also has a sub-state machine called ProjectController.
 */
class WorkspaceController : public QWidget,
                            public IHasPTreeState,
                            public HasUnsavedChangesPrompt
{
	Q_OBJECT
public:
	WorkspaceController(std::shared_ptr<Ws::IWorkspaceFactory> f, AppController* m);

	bool Open(const std::string& path);

	QAction* GetActionNewProject() const;

	QAction* GetActionRefreshWorkspace() const;

	QDockWidget* GetPreferencesDock() const;

	QMenu* GetPreferencesMenu() const;

	/// @see IHasPTreeState
	virtual boost::property_tree::ptree GetPTreeState() const;

	WorkspaceView* GetView() const;

	/// @see HasUnsavedChanges
	virtual bool IsOpened() const;

	ProjectController& Project();

	const ProjectController& Project() const;

	/// @see IHasPTreeState
	virtual void SetPTreeState(const boost::property_tree::ptree&);

	/// @return Whether we are in opened state.
	operator bool() const;

	Ws::IWorkspace* operator->();
	const Ws::IWorkspace* operator->() const;

	std::shared_ptr<Ws::IWorkspace> operator*();
	std::shared_ptr<const Ws::IWorkspace> operator*() const;

protected:
	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual void InternalPreForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private slots:
	void SLOT_ApplyPreferences(const boost::property_tree::ptree &);
	void SLOT_NewProjectDialog();
	void SLOT_RefreshWorkspace();

private:
	void InitActions();
	void InitBaseClasses();
	void InitWidgets();

private:
	std::shared_ptr<Ws::IWorkspaceFactory>              m_factory;
	AppController*                                      m_main_controller;

	// These shared ptrs will only contain objects if we are in opened state.
	std::shared_ptr<WorkspaceQtModel>                   m_model;

	// Widgets
	WorkspaceView*                                      m_view;
	std::shared_ptr<PTreeContainerPreferencesObserver>  m_container_preferences;   ///< Collection of menu, dock window, and editor associated with workspace preferences.

	// Actions and menus
	QAction*                                            m_a_new_project;
	QAction*                                            m_a_refresh_workspace;

	// Sub-state
	ProjectController                                   m_project_controller;

	EnabledActions                                      m_enabled_actions;
};

} // namespace Controller
} // namespace Gui
} // namespace SimShell

#endif
