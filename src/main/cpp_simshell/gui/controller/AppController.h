#ifndef GUI_APPCONTROLLER_H_INCLUDED
#define GUI_APPCONTROLLER_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * AppController header.
 */

#include <memory>
#include <QMainWindow>
#include <QSettings>
#include "gui/factory/IFactory.h"
#include "gui/IHasPTreeState.h"
#include "gui/LogWindow.h"
#include "timekeeper/Timeable.h"
#include "WorkspaceController.h"

class QCloseEvent;

namespace SimShell {
namespace Gui {
namespace Controller {

/**
 * Main window of GUI.
 *
 * Owner of 1 WorkspaceController instance.
 */
class AppController :  public QMainWindow,
                       public IHasPTreeState,
                       public UA_CoMP::Timekeeper::Timeable<>
{
	Q_OBJECT
public:
	AppController(const std::shared_ptr<Gui::IFactory>&,
	               QWidget* parent = 0);

	virtual ~AppController();

	/// @see IHasPTreeState
	virtual boost::property_tree::ptree GetPTreeState() const;

	/// @see UA_CoMP::TimeKeeper::Timeable
	virtual Timings GetTimings() const;

	void Log(const std::string&);

	/// @see IHasPTreeState
	virtual void SetPTreeState(const boost::property_tree::ptree&);

protected:
	virtual void closeEvent(QCloseEvent*);

private slots:
	void SLOT_AboutDialog();
	void SLOT_WorkspaceWizard();

private:
	void InitMenu();
	void InitProject();
	void InitWidgets();
	void InitWorkspace();

private:
	std::shared_ptr<Gui::IFactory>              m_factory;

	QSettings                                   m_settings;

	// Widgets
	LogWindow*                                  m_log_dock;

	// Sub-state
	WorkspaceController                         m_workspace_controller;
};

} // namespace Controller
} // namespace Gui
} // namespace SimShell

#endif
