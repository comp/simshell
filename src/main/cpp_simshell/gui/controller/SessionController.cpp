/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011 - 2014,Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * SessionController implementation.
 * @author Joeri Exelmans
 */

#include "SessionController.h"

#include <QAction>
#include <QMessageBox>
#include <QTimer>

#include "util/Exception.h"

using namespace std;
using namespace UA_CoMP::Util;

namespace SimShell {
namespace Gui {
namespace Controller {

SessionController::SessionController()
{
	qRegisterMetaType< std::string >();

	m_action_run = new QAction("Run", this);
	m_action_run->setCheckable(true);
	m_action_run->setEnabled(false);
	m_action_run->setShortcut(QKeySequence("R"));
	m_action_run->setShortcutContext(Qt::ApplicationShortcut);
	connect(m_action_run, SIGNAL(toggled(bool)), this, SLOT(SLOT_ToggleRunning(bool)));

	m_action_single_step = new QAction("Single Step", this);
	m_action_single_step->setEnabled(false);
	m_action_single_step->setShortcut(QKeySequence("S"));
	m_action_single_step->setShortcutContext(Qt::ApplicationShortcut);
	connect(m_action_single_step, SIGNAL(triggered()), this, SLOT(SLOT_TimeStep()));

	m_enabled_actions.Add(m_action_run);
	m_enabled_actions.Add(m_action_single_step);
}

void SessionController::Disable()
{
	m_project.reset();
	m_action_run->setChecked(false);
	m_enabled_actions.Disable();
}

void SessionController::Enable(const shared_ptr<Ws::IProject>& project)
{
	Disable();
	m_project = project;
	if (m_project->IsOpened()) {
		connect(&m_project->Session(),
			SIGNAL(InfoMessage(const std::string&, const InfoMessageReason&)),
			this, SLOT(SimulationInfo(const std::string&, const InfoMessageReason&)));
                connect(&m_project->Session(),
                	SIGNAL(ErrorMessage(const std::string&)),
                	this, SLOT(SimulationError(const std::string&)));
		m_enabled_actions.Enable();
	}
}

QAction* SessionController::GetActionRun() const
{
	return m_action_run;
}

QAction* SessionController::GetActionSingleStep() const
{
	return m_action_single_step;
}

bool SessionController::IsEnabled() const
{
	return (bool) m_project;
}

bool SessionController::IsRunning() const
{
	return m_action_run->isChecked();
}

void SessionController::SetRunning(bool b)
{
	if (m_action_run->isEnabled())
		m_action_run->setChecked(b);
}

void SessionController::SimulationError(const std::string& error)
{
        //if (m_project)
        //	m_project->StopSimulation();
	std::cerr << "Exception> " + error << std::endl;
	QString qmess = QString("Exception:\n%1\n").arg(error.c_str());
	QMessageBox::information(0, "Critical Error", qmess);
}

void SessionController::SimulationInfo(const std::string& ,
		const Session::ISession::InfoMessageReason& reason)
{
        switch (reason) {
        case Session::ISession::InfoMessageReason::Stopped:
                SetRunning(false);
                break;
        case Session::ISession::InfoMessageReason::Terminated:
                Disable();
                break;
        default:
                break;
        }
}

void SessionController::SLOT_ToggleRunning(bool b)
{
	if (m_project) {
		if (m_project->IsOpened()) {
			if (b) {
				m_project->Session().StartSimulation();
			} else {
				m_project->Session().StopSimulation();
			}
		}
	}

	m_action_single_step->setEnabled(!b);
}

void SessionController::SLOT_TimeStep()
{
	if (m_project->IsOpened()) {
		m_project->Session().TimeStep();
	}
}

} // namespace Controller
} // namespace Gui
} // namespace SimShell
