/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * AppController implementation.
 */

#include "AppController.h"

#include <QCloseEvent>
#include <QDockWidget>
#include <QMenuBar>
#include <QMessageBox>

#include "common/PTreeQtState.h"
#include "gui/WorkspaceView.h"
#include "gui/WorkspaceWizard.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {
namespace Controller {

AppController::AppController(const shared_ptr<Gui::IFactory>& f, QWidget* p)
	: QMainWindow(p),
	  m_factory(f),
	  m_settings(QString::fromStdString(f->GetOrganizationName()), QString::fromStdString(f->GetApplicationName())),
	  m_workspace_controller(f->CreateWorkspaceFactory(), this)
{
	InitWidgets();
	InitMenu();
	InitWorkspace();
	InitProject();
}

AppController::~AppController()
{
}

void AppController::closeEvent(QCloseEvent* event)
{
	if (m_workspace_controller) {
		m_workspace_controller->SetUserData("recently_opened.project",
			ptree(m_workspace_controller.Project().GetOpenedProjectName()));
		m_workspace_controller->SetUserData("recently_opened.file",
			ptree(m_workspace_controller.Project().GetOpenedFileName()));
		m_settings.setValue("workspace", QString::fromStdString(m_workspace_controller->GetPath()));
	} else {
		m_settings.remove("workspace");
	}

	if (m_workspace_controller.PromptClose()) {
		event->accept();
	} else {
		event->ignore();
	}
}

ptree AppController::GetPTreeState() const
{
	ptree state;
	state.put_child("workspace_controller", m_workspace_controller.GetPTreeState());
	state.put_child("workspace_preferences_dock", PTreeQtState::GetDockWidgetArea(this, m_workspace_controller.GetPreferencesDock()));
	state.put_child("project_parameters_dock", PTreeQtState::GetDockWidgetArea(this, m_workspace_controller.Project().GetParametersDock()));
	state.put_child("project_preferences_dock", PTreeQtState::GetDockWidgetArea(this, m_workspace_controller.Project().GetPreferencesDock()));
	state.put_child("widget", PTreeQtState::GetWidgetState(this));
	return state;
}

AppController::Timings AppController::GetTimings() const
{
	return m_workspace_controller.Project().GetTimings();
}

void AppController::InitMenu()
{
	auto menu = menuBar();

	// File menu
	auto menu_file = menu->addMenu("&File");
	menu_file->addAction(m_workspace_controller.GetActionNewProject());
	menu_file->addSeparator();
	menu_file->addAction(m_workspace_controller.GetView()->GetOpenAction());
	menu_file->addAction(m_workspace_controller.GetView()->GetRenameAction());
	menu_file->addAction(m_workspace_controller.GetView()->GetRemoveAction());
	menu_file->addSeparator();
	menu_file->addMenu(m_workspace_controller.Project().GetExportMenu());
	menu_file->addSeparator();
	menu_file->addAction(m_workspace_controller.GetActionRefreshWorkspace());
	menu_file->addAction("Switch Workspace...", this, SLOT(SLOT_WorkspaceWizard()));
	menu_file->addSeparator();
	menu_file->addAction(m_workspace_controller.Project().GetActionClose());
	auto action_exit = menu_file->addAction("E&xit", this, SLOT(close()));
	action_exit->setShortcut(QKeySequence("Ctrl+Q"));

	// Edit menu
	auto menu_edit = menu->addMenu("&Edit");
	menu_edit->addMenu(m_workspace_controller.Project().GetParametersMenu());
	menu_edit->addSeparator();
	menu_edit->addMenu(m_workspace_controller.Project().GetPreferencesMenu());
	menu_edit->addMenu(m_workspace_controller.GetPreferencesMenu());

	// Viewers menu
	menu->addMenu(m_workspace_controller.Project().GetViewersMenu());

	// Simulation menu
	menu->addMenu(m_workspace_controller.Project().GetSimulationMenu());

	// Help menu
	auto menu_help = menu->addMenu("&Help");
	menu_help->addAction("About", this, SLOT(SLOT_AboutDialog()));
}

void AppController::InitProject()
{
	if (m_workspace_controller) {
		auto p = m_workspace_controller->Find(
			m_workspace_controller->GetUserData("recently_opened.project").get_value<string>());
		if (p != m_workspace_controller->end()) {
			auto project = p->second.Project();
			auto f = project->Find(
				m_workspace_controller->GetUserData("recently_opened.file").get_value<string>());
			if (f != project->end()) {
				project->Open(f);
				m_workspace_controller.Project().Set(p->first, project);
			}
		}
	}
}

void AppController::InitWidgets()
{
	setWindowTitle(QString::fromStdString(m_factory->GetApplicationName()));
	setCentralWidget(m_workspace_controller.GetView());

	m_log_dock = new LogWindow(this);
	addDockWidget(Qt::BottomDockWidgetArea, m_log_dock);

	auto workspace_preferences_dock = m_workspace_controller.GetPreferencesDock();
	addDockWidget(Qt::RightDockWidgetArea, workspace_preferences_dock);
	workspace_preferences_dock->hide();

	auto project_preferences_dock = m_workspace_controller.Project().GetPreferencesDock();
	addDockWidget(Qt::RightDockWidgetArea, project_preferences_dock);
	project_preferences_dock->hide();

	auto project_parameters_dock = m_workspace_controller.Project().GetParametersDock();
	addDockWidget(Qt::RightDockWidgetArea, project_parameters_dock);
}

void AppController::InitWorkspace()
{
	// Open workspace
	if (m_settings.contains("workspace")) {
		const string workspace = m_settings.value("workspace").toString().toStdString();
		m_workspace_controller.Open(workspace);
	}

	if (!m_workspace_controller) {
		SLOT_WorkspaceWizard();
	}
}

void AppController::Log(const string& s)
{
	m_log_dock->AddLine(s);
}

void AppController::SetPTreeState(const ptree& state)
{
	auto widget_state = state.get_child_optional("widget");
	if (widget_state) {
		PTreeQtState::SetWidgetState(this, widget_state.get());
	}

	auto workspace_preferences_dock_state = state.get_child_optional("workspace_preferences_dock");
	if (workspace_preferences_dock_state) {
		PTreeQtState::SetDockWidgetArea(this,
			m_workspace_controller.GetPreferencesDock(), workspace_preferences_dock_state.get());
	}

	auto project_parameters_dock_state = state.get_child_optional("project_parameters_dock");
	if (project_parameters_dock_state) {
		PTreeQtState::SetDockWidgetArea(this,
			m_workspace_controller.Project().GetParametersDock(), project_parameters_dock_state.get());
	}

	auto project_preferences_dock_state = state.get_child_optional("project_preferences_dock");
	if (project_preferences_dock_state) {
		PTreeQtState::SetDockWidgetArea(this,
			m_workspace_controller.Project().GetPreferencesDock(), project_preferences_dock_state.get());
	}

	auto workspace_controller_state = state.get_child_optional("workspace_controller");
	if (workspace_controller_state) {
		m_workspace_controller.SetPTreeState(workspace_controller_state.get());
	}
}

void AppController::SLOT_AboutDialog()
{
	QMessageBox::about(this,
		QString::fromStdString(m_factory->GetApplicationName()),
		QString::fromStdString(m_factory->GetAbout()));
}

void AppController::SLOT_WorkspaceWizard()
{
	auto& p = m_workspace_controller.Project();
	if (p) {
		p.SetRunning(false);
	}
	WorkspaceWizard w(m_factory->CreateWorkspaceFactory());
	if (w.exec() == QDialog::Accepted) {
		m_workspace_controller.Open(w.GetWorkspaceDir());
	}
}

} // namespace Controller
} // namespace Gui
} // namespace SimShell
