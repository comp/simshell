#ifndef GUI_PROJECTCONTROLLER_H_INCLUDED
#define GUI_PROJECTCONTROLLER_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011 -2014, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * ProjectController header.
 */

#include <memory>
#include <QWidget>

#include "gui/project_actions/ExportActions.h"
#include "gui/project_actions/SimActions.h"
#include "gui/project_actions/ViewerActions.h"
#include "gui/EnabledActions.h"
#include "gui/HasUnsavedChangesPrompt.h"
#include "gui/IHasPTreeState.h"
#include "workspace/IProject.h"
#include "SessionController.h"

class QDockWidget;

namespace SimShell {
namespace Gui {

class PTreeContainer;
class PTreeContainerPreferencesObserver;

namespace Controller {

class AppController;

/**
 * Project controller.
 * Simple state machine that has 2 states: "opened" and "closed".
 * In opened state, it keeps a pointer to a project in the workspace and
 * becomes owner of menus, actions and viewers concerning the opened project.
 * The "opened" state also has a sub-state machine called RunningController.
 */
class ProjectController : public QWidget,
                          public IHasPTreeState,
                          public HasUnsavedChangesPrompt,
                          public UA_CoMP::Timekeeper::Timeable<>
{
	Q_OBJECT
public:
	ProjectController(AppController* m);

	QAction* GetActionClose() const;

	QMenu* GetExportMenu() const;

	const std::string& GetOpenedFileName() const;

	const std::string& GetOpenedProjectName() const;

	QDockWidget* GetParametersDock() const;

	QMenu* GetParametersMenu() const;

	QDockWidget* GetPreferencesDock() const;

	QMenu* GetPreferencesMenu() const;

	/// @see IHasPTreeState
	boost::property_tree::ptree GetPTreeState() const;

	QMenu* GetSimulationMenu() const;

	virtual Timings GetTimings() const;

	QMenu* GetViewersMenu() const;

	/// @see HasUnsavedChanges
	virtual bool IsOpened() const;

	/// @return Whether simulation is running.
	bool IsRunning() const;

	bool Set(const std::string& project_name, const std::shared_ptr<Ws::IProject>& project);

	/// @see IHasPTreeState
	void SetPTreeState(const boost::property_tree::ptree&);

	/// Set simulation running.
	void SetRunning(bool);

	/// @return whether we are in opened state
	operator bool() const;

protected:
	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual void InternalPreForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private slots:
	void SLOT_ApplyParameters(const boost::property_tree::ptree&);
	void SLOT_ApplyPreferences(const boost::property_tree::ptree&);
	void SLOT_Close();

private:
	void InitActions();
	void InitBaseClasses();
	void InitWidgets();

private:
	AppController*                                      m_main_controller;

	// Timings of all simulations
	Timings                                             m_timings;

	// These shared ptrs will only contain objects if we are in opened state.
	std::string                                         m_project_name;
	std::string                                         m_file_name;
	std::shared_ptr<Ws::IProject>                       m_project;
	std::shared_ptr<Viewer::IViewerNode>                m_root_viewer;
	std::shared_ptr<ProjectActions::ExportActions>      m_export_actions;
	std::shared_ptr<ProjectActions::ViewerActions>      m_viewer_actions;
	std::shared_ptr<ProjectActions::SimActions>         m_sim_actions;

	// Widgets
	std::shared_ptr<PTreeContainer>                     m_container_parameters;
	std::shared_ptr<PTreeContainerPreferencesObserver>  m_container_preferences;

	// Actions and menus
	QAction*                                            m_a_close_project;
	QMenu*                                              m_m_export;
	QMenu*                                              m_m_viewers;
	QMenu*                                              m_m_simulation;

	// Sub-state
	SessionController                                   m_running_controller;

	EnabledActions                                      m_enabled_actions; ///< Actions to be enabled when we enter 'opened' state, disabled when entering 'closed' state.
};

} // namespace Controller
} // namespace Gui
} // namespace SimShell

#endif
