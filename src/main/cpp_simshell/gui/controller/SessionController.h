#ifndef GUI_RUNNINGCONTROLLER_H_INCLUDED
#define GUI_RUNNINGCONTROLLER_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * SessionController header.
 */

#include <memory>
#include <QObject>

#include "session/ISession.h"
#include "gui/EnabledActions.h"
#include "workspace/IProject.h"

class QAction;
class QTimer;

namespace SimShell {
namespace Gui {
namespace Controller {

/**
 * Simple state machine with 3 states:
 * "disabled", "enabled-not-running" and "enabled-running".
 *
 * Resembles the boolean "simulation running" property of an opened project.
 * Owner of actions to start/stop the simulation of an opened project, or
 * performing a single step.
 */
class SessionController : public QObject
{
	Q_OBJECT
public:
	SessionController();

	void Disable();

	void Enable(const std::shared_ptr<Ws::IProject>& project);

	QAction* GetActionRun() const;

	QAction* GetActionSingleStep() const;

	bool IsEnabled() const;

	bool IsRunning() const;

	void SetRunning(bool);

private slots:
	void SLOT_ToggleRunning(bool);

	void SLOT_TimeStep();

private:
	// Because of Qt wanting to match parameter types of signals and slots by string comparison
        typedef SimShell::Session::ISession::InfoMessageReason InfoMessageReason;

private slots:
	/// Slot to receive an simulation error from the project
	void SimulationError(const std::string& error);

        /// Slot to receive an simulation info message from the project
	void SimulationInfo(const std::string& message, const InfoMessageReason& reason);

private:
	QAction*                                    m_action_run;
	QAction*                                    m_action_single_step;
	EnabledActions                              m_enabled_actions;
	std::shared_ptr<Ws::IProject>               m_project;
};

} // namespace Controller
} // namespace Gui
} // namespace SimShell

#endif
