/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * ProjectController implementation.
 */

#include "ProjectController.h"

#include <QAction>

#include "common/PTreeQtState.h"
#include "gui/PTreeContainer.h"
#include "gui/PTreeContainerPreferencesObserver.h"
#include "gui/PTreeMenu.h"
#include "util/Exception.h"
#include "AppController.h"


using namespace std;
using namespace boost::property_tree;
using namespace UA_CoMP::Util;

namespace SimShell {
namespace Gui {
namespace Controller {

ProjectController::ProjectController(AppController* m)
	: QWidget(m),
	  HasUnsavedChangesPrompt("Project"),
	  m_main_controller(m)
{
	InitActions();
	InitWidgets();
	InitBaseClasses();
}

QAction* ProjectController::GetActionClose() const
{
	return m_a_close_project;
}

QMenu* ProjectController::GetExportMenu() const
{
	return m_m_export;
}

const std::string& ProjectController::GetOpenedFileName() const
{
	return m_file_name;
}

const std::string& ProjectController::GetOpenedProjectName() const
{
	return m_project_name;
}

QDockWidget* ProjectController::GetParametersDock() const
{
	return m_container_parameters->GetDock();
}

QMenu* ProjectController::GetParametersMenu() const
{
	return m_container_parameters->GetMenu();
}

QDockWidget* ProjectController::GetPreferencesDock() const
{
	return m_container_preferences->GetDock();
}

QMenu* ProjectController::GetPreferencesMenu() const
{
	return m_container_preferences->GetMenu();
}

ptree ProjectController::GetPTreeState() const
{
	ptree state;
	state.put_child("container_parameters", m_container_parameters->GetPTreeState());
	state.put_child("container_preferences", m_container_preferences->GetPTreeState());
	return state;
}

QMenu* ProjectController::GetSimulationMenu() const
{
	return m_m_simulation;
}

ProjectController::Timings ProjectController::GetTimings() const
{
	return m_timings;
}

QMenu* ProjectController::GetViewersMenu() const
{
	return m_m_viewers;
}

void ProjectController::InitActions()
{
	m_a_close_project = new QAction("&Close", this);
	m_a_close_project->setShortcut(QKeySequence("Ctrl+W"));
	m_a_close_project->setEnabled(false);
	connect(m_a_close_project, SIGNAL(triggered()), this, SLOT(SLOT_Close()));
	m_enabled_actions.Add(m_a_close_project);
}

void ProjectController::InitBaseClasses()
{
	SetChildren({
		m_container_parameters.get(),
		m_container_preferences.get()
	});
}

void ProjectController::InitWidgets()
{
	m_container_parameters = make_shared<PTreeContainer>("Parameters");
	connect(m_container_parameters.get(), SIGNAL(Applied(const boost::property_tree::ptree&)),
		this, SLOT(SLOT_ApplyParameters(const boost::property_tree::ptree&)));

	m_container_preferences = make_shared<PTreeContainerPreferencesObserver>("Project Preferences");
	m_container_preferences->SetOnlyEditData(true);
	connect(m_container_preferences.get(), SIGNAL(Applied(const boost::property_tree::ptree&)),
		this, SLOT(SLOT_ApplyPreferences(const boost::property_tree::ptree&)));

	m_m_export = new QMenu("&Export", this);
	m_m_export->setEnabled(false);
	m_enabled_actions.Add(m_m_export);

	m_m_viewers = new QMenu("Viewers", this);
	m_m_viewers->setEnabled(false);
	m_enabled_actions.Add(m_m_viewers);

	m_m_simulation = new QMenu("&Simulation", this);
	m_m_simulation->addAction(m_running_controller.GetActionSingleStep());
	m_m_simulation->addAction(m_running_controller.GetActionRun());
	m_m_simulation->addSeparator();
}

void ProjectController::InternalForceClose()
{
	m_timings.Merge(m_project->Session().GetTimings());
	m_enabled_actions.Disable();
	m_running_controller.Disable();
	m_project->Close();
	m_project->Subject<Ws::Event::PreferencesChanged,
		weak_ptr<const void>>::Unregister(m_container_preferences);

	// Clear menus
	for (auto action : m_sim_actions->GetActions())
		m_m_simulation->removeAction(action);
	//m_m_viewers->clear();
	m_m_export->clear();

	m_export_actions.reset();
	m_sim_actions.reset();
	m_viewer_actions.reset();

	m_root_viewer.reset();

	// No need to Close() m_container_parameters and m_container_preferences,
	// they are present in the list of children in HasUnsavedChanges base class.

	m_project.reset();
	m_file_name.clear();
	m_project_name.clear();
}

bool ProjectController::InternalIsClean() const
{
	return true;
}

void ProjectController::InternalPreForceClose()
{
	m_project->SetUserData("project_controller", GetPTreeState());
}

bool ProjectController::InternalSave()
{
	return true;
}

bool ProjectController::IsOpened() const
{
	return (bool) m_project;
}

bool ProjectController::IsRunning() const
{
	return m_running_controller.IsRunning();
}

bool ProjectController::Set(const string& project_name, const shared_ptr<Ws::IProject>& project)
{
	if (!PromptClose())
		return false;
	try {
		m_project = project;

		m_project_name = project_name;
		m_file_name = project->GetSessionFileName();

		// Open ptrees in dock windows and menus.
		m_container_parameters->Open(m_project->Session().GetParameters());
		m_container_preferences->Open(m_project->GetPreferences());

		// Create actions for open project (Export, Simulation-specific, Viewers, ...)
		m_root_viewer = m_project->Session().CreateRootViewer(m_main_controller);

		// Project actions
		m_export_actions = make_shared<ProjectActions::ExportActions>(m_project,
					m_project->Session().GetExporters(), m_main_controller);
		m_viewer_actions = ProjectActions::ViewerActions::Create(m_root_viewer);
		m_sim_actions = make_shared<ProjectActions::SimActions>(m_project->Session().GetSimActions());

		// Copy actions into export menu
		for (auto action : m_export_actions->GetActions())
			m_m_export->addAction(action);

		m_m_viewers->clear();
		// Copy actions into viewer menu
		for (auto action : m_viewer_actions->GetMenu()->actions())
			m_m_viewers->addAction(action);

		if (m_m_viewers->actions().size() == 0) {
			auto action = m_m_viewers->addAction("No viewers");
			action->setEnabled(false);
		}

		// Copy actions into simulation menu
		for (auto action : m_sim_actions->GetActions()) {
			m_m_simulation->addAction(action);
		}

		// Register preferences container with PreferencesChanged events coming from workspace project.
		auto handler = bind(&PTreeContainerPreferencesObserver::Update,
				m_container_preferences.get(), std::placeholders::_1);
		m_project->Subject<Ws::Event::PreferencesChanged,
				weak_ptr<const void>>::Register(m_container_preferences, handler);

		// Enable RunningController's actions
		m_running_controller.Enable(m_project);

		m_enabled_actions.Enable();

		SetPTreeState(m_project->GetUserData("project_controller"));

		return true;
	}
	catch (Exception&) {
		return false;
	}
}

void ProjectController::SetPTreeState(const ptree& state)
{
	auto container_parameters_state = state.get_child_optional("container_parameters");
	if (container_parameters_state) {
		m_container_parameters->SetPTreeState(container_parameters_state.get());
	}

	auto container_preferences_state = state.get_child_optional("container_preferences");
	if (container_preferences_state) {
		m_container_preferences->SetPTreeState(container_preferences_state.get());
	}
}

void ProjectController::SetRunning(bool b)
{
	m_running_controller.SetRunning(b);
}

ProjectController::operator bool() const
{
	return IsOpened();
}

void ProjectController::SLOT_ApplyParameters(const ptree& p)
{
	m_project->Session().SetParameters(p);
}

void ProjectController::SLOT_ApplyPreferences(const ptree& p)
{
	m_project->SetPreferences(p);
}

void ProjectController::SLOT_Close()
{
	PromptClose();
}

} // namespace Controller
} // namespace Gui
} // namespace SimShell
