/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2).
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PanAndZoomView
 *
 * @author	Coding with(out) Discipline
 */

#include "PanAndZoomView.h"

#include <cmath>
#include <QApplication>
#include <QEvent>
#include <QKeyEvent>
#include <QWheelEvent>

namespace SimShell {
namespace Gui {

PanAndZoomView::PanAndZoomView(double zoomMin, double zoomMax, QWidget *parent)
	: QGraphicsView(parent), m_zoom_min(zoomMin), m_zoom_max(zoomMax)
{
	setTransformationAnchor(QGraphicsView::AnchorViewCenter);
	setResizeAnchor(QGraphicsView::AnchorViewCenter);
}

PanAndZoomView::~PanAndZoomView()
{
}

void PanAndZoomView::ScaleView(double factor)
{
	double zoomX = transform().m11() * factor;
	double zoomY = transform().m22() * factor;

	if ((zoomX >= m_zoom_min) && (zoomY >= m_zoom_min) && (zoomX <= m_zoom_max) && (zoomY <= m_zoom_max)) {
		scale(factor, factor);
	}
}

void PanAndZoomView::ResetZoom() {
	scale(1 / transform().m11(), 1 / transform().m22());
}

void PanAndZoomView::keyPressEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_Control) {
		setDragMode(QGraphicsView::ScrollHandDrag);
	}

	QGraphicsView::keyPressEvent(event);
}

void PanAndZoomView::keyReleaseEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_Control) {
		setDragMode(QGraphicsView::NoDrag);
	}

	QGraphicsView::keyReleaseEvent(event);
}


void PanAndZoomView::enterEvent(QEvent *event) {
	if (QApplication::keyboardModifiers().testFlag(Qt::ControlModifier)) {
		setDragMode(QGraphicsView::ScrollHandDrag);
	}
	else {
		setDragMode(QGraphicsView::NoDrag);
	}

	setFocus();

	QGraphicsView::enterEvent(event);
}

double PanAndZoomView::Scaling()
{
	return transform().m11();
}

void PanAndZoomView::wheelEvent(QWheelEvent *event)
{
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	ScaleView(std::pow(2, -event->delta() / 240.0));
	setTransformationAnchor(QGraphicsView::AnchorViewCenter);
}

} // namespace Gui
} // namespace SimShell
