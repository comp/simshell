/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for MyGraphicsView.
 */

#include "MyGraphicsView.h"

#include <cmath>
#include <QWheelEvent>


namespace SimShell {
namespace Gui {

using namespace std;

MyGraphicsView::MyGraphicsView(std::shared_ptr<QGraphicsScene> s, QWidget* parent)
	: QGraphicsView(s.get(), parent), m_scene(s)
{
	setInteractive(true); // required for zooming
}

MyGraphicsView::MyGraphicsView(QWidget* parent)
	: QGraphicsView(parent)
{
	setInteractive(true); // required for zooming
}

void MyGraphicsView::wheelEvent(QWheelEvent* event)
{
	double scaleFactor = pow(2.0, -event->delta() / 240.0);
	double factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
	if (factor > 0.07 && factor < 100.0)
	{
		scale(scaleFactor, scaleFactor);
	}
}

} // end of namespace Gui
} // end of namespace SimShell
