#ifndef SIMSHELL_GUI_LOGWINDOW_H_INCLUDED
#define SIMSHELL_GUI_LOGWINDOW_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Header for LogWindow.
 */

#include <functional>
#include "ViewerDockWidget.h"

class QPlainTextEdit;
class QWidget;

namespace SimShell {
namespace Gui {

/**
 * A viewer that displays sim events in a log in a dock window.
 */
class LogWindow : public QDockWidget
{
public:
	LogWindow(QWidget* parent = nullptr);

	virtual ~LogWindow();

	void AddLine(const std::string& line);

private:
	QPlainTextEdit*  m_log_widget;
	QStringList      m_lines;
};

} // namespace Gui
} // namespace SimShell

#endif
