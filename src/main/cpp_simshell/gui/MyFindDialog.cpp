/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for MyFindDialog.
 */

#include "MyFindDialog.h"

#include <QVBoxLayout>
#include <QHBoxLayout>

namespace SimShell {
namespace Gui {
MyFindDialog::MyFindDialog(QWidget* parent)
	: QDialog(parent)
{
	m_label = new QLabel("Search for:", this);
	m_input = new QLineEdit;
	m_label->setBuddy(m_input);

	m_match_case = new QCheckBox("Match case");

	m_close_button = new QPushButton("Close");
	m_find_button  = new QPushButton("Find");
	m_find_button->setDefault(true);
	m_find_button->setAutoDefault(true);

	QHBoxLayout* input_layout = new QHBoxLayout;
	input_layout->addWidget(m_label);
	input_layout->addWidget(m_input);

	QHBoxLayout* button_layout = new QHBoxLayout;
	button_layout->addWidget(m_close_button);
	button_layout->addWidget(m_find_button);

	QVBoxLayout* layout = new QVBoxLayout;
	layout->addLayout(input_layout);
	layout->addWidget(m_match_case);
	layout->addLayout(button_layout);

	setLayout(layout);
	setWindowTitle("Find");
	setModal(false);

	connect(m_close_button, SIGNAL(clicked()), this, SLOT(close()));
	connect(m_find_button, SIGNAL(clicked()), this, SLOT(Find()));
}

void MyFindDialog::CorrectFocus()
{
	m_input->setFocus();
	m_find_button->setDefault(true);
}

void MyFindDialog::Find()
{
	emit FindNext(m_input->text(), m_match_case->isChecked());
}

} // namespace Gui
} // namespace SimShell
