#ifndef GUI_HASUNSAVEDCHANGES_H_INCLUDED
#define GUI_HASUNSAVEDCHANGES_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for HasUnsavedChanges.
 */

#include <string>
#include <vector>

namespace SimShell {
namespace Gui {

/**
 * Abstract class that represents the ability be in closed or opened state, and, if the latter, in clean or dirty state.
 * There are methods for querying the state, and quietly saving or discarding (if in opened state).
 *
 * Objects of this type can also have children of the same type, to represent a hierarchical tree of closed/opened/clean/dirty objects.
 * Children of this object can only be in opened state if this object is in opened state.
 * Therefore, this object can only be closed if all of its children are closed first.
 * This has the consequence that if one of this object's children are in 'dirty' state, a user confirmation might be necessary before being able close this object.
 *
 * @see HasUnsavedChangesPrompt  SaveChangesDialog
 */
class HasUnsavedChanges
{
public:
	typedef std::vector<HasUnsavedChanges*> ChildrenType;

	/// @param title  Title for this window. Displayed in "Save Changes?" dialog.
	HasUnsavedChanges(std::string&& title);

	/// Virtual destructor.
	virtual ~HasUnsavedChanges();

	/// Get iterator to first child.
	ChildrenType::iterator begin();

	/// Get iterator to first child.
	ChildrenType::const_iterator begin() const;

	/// Get iterator to one position after last child.
	ChildrenType::iterator end();

	/// Get iterator to one position after last child.
	ChildrenType::const_iterator end() const;

	/// Force this object and its children to go back to unopened state so it can be safely deleted.
	void ForceClose();

	/// Get title of this object.
	const std::string& GetTitle();

	/// @return Whether this widget and all of its children are in clean state.
	bool IsClean() const;

	/// @return Whether this widget (ignoring its children) is in opened state.
	virtual bool IsOpened() const = 0;

	/// Try to save this widget's (and its children's) changes.
	/// @return whether we are in clean state.
	bool Save();

	/// Try to save this object (and its children) and, if successful, close them all.
	bool SaveAndClose();

protected:
	/// Add child.
	void AddChild(ChildrenType::value_type&& v);

	/// Set children.
	void SetChildren(ChildrenType&& c);

	/// Implementation should only close THIS widget, not its children.
	virtual void InternalForceClose() = 0;

	/// The result should be true only if THIS widget is in clean state.
	virtual bool InternalIsClean() const = 0;

	/// Additional things to do before closing children of this widget.
	virtual void InternalPreForceClose() {}

	/// Implementation should only save THIS widget.
	/// The result should be true only if THIS widget is in clean state.
	virtual bool InternalSave() = 0;

private:
	std::string    m_title;
	ChildrenType   m_children;
};

class HasUnsavedChangesDummy : public HasUnsavedChanges
{
public:
	HasUnsavedChangesDummy() : HasUnsavedChanges("Dummy"), m_opened(true) {}

	void SetChildren(ChildrenType&& c) { HasUnsavedChanges::SetChildren(move(c)); }

	virtual bool IsOpened() const { return m_opened; }

protected:
	virtual void InternalForceClose() { m_opened = false; }
	virtual bool InternalIsClean() const { return true; }
	virtual bool InternalSave() { return true; }

private:
	bool m_opened;
};

} // namespace Gui
} // namespace SimShell

#endif
