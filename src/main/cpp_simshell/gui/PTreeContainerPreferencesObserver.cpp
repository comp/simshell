/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PTreeContainerPreferencesObserver.
 */

#include "PTreeContainerPreferencesObserver.h"
#include <QMessageBox>

namespace SimShell {
namespace Gui {

PTreeContainerPreferencesObserver::PTreeContainerPreferencesObserver(QString const & title, QWidget* parent)
	: PTreeContainer(title, parent) {}

PTreeContainerPreferencesObserver::~PTreeContainerPreferencesObserver()
{
}

void PTreeContainerPreferencesObserver::Update(const Ws::Event::PreferencesChanged& e)
{
	if (!IsClean()) {
		// TODO: (extremely low priority since this case will never occur) Make it possible for the user to save his unsaved changes elsewhere.
		if (QMessageBox::warning(this, "Preferences updated",
			"Another entity has updated the preferences contained in this window to "
			"a newer version. You have unsaved changes. Do you want to discard them "
			"and use the most recent version, or keep your changes and ignore the"
			"newer version?",
			QMessageBox::Discard | QMessageBox::Ignore, QMessageBox::Discard)
				== QMessageBox::Ignore)
		{
			return;
		}
	}

	auto state = GetPTreeState();
	ForceClose();
	Open(e.GetPreferences());
	SetPTreeState(state);
}

} // namespace Gui
} // namespace SimShell

