/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for WorkspaceWizard.
 */

#include "WorkspaceWizard.h"

#include <QDir>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QVariant>
#include <QWizardPage>

#include "workspace/IWorkspaceFactory.h"
#include "workspace/IWorkspace.h"
#include "util/Exception.h"


namespace SimShell {
namespace Gui {

using namespace std;
using namespace Ws;
using namespace UA_CoMP::Util;

WorkspaceWizard::WorkspaceWizard(const shared_ptr<IWorkspaceFactory>& f, QWidget* parent)
	: QWizard(parent), m_workspace_factory(f)
{
	setPage(Page_Path, new PathPage(m_workspace_factory));
	setPage(Page_Existing, new ExistingPage(m_workspace_factory));
	setPage(Page_Init, new InitPage(m_workspace_factory));
	setPage(Page_Done, new DonePage(m_workspace_factory));
	setWindowTitle("Workspace Setup");
}

string WorkspaceWizard::GetWorkspaceDir() const
{
	if (field("workspace_path").isValid()) {
		return field("workspace_path").toString().toStdString();
	} else {
		return string();
	}
}

WorkspaceWizard::PathPage::PathPage(const shared_ptr<IWorkspaceFactory>& f)
	: m_workspace_factory(f)
{
	setTitle("Workspace Setup");
	setSubTitle("Please specify a workspace path.");

	dialog = 0;

	QLabel* label = new QLabel("Path");
	QLineEdit* edit_path = new QLineEdit;
	edit_path->setText(QDir::homePath() + '/' + QString::fromStdString(m_workspace_factory->GetDefaultWorkspaceName()));
	label->setBuddy(edit_path);
	QPushButton* button_browse = new QPushButton("Browse...");

	QHBoxLayout* layout = new QHBoxLayout;
	layout->addWidget(label);
	layout->addWidget(edit_path);
	layout->addWidget(button_browse);
	setLayout(layout);

	registerField("workspace_path", edit_path);
	connect(button_browse, SIGNAL(clicked()), this, SLOT(showBrowseDialog()));
}

int WorkspaceWizard::PathPage::nextId() const
{
	int id = 0;
	string workspace_path = field("workspace_path").toString().toStdString();
	try {
		m_workspace_factory->CreateWorkspace(workspace_path); // may throw
		id = Page_Existing;
	}
	catch (Exception &) {
		id = Page_Init;
	}
	return id;
}

void WorkspaceWizard::PathPage::showBrowseDialog()
{
	if (dialog) {
		dialog->exec();
	} else {
		dialog = new QFileDialog(this, "Browse", QDir::homePath());
		dialog->setAcceptMode(QFileDialog::AcceptOpen);
		dialog->setFileMode(QFileDialog::Directory);
		dialog->setOptions(QFileDialog::ShowDirsOnly);
		connect(dialog, SIGNAL(fileSelected(const QString &)), this, SLOT(pathSelected(const QString &)));
		dialog->exec();
	}
}

void WorkspaceWizard::PathPage::pathSelected(const QString& path)
{
	setField("workspace_path", path);
}

WorkspaceWizard::ExistingPage::ExistingPage(const shared_ptr<IWorkspaceFactory>& f)
	: m_workspace_factory(f)
{
	setTitle("Existing workspace");
	QLabel* label = new QLabel("This workspace contains the following projects:");
	projects = new QListWidget;
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget(label);
	layout->addWidget(projects);
	setLayout(layout);
}

void WorkspaceWizard::ExistingPage::initializePage()
{
	string workspace_path = field("workspace_path").toString().toStdString();
	auto workspace = m_workspace_factory->CreateWorkspace(workspace_path);
	projects->clear();
	for (auto& project : *workspace) {
		projects->addItem(QString::fromStdString(project.first));
	}
	setSubTitle(workspace_path.c_str());
}

int WorkspaceWizard::ExistingPage::nextId() const
{
	return Page_Done;
}

WorkspaceWizard::InitPage::InitPage(const shared_ptr<IWorkspaceFactory>& f)
	: m_workspace_factory(f)
{
	setTitle("New workspace");
	directory_label = new QLabel("Given path is not an existing directory. Directory will be created for you.");
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget(new QLabel("A new workspace will be created at the given path."));
	layout->addWidget(directory_label);
	setLayout(layout);
}

void WorkspaceWizard::InitPage::initializePage()
{
	QString workspace_path = field("workspace_path").toString();
	setSubTitle(workspace_path);
	QDir d(workspace_path);
	directory_label->setVisible(!d.exists());
}

int WorkspaceWizard::InitPage::nextId() const
{
	return Page_Done;
}

bool WorkspaceWizard::InitPage::validatePage()
{
	// Actually initialize workspace now
	// Create directory if it doesn't exist
	QString workspace_path = field("workspace_path").toString();
	QDir d(workspace_path);
	if (!d.exists()) {
		if (!d.mkpath(workspace_path)) {
			QMessageBox::warning(this, "Error", "Could not create directory.");
			return false;
		}
	}
	try {
		m_workspace_factory->InitWorkspace(workspace_path.toStdString());
	} catch (Exception& e) {
		QMessageBox::warning(this, "Error", QString("Could not initialize workspace: ") + e.what());
		return false;
	}
	return true;
}

WorkspaceWizard::DonePage::DonePage(const shared_ptr<IWorkspaceFactory>& f)
	: m_workspace_factory(f)
{
	setTitle("All Done!");
	check_default = new QCheckBox("Remember as default workspace.");
	check_default->setChecked(true);
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget(check_default);
	setLayout(layout);
}

void WorkspaceWizard::DonePage::initializePage()
{
	QString workspace_path = field("workspace_path").toString();
	setSubTitle(workspace_path);
}

int WorkspaceWizard::DonePage::nextId() const
{
	return -1;
}

bool WorkspaceWizard::DonePage::validatePage()
{
	QString workspace_path = field("workspace_path").toString();
	if (check_default->isChecked()) {
		QSettings settings;
		settings.setValue("workspace", workspace_path);
	}
	return true;
}

} // end of namespace Gui
} // end of namespace SimShell
