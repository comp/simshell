#ifndef WORKSPACE_WIZARD_H_INCLUDED
#define WORKSPACE_WIZARD_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for WorkspaceWizard.
 */

#include <memory>
#include <QWizard>
#include <QFileDialog>
#include <QListWidget>
#include <QComboBox>
#include <QLabel>
#include <QCheckBox>

namespace SimShell {

namespace Ws {
	class IWorkspaceFactory;
}

namespace Gui {

/**
 * A wizard that assists the user either specifying an existing workspace,
 * or creating a new workspace.
 */
class WorkspaceWizard : public QWizard
{
	Q_OBJECT
public:
	WorkspaceWizard(const std::shared_ptr<Ws::IWorkspaceFactory>&, QWidget * parent = 0);

	/**
	 * After WorkspaceWizard::exec() returns QDialog::Accepted,
	 * this function will return workspace directory selected by user.
	 * @return Path to workspace selected by user.
	 */
	std::string GetWorkspaceDir() const;

private:
	class PathPage;
	class ExistingPage;
	class InitPage;
	class DonePage;

	enum { Page_Path, Page_Existing, Page_Init, Page_Done };

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

class WorkspaceWizard::PathPage : public QWizardPage
{
	Q_OBJECT
public:
	PathPage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~PathPage() {}

	virtual int nextId() const;

private slots:
	void showBrowseDialog();
	void pathSelected(const QString &);

private:
	QFileDialog* dialog;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;

};

/// Page shown when user specified an existing workspace
class WorkspaceWizard::ExistingPage : public QWizardPage
{
	Q_OBJECT
public:
	ExistingPage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~ExistingPage() {}

	virtual void initializePage();
	virtual int nextId() const;

private:
	QListWidget* projects;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

/// Page shown when user specified a path that doesn't contain a workspace already
class WorkspaceWizard::InitPage : public QWizardPage
{
	Q_OBJECT
public:
	InitPage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~InitPage() {}

	virtual void initializePage();
	virtual int nextId() const;

protected:
	virtual bool validatePage();

private:
	QLabel* directory_label;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

class WorkspaceWizard::DonePage : public QWizardPage
{
	Q_OBJECT
public:
	DonePage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~DonePage() {}

	virtual void initializePage();
	virtual int nextId() const;

signals:
	void done(std::string const & workspace_dir);

protected:
	virtual bool validatePage();

private:
	QCheckBox* check_default;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif
