#ifndef PAN_AND_ZOOM_VIEW_H_INCLUDED
#define PAN_AND_ZOOM_VIEW_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2).
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PanAndZoomView
 *
 * @author	Coding with(out) Discipline
 */

#include <QGraphicsView>

namespace SimShell {
namespace Gui {

/**
 * QGraphicsView with the ability to pan and zoom.
 */
class PanAndZoomView : public QGraphicsView
{
public:

	/**
	 * Constructor
	 *
	 * @param	zoomMin		The minimum zooming factor
	 * @param	zoomMax		The maximum zooming factor
	 * @param	parent		The widget's parent
	 */
	PanAndZoomView(double zoomMin = 0.07, double zoomMax = 100.0, QWidget *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~PanAndZoomView();

	/**
	 * Scales the view, bounded by the minimum and maximum zooming factor
	 * @param	factor	The scaling factor
	 */
	void ScaleView(double factor);

	/**
	 * Gets the current scaling of the view
	 * (assuming the view was only scaled with ScaleView() and the
	 * horizontal and vertical scaling are the same)
	 * @param	double	The scaling factor of the view
	 */
	double Scaling();

	/**
	 * Resets the view to the normal zoom level
	 */
	void ResetZoom();

	/**
	 * Overriding keyPressEvent to detect when control key is pressed
	 * @param	event	The key event
	 */
	virtual void keyPressEvent(QKeyEvent *event);

	/**
	 * Overriding releasePressEvent to detect when control key is released
	 * @param	event	The key event
	 */
	virtual void keyReleaseEvent(QKeyEvent *event);

	/**
	 * Overriding enterEvent to grab focus when the view is entered
	 * @param	event	The key event
	 */
	virtual void enterEvent(QEvent *event);

	/**
	 * Overriding wheelEvent to scroll the view when control key is held down
	 * @param	event
	 */
	virtual void wheelEvent(QWheelEvent *event);

private:
	double m_zoom_min;
	double m_zoom_max;
};

} // namespace Gui
} // namespace SimShell

#endif // end-of-include-guard
