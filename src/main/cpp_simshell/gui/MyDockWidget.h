#ifndef GUI_MYDOCKWIDGET_H_INCLUDED
#define GUI_MYDOCKWIDGET_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Header for MyDockWidget.
 */

#include <boost/property_tree/ptree.hpp>
#include <QDockWidget>
#include "IHasPTreeState.h"

namespace SimShell {
namespace Gui {

/**
 * QDockWidget with methods to import/export widget layout (x,y,width, ...) in ptree format.
 *
 * @see QDockWidget IHasPTreeState
 */
class MyDockWidget : public QDockWidget, public IHasPTreeState
{
	Q_OBJECT
public:
	MyDockWidget(QWidget* parent = 0);

	/// @see IHasPTreeState::GetPTreeState().
	virtual boost::property_tree::ptree GetPTreeState() const;

	/// @see IHasPTreeState::SetPTreeState().
	virtual void SetPTreeState(const boost::property_tree::ptree&);
};

} // Gui
} // SimShell

#endif
