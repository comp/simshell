#ifndef GUI_PROJECTACTIONS_EXPORTACTIONS_H_INCLUDED
#define GUI_PROJECTACTIONS_EXPORTACTIONS_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * ExportActions header.
 */

#include <map>
#include <vector>
#include <QObject>

#include "session/ISession.h"
#include "workspace/IProject.h"

class QAction;
class QWidget;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

/**
 * For a given Session::ISession::ExportersType instance, generates a list of actions
 * that will trigger file "Export" to happen.
 * This object maintains ownership of those actions, so a long as they are available
 * to the user (e.g. added to a menu), this object should NOT be destroyed.
 * Such a trigger of "Export" will cause a file dialog window to be shown, so the
 * user can choose where to save the exported file.
 */
class ExportActions : public QObject
{
	Q_OBJECT
public:
	ExportActions(const std::shared_ptr<Ws::IProject>&,
		const Session::ISession::ExportersType&,
		QWidget* parent = nullptr);

	/// Virtual destructor.
	virtual ~ExportActions();

	/// Get actions.
	/// This class keeps ownership over the returned objects!
	const std::vector<QAction*>& GetActions() const;

private slots:
	void SLOT_Trigger();

private:
	std::shared_ptr<Ws::IProject>           m_project;                ///< Project
	QWidget*                                m_parent;                 ///<
	std::vector<QAction*>                   m_actions;                ///< List of QAction objects associated with Export callbacks.
	std::map<QObject*, std::pair<std::string, Session::ISession::ExporterType>>
	                                        m_callback_map;           ///< Mapping from QAction objects to file extension and Export callback.
};

} // namespace ProjectActions
} // namespace Gui
} // namespace SimShell

#endif
