#ifndef GUI_PROJECTACTIONS_SIMACTIONS_H_INCLUDED
#define GUI_PROJECTACTIONS_SIMACTIONS_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * SimActions header.
 */
#include <map>
#include <vector>
#include <QObject>
#include "session/ISession.h"

class QAction;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

/**
 * Given a Session::ISession::SimActionsType instance, will generate a list of 'simulation-specific' actions.
 * This object maintains ownership of those actions, so a long as they are available to the user (e.g. added to a menu), this object should NOT be destroyed.
 */
class SimActions : public QObject
{
	Q_OBJECT
public:
	SimActions(const Session::ISession::SimActionsType&);

	/// Virtual destructor.
	virtual ~SimActions();

	/// Get actions.
	/// This class keeps ownership over returned objects.
	const std::vector<QAction*>& GetActions() const;

private slots:
	void SLOT_Trigger();

private:
	std::vector<QAction*>                   m_actions;                ///< List of QAction objects associated with Export callbacks.
	std::map<QObject*, Session::ISession::SimpleCallbackType>
	                                        m_callback_map;    ///< Mapping from QAction objects to file extension and Export callback.
};

} // namespace ProjectActions
} // namespace Gui
} // namespace SimShell

#endif
