#ifndef GUI_PROJECTACTIONS_VIEWERACTIONS_H_INCLUDED
#define GUI_PROJECTACTIONS_VIEWERACTIONS_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * ViewerActions header.
 */

#include <map>
#include <memory>
#include <vector>
#include <QObject>
#include "viewer/IViewerNode.h"

class QAction;
class QMenu;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

/**
 * Given a so-called 'root' viewer instance, constructs a menu representing the hierarchical structure of viewers.
 * The menu has actions for enabling/disabling viewers at any level in the hierarchy.
 * This object maintains ownership of those actions, so a long as they are available to the user (e.g. added to a menu), this object should NOT be destroyed.
 */
class ViewerActions : public QObject
{
	Q_OBJECT
public:
	/// Construct instance of this class.
	/// @param root_viewer   Viewer instance acting as the root of the tree of viewers.
	static std::shared_ptr<ViewerActions> Create(const std::shared_ptr<Viewer::IViewerNode>& root_viewer);

	virtual ~ViewerActions();

	/// Get menu.
	/// This class keeps ownership over returned object.
	QMenu* GetMenu() const;

private slots:
	void SLOT_Toggle(bool);

private:
	ViewerActions(const std::shared_ptr<Viewer::IViewerNode>& root_viewer);

	QMenu*                                                     m_menu;         ///< List of QAction objects associated with Export callbacks.
	std::map<QObject*, std::shared_ptr<Viewer::IViewerNode>>   m_node_map;     ///< Mapping from QAction objects to callbacks.
	std::map<QObject*, std::vector<QAction*>>                  m_children_map; ///< For disabling viewers that depend on other viewers when those viewers are disabled.
};

} // namespace ProjectActions
} // namespace Gui
} // namespace SimShell


#endif
