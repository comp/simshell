/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * SimActions implementation.
 */

#include "SimActions.h"

#include <QAction>

using namespace std;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

SimActions::SimActions(const Session::ISession::SimActionsType& sim_actions)
{
	for (auto& sim_action : sim_actions) {
		QAction* a = new QAction(QString::fromStdString(sim_action.first), this);
		m_actions.push_back(a);
		m_callback_map[a] = sim_action.second;
		connect(a, SIGNAL(triggered()), this, SLOT(SLOT_Trigger()));
	}
}

SimActions::~SimActions()
{
}

const std::vector<QAction*>& SimActions::GetActions() const
{
	return m_actions;
}

void SimActions::SLOT_Trigger()
{
	(m_callback_map[sender()])();
}

} // namespace ProjectActions
} // namespace Gui
} // namespace SimShell
