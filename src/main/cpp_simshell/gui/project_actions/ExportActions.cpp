/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * ExportActions implementation.
 */

#include "ExportActions.h"

#include <QAction>
#include <QFileDialog>
#include <QMessageBox>

using namespace std;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

ExportActions::ExportActions(
		const shared_ptr<Ws::IProject>& project,
		const Session::ISession::ExportersType& exporters,
		QWidget* parent)
	: m_project(project), m_parent(parent)
{
	for (auto& exporter : exporters) {
		QAction* a = new QAction(QString::fromStdString(exporter.first) + " ...", this);
		m_actions.push_back(a);
		m_callback_map[a] = exporter;
		connect(a, SIGNAL(triggered()), this, SLOT(SLOT_Trigger()));
	}
}

ExportActions::~ExportActions()
{
}

const vector<QAction*>& ExportActions::GetActions() const
{
	return m_actions;
}

void ExportActions::SLOT_Trigger()
{
	auto& exp           = m_callback_map[sender()];
	auto& exp_name      = exp.first;
	auto& exp_extension = exp.second.extension;
	auto& exp_callback  = exp.second.callback;

	QString dir       = QString::fromStdString(m_project->GetPath());
	QString filters   = QString::fromStdString(exp_name) + " (*."
				+ QString::fromStdString(exp_extension) + ");;All Files (*)";
	QFileDialog* fd   = new QFileDialog(m_parent, "Export " + QString::fromStdString(exp_name),
				dir, filters);
	fd->setFileMode(QFileDialog::AnyFile);
	fd->setAcceptMode(QFileDialog::AcceptSave);

	if (fd->exec() == QDialog::Accepted) {
		QString path;
		QStringList paths = fd->selectedFiles();
		if (!paths.empty()) {
			path = paths.first();
		} else {
			QMessageBox::warning(m_parent, "Error", "No filename given!");
			emit static_cast<QAction*>(sender())->trigger();
			return;
		}

		// extract extension from filename
		QFileInfo fi(path);
		QString extension = fi.suffix();
		if (extension.isEmpty()) {
			extension = QString::fromStdString(exp_extension);
			path += ".";
			path += extension;
		}

		// Perform export action
		(exp_callback)(path.toStdString());
	}
}

} // namespace ProjectActions
} // namespace Gui
} // namespace SimShell
