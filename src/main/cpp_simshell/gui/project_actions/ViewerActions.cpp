/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * ViewerActions implementation.
 */

#include "ViewerActions.h"

#include <QAction>
#include <QMenu>

using namespace std;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

ViewerActions::ViewerActions(const shared_ptr<Viewer::IViewerNode>& root_node)
{
	function<QMenu*(const shared_ptr<Viewer::IViewerNode>&, QAction*)> init_recursive;
	init_recursive = [&](const shared_ptr<Viewer::IViewerNode>& root_node, QAction* parent_action)
		-> QMenu*
	{
		QMenu* result = new QMenu("Viewers");
		for (auto& node : *root_node) {
			result->addSeparator();
			//result->addAction(QString::fromStdString(node.first))->setEnabled(false);
			QAction* enable_action = result->addAction(QString::fromStdString(node.first));
			enable_action->setCheckable(true);
			if (node.second->IsEnabled())
				enable_action->setChecked(true);
			enable_action->setEnabled(node.second->IsParentEnabled());
			m_node_map.insert({enable_action, node.second});
			m_children_map[parent_action].push_back(enable_action);
			connect(enable_action, SIGNAL(toggled(bool)), this, SLOT(SLOT_Toggle(bool)));
			auto children_menu = init_recursive(node.second, enable_action);
			if (children_menu->actions().size())
				result->addMenu(children_menu);
		}
		return result;
	};
	m_menu = init_recursive(root_node, nullptr);
}

ViewerActions::~ViewerActions()
{
}

shared_ptr<ViewerActions> ViewerActions::Create(const shared_ptr<Viewer::IViewerNode>& root_node)
{
	auto result = shared_ptr<ViewerActions>(new ViewerActions(root_node));

	for (auto& val : result->m_node_map) {
		val.second->Register(val.second, [&](const Viewer::Event::ViewerEvent& e) {
			auto type = e.GetType();
			bool checked = type == Viewer::Event::ViewerEvent::Enabled;
			static_cast<QAction*>(val.first)->setChecked(checked);
		});
	}

	return result;
}

QMenu* ViewerActions::GetMenu() const
{
	return m_menu;
}

void ViewerActions::SLOT_Toggle(bool checked)
{
	auto& node = m_node_map[sender()];

	if (checked) {
		node->Enable();
	} else {
		node->Disable();
	}

	function<void(vector<QAction*>& children)> work_on_children;
	work_on_children = [&](vector<QAction*>& children) {
		for (auto a : children) {
			a->setEnabled(m_node_map[a]->IsParentEnabled());
			work_on_children(m_children_map[a]);
		}
	};
	work_on_children(m_children_map[sender()]);
}

} // namespace ProjectActions
} // namespace Gui
} // namespace SimShell
