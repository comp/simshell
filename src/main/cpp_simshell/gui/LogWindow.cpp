/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for LogWindow.
 */

#include "LogWindow.h"

#include <sstream>
#include <QPlainTextEdit>

using namespace std;
using namespace UA_CoMP::Util;

namespace SimShell {
namespace Gui {

LogWindow::LogWindow(QWidget* parent)
	: QDockWidget(parent)
{
	m_log_widget = new QPlainTextEdit(this);
	m_log_widget->setReadOnly(true);
	m_log_widget->setMaximumBlockCount(100);
	m_log_widget->setCenterOnScroll(true);
	setWindowTitle("Log");
	setWidget(m_log_widget);
	show();
	raise();
}

LogWindow::~LogWindow()
{
}

void LogWindow::AddLine(const string& line)
{
	m_log_widget->appendPlainText(QString::fromStdString(line));
}

} // namespace Gui
} // namespace SimShell
