/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for SaveChangesDialog.
 */

#include "SaveChangesDialog.h"

#include <functional>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QTreeView>
#include <QVBoxLayout>

#include "qtmodel/CheckableTreeModel.h"
#include "HasUnsavedChanges.h"


using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

SaveChangesDialog::SaveChangesDialog(const ptree& pt, QWidget* parent)
    : QDialog(parent)
{
	auto layout = new QVBoxLayout;
	auto label = new QLabel("The following windows contain unsaved changes:");

	layout->addWidget(label);
	auto treeview = new QTreeView(this);
	m_model = new CheckableTreeModel(pt, treeview);
	treeview->setModel(m_model);
	treeview->header()->hide();
	layout->addWidget(treeview);

	auto box = new QDialogButtonBox(Qt::Horizontal, this);
	m_save_button = box->addButton(QDialogButtonBox::Save);
	m_cancel_button = box->addButton(QDialogButtonBox::Cancel);
	m_discard_button = box->addButton(QDialogButtonBox::Discard);
	layout->addWidget(box);

	connect(box, SIGNAL(clicked(QAbstractButton*)), this, SLOT(SLOT_Clicked(QAbstractButton*)));

	setLayout(layout);
	setWindowTitle("Save changes?");
}

void SaveChangesDialog::SLOT_Clicked(QAbstractButton* button)
{
	if (button == m_save_button) {
		done(Save);
	} else if (button == m_cancel_button) {
		done(Cancel);
	} else if (button == m_discard_button) {
		done(Discard);
	}
}

bool SaveChangesDialog::Prompt(HasUnsavedChanges* widget, QWidget* parent)
{
	HasUnsavedChangesDummy dummy;
	dummy.SetChildren({widget});

	function<ptree(HasUnsavedChanges*)> work_recursive;
	auto unsaved_tree = (work_recursive = [&](HasUnsavedChanges* widget) -> ptree {
		ptree result;
		for (auto child : *widget) {
			if (child->IsOpened() && !child->IsClean()) {
				ptree child_pt;
				child_pt.put("checked", true);
				child_pt.put_child("children", work_recursive(child));
				result.put_child(child->GetTitle(), child_pt);
			}
		}
		return result;
	})(&dummy);

	if (unsaved_tree.size())
	{
		// Create "Save Changes?"-dialog.
		SaveChangesDialog dialog(unsaved_tree, parent);

		switch (dialog.exec()) {
		case Save: {
			// Save selected items
			function<bool(const ptree&, HasUnsavedChanges*)> work_recursive;
			if (!(work_recursive = [&](const ptree& pt, HasUnsavedChanges* i) {
				auto pt_it = pt.begin();
				auto i_it = i->begin();

				while (pt_it != pt.end())
				{
					auto& child_pt = pt_it->second;
					auto checked = child_pt.get<bool>("checked");
					if (checked) {
						if (!(*i_it)->Save())
							return false;
					}

					auto children_optional = child_pt.get_child_optional("children");
					if (children_optional) {
						if (!work_recursive(children_optional.get(), *i_it))
							return false;
					}

					pt_it++;
					i_it++;
				}
				return true;
			})(dialog.m_model->ToPTree(), &dummy)) {
				return false;
			}
			break;
		}
		case Discard:
			break;
		case Cancel:
			return false;
		}
	}

	dummy.ForceClose();
	return true;
}

} // end of namespace Gui
} // end of namespace SimShell
