/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for MyTreeView.
 */

#include "MyTreeView.h"
#include "common/PTreeQtState.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

MyTreeView::MyTreeView(QWidget* parent)
	: QTreeView(parent)
{
}

ptree MyTreeView::GetPTreeState() const
{
	ptree result;
	if (model()) {
		result.put_child("treeview", PTreeQtState::GetTreeViewState(this));
	}
	//result.put_child("widget", PTreeQtState::GetWidgetState(this));
	return result;
}

void MyTreeView::SetPTreeState(const ptree& state)
{
	if (model()) {
		auto treeview_state = state.get_child_optional("treeview");
		if (treeview_state) {
			PTreeQtState::SetTreeViewState(this, treeview_state.get());
		}
	}

	/*auto widget_state = state.get_child_optional("widget");
	if (widget_state) {
		PTreeQtState::SetWidgetState(this, widget_state.get());
	}*/
}

} // namespace Gui
} // namespace SimShell
