/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation of PTreeEditorWindow.
 */

#include "PTreeEditorWindow.h"

#include <QCloseEvent>
#include <QHeaderView>
#include <QMenuBar>
#include <QMessageBox>
#include <QStatusBar>
#include <QToolBar>

#include "qtmodel/PTreeModel.h"
#include "PTreeView.h"

namespace SimShell {
namespace Gui {

using std::string;
using boost::property_tree::ptree;

int const PTreeEditorWindow::g_column_width = 200;

PTreeEditorWindow::PTreeEditorWindow(QWidget* parent)
	: QMainWindow(parent), HasUnsavedChanges({}), m_only_edit_data(false), m_model(0)
{
	/* central widget */
	m_treeview = new PTreeView(this);
	m_treeview->SetOnlyEditData(m_only_edit_data);
	setCentralWidget(m_treeview);
	connect(m_treeview, SIGNAL(Edited()), this, SLOT(Apply()));
	connect(m_treeview, SIGNAL(StatusChanged(QString const&)), this, SIGNAL(StatusChanged(QString const&)));

	/* actions */
	m_action_show_toolbar_main = new QAction("Main Toolbar", this);

	/* borrow actions from treeview */
	QAction* action_find              = m_treeview->GetFindDialogAction();
	QAction* action_clear_highlight   = m_treeview->GetClearHighlightAction();
	QAction* action_undo              = m_treeview->GetUndoAction();
	QAction* action_redo              = m_treeview->GetRedoAction();
	QAction* action_cut               = m_treeview->GetCutAction();
	QAction* action_copy              = m_treeview->GetCopyAction();
	QAction* action_paste             = m_treeview->GetPasteAction();
	QAction* action_expand_all        = m_treeview->GetExpandAllAction();
	QAction* action_expand_none       = m_treeview->GetExpandNoneAction();

	m_action_show_toolbar_main->setCheckable(true);
	m_action_show_toolbar_main->setChecked(true);

	/* toolbar */
	m_toolbar_main = new QToolBar(this);
	m_toolbar_main->setToolButtonStyle(Qt::ToolButtonIconOnly);
	m_toolbar_main->setMovable(false);
	m_toolbar_main->setWindowTitle("Main toolbar");
	m_toolbar_main->toggleViewAction()->setEnabled(false);
	m_toolbar_main->setIconSize(QSize(16,16)); // save some space in our already crowded window
	// add actions
	m_toolbar_main->addAction(action_undo);
	m_toolbar_main->addAction(action_redo);
	m_toolbar_main->addSeparator();
	m_toolbar_main->addAction(action_cut);
	m_toolbar_main->addAction(action_copy);
	m_toolbar_main->addAction(action_paste);
	m_toolbar_main->addSeparator();
	m_toolbar_main->addAction(action_expand_all);
	m_toolbar_main->addAction(action_expand_none);
	m_toolbar_main->addSeparator();
	m_toolbar_main->addAction(action_find);
	m_toolbar_main->addAction(action_clear_highlight);
	addToolBar(m_toolbar_main);

	connect(m_action_show_toolbar_main, SIGNAL(toggled(bool)), m_toolbar_main, SLOT(setVisible(bool)));
}

void PTreeEditorWindow::Apply()
{
	if (m_edit_path.empty())
		m_pt = m_model->Store();
	else
		m_pt.put_child(m_edit_path, m_model->Store());
	m_treeview->SetClean();

	emit ApplyTriggered(m_pt);
}

std::string PTreeEditorWindow::GetEditPath() const {
	return m_edit_path;
}

ptree PTreeEditorWindow::GetPTreeState() const
{
	ptree result;
	result.put("edit_path", m_edit_path);
	result.put_child("treeview", m_treeview->GetPTreeState());
	return result;
}

void PTreeEditorWindow::InternalForceClose()
{
	if (!m_model)
		return; // we are already in unopened state

	// can't use QAbstractItemview::revert(), since it is non-virtual, so it doesn't emit canCopyChanged()
	m_treeview->setModel(nullptr);
	delete m_model;
	m_model = nullptr;
}

bool PTreeEditorWindow::InternalIsClean() const
{
	return m_treeview->IsClean();
}

bool PTreeEditorWindow::InternalSave()
{
	if (!m_treeview->IsClean())
		Apply();
	return true;
}

bool PTreeEditorWindow::IsOnlyEditData() const
{
	return m_only_edit_data;
}

bool PTreeEditorWindow::IsOpened() const
{
	return m_model;
}

bool PTreeEditorWindow::OpenPath(QString const & edit_path)
{
	if (!m_model)
		return false;

	if (edit_path.toStdString() == m_edit_path)
		return true; // don't change anything, but return success

	if (!m_treeview->IsClean()) {
		int result = QMessageBox::warning(this, windowTitle(), "Apply changes before opening different subtree?\n",
				QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::No);
		if (result == QMessageBox::Yes)
			Apply();
		else if (result == QMessageBox::Cancel)
			return false; // user changed his mind, edit path was not set
	}

	m_edit_path = edit_path.toStdString();
	m_treeview->setModel(0);
	delete m_model;
	m_model = new PTreeModel(m_pt.get_child(m_edit_path), this);
	m_model->SetOnlyEditData(m_only_edit_data);
	m_treeview->setModel(m_model);
	m_treeview->header()->resizeSection(0, g_column_width);
	return true; // success
}

bool PTreeEditorWindow::OpenPTree(ptree const & pt, QString const & edit_path)
{
	if (!SaveAndClose())
		return false; // must be in unopened state

	m_pt = pt;
	m_edit_path = edit_path.toStdString();
	m_model = new PTreeModel(m_pt.get_child(m_edit_path), this);
	m_model->SetOnlyEditData(m_only_edit_data);
	m_treeview->setModel(m_model);
	m_treeview->header()->resizeSection(0, g_column_width);

	return true;
}

void PTreeEditorWindow::Redo()
{
	m_treeview->GetRedoAction()->trigger();
}

void PTreeEditorWindow::SetOnlyEditData(bool b)
{
	m_only_edit_data = b;

	m_treeview->SetOnlyEditData(m_only_edit_data);

	if (m_model)
		m_model->SetOnlyEditData(m_only_edit_data);
}

void PTreeEditorWindow::SetPTreeState(const ptree& state)
{
	auto edit_path = state.get_optional<string>("edit_path");
	if (edit_path) {
		OpenPath(QString::fromStdString(edit_path.get()));
	}

	auto treeview = state.get_child_optional("treeview");
	if (treeview) {
		m_treeview->SetPTreeState(treeview.get());
	}
}

QSize PTreeEditorWindow::sizeHint() const
{
	return QSize(320, 600);
}

void PTreeEditorWindow::Undo()
{
	m_treeview->GetUndoAction()->trigger();
}

} // end of namespace Gui
} // end of namespace SimShell
