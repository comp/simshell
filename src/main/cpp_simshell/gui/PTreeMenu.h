#ifndef PTREE_MENU_H_INCLUDED
#define PTREE_MENU_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeMenu.
 */
#include <boost/property_tree/ptree.hpp>
#include <QMenu>
#include "gui/HasUnsavedChanges.h"

class QAction;
class QSignalMapper;

namespace SimShell {
namespace Gui {

/**
 * A menu reflecting the structure of a ptree.
 *
 * Given a ptree, this class constructs a hierarchical menu reflecting the ptree structure.
 * Each subtree can be selected, causing the menu to emit the ItemTriggered() signal.
 */
class PTreeMenu : public QMenu, public HasUnsavedChanges
{
	Q_OBJECT
public:
	/**
	 * @param title   Menu title. Will also be used as dock widget title.
	 * @param parent  Parent widget.
	 */
	PTreeMenu(QString const& title, QString const& path_prefix = QString(), QWidget* parent = 0);

	/// Destructor does not do a thing, except be virtual.
	virtual ~PTreeMenu();

	/// Test whether menu is in opened state.
	virtual bool IsOpened() const;

	/**
	 * Open a ptree. Menu will reflect hierarchical structure of the ptree.
	 * @param pt     PTree to display in menu.
	 * @param depth  Maximum depth of submenu hierarchy.
	 *               If depth = -1, there is no maximum depth.
	 */
	bool OpenPTree(boost::property_tree::ptree const& pt, int depth = -1);

signals:
	void ItemTriggered(QString const& subtree = QString());

protected:
	/// @see IHasUnsavedChanges
	virtual void InternalForceClose();

	/// @see IHasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see IHasUnsavedChanges
	virtual bool InternalSave();

private:
	///
	void AddAllAction();

	/// Recursive implementation of OpenPTree().
	void OpenPTreeInternal(boost::property_tree::ptree const& pt, int remaining_depth);

private:
	bool               m_opened;
	QAction*           m_action_all;        // the "All..."-action
	QString            m_path;
	QSignalMapper*     m_signal_mapper;

};

} // end of namespace Gui
} // end of namespace SimShell

#endif // end-of-include-guard
