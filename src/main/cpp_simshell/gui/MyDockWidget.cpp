/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * MyDockWidget implementation.
 */

#include "MyDockWidget.h"

#include "common/PTreeQtState.h"


using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

MyDockWidget::MyDockWidget(QWidget* parent)
	: QDockWidget(parent)
{
}

ptree MyDockWidget::GetPTreeState() const
{
	ptree result;
	result.put_child("widget", PTreeQtState::GetWidgetState(this));
	result.put_child("dock", PTreeQtState::GetDockWidgetState(this));
	return result;
}

void MyDockWidget::SetPTreeState(const ptree& state)
{
	auto widget_state = state.get_child_optional("widget");
	if (widget_state)
		PTreeQtState::SetWidgetState(this, widget_state.get());

	auto dock_state = state.get_child_optional("dock");
	if (dock_state) {
		PTreeQtState::SetDockWidgetState(this, dock_state.get());
	}
}

} // namespace Gui
} // namespace SimShell
