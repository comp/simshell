#ifndef GUI_ENABLEDACTIONS_H_INCLUDED
#define GUI_ENABLEDACTIONS_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * EnabledActions interface.
 */

#include <vector>

class QAction;
class QMenu;

namespace SimShell {
namespace Gui {

/**
 * Helper class to enable or disable a collection of QAction and QMenu objects at once.
 */
class EnabledActions
{
public:
	/// Add action object to collection.
	void Add(QAction* a);

	/// Add menu to collection.
	void Add(QMenu* m);

	/// Set action object collection.
	void Set(std::vector<QAction*> actions);

	/// Enable all actions and menus in collection.
	void Enable();

	/// Disable all actions and menus in collection.
	void Disable();

private:
	std::vector<QAction*>  actions;
	std::vector<QMenu*>    menus;
};

} // namespace Gui
} // namespace SimShell

#endif
