/*
 *  This file is part of the Virtualfile 2 (a.k.a. vfile2) software.
 *  Virtualfile 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  Virtualfile 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with Virtualfile 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for WorkspaceView.
 */

#include "WorkspaceView.h"

#include <QContextMenuEvent>
#include <QHeaderView>
#include <QInputDialog>
#include <QItemSelectionModel>
#include <QMenu>
#include <QMessageBox>

#include "qtmodel/WorkspaceQtModel.h"
#include "util/Exception.h"
#include "workspace/MergedPreferences.h"


namespace SimShell {
namespace Gui {

using namespace UA_CoMP::Util;
using namespace std;

WorkspaceView::WorkspaceView(QWidget* parent)
	: MyTreeView(parent),
	  model(nullptr),
	  project_widget(nullptr)
{
	action_open = new QAction(QIcon::fromTheme("document-open"), "Open", this);
	action_rename = new QAction("Rename...", this);
	action_rename->setShortcut(QKeySequence("F2"));
	action_rename->setShortcutContext(Qt::WidgetShortcut);
	action_remove = new QAction(QIcon::fromTheme("edit-delete"), "Remove", this);
	action_remove->setShortcut(QKeySequence("Del"));
	action_remove->setShortcutContext(Qt::WidgetShortcut);
	action_close = new QAction("Close", this);
	action_close->setShortcutContext(Qt::WidgetShortcut);

	// add actions so they can be triggered by shortcut when widget has focus.
	addAction(action_open);
	addAction(action_rename);
	addAction(action_remove);
	addAction(action_close);

	// all actions are disabled until some workspace is opened.
	action_open->setEnabled(false);
	action_rename->setEnabled(false);
	action_remove->setEnabled(false);
	action_close->setEnabled(false);

	connect(action_open, SIGNAL(triggered()), this, SLOT(SLOT_Open()));
	connect(action_rename, SIGNAL(triggered()), this, SLOT(SLOT_RenameDialog()));
	connect(action_remove, SIGNAL(triggered()), this, SLOT(SLOT_Remove()));
	connect(action_close, SIGNAL(triggered()), this, SLOT(SLOT_Close()));

	header()->hide();
	setAnimated(true);
}

void WorkspaceView::setModel(QAbstractItemModel* m)
{
	// disable all actions until a selection is made
	action_open->setEnabled(false);
	action_rename->setEnabled(false);
	action_remove->setEnabled(false);
	action_close->setEnabled(false);

	model = dynamic_cast<WorkspaceQtModel*>(m);

	QTreeView::setModel(model);

	if (model) {
		connect(selectionModel(), SIGNAL(selectionChanged(QItemSelection const &, QItemSelection const &)),
				this, SLOT(SLOT_SelectionChanged(QItemSelection const &, QItemSelection const &)));
	}
}

QAction* WorkspaceView::GetOpenAction() const
{
	return action_open;
}

QAction* WorkspaceView::GetRenameAction() const
{
	return action_rename;
}

QAction* WorkspaceView::GetRemoveAction() const
{
	return action_remove;
}

void WorkspaceView::SLOT_Close()
{
	QModelIndexList list = selectedIndexes();
	if (list.empty()) {
		return;
	}
	QModelIndex const& index = list.first();
	model->Close(index);
	action_rename->setEnabled(true);
}

void WorkspaceView::SLOT_Open()
{
	QModelIndexList list = selectedIndexes();
	if (list.empty()) {
		return;
	}
	QModelIndex const& index = list.first();
	try {
		model->Open(index);
		// if project was opened we can't rename it
		action_rename->setEnabled(false);
	} catch (exception& e) {
		QMessageBox::warning(this, "Could not open file", e.what());
	}
}

void WorkspaceView::SLOT_RenameDialog()
{
	QModelIndexList list = selectedIndexes();
	if (list.empty()) {
		return;
	}
	QModelIndex index = list.first();
	if (model->GetType(index) == WorkspaceQtModel::FileType) {
		index = model->parent(index);
	}
	string const old_name = model->GetName(index);
	bool ok;
	string const new_name = QInputDialog::getText(this, "Rename project", "Please enter a new name:",
			QLineEdit::Normal, QString::fromStdString(old_name), &ok).toStdString();
	if (ok && (new_name != old_name)) {
		try {
			model->Workspace()->Rename(old_name, new_name);
			// Remove project widget if there is one
			if (project_widget_project_name == old_name) {
				project_widget.reset();
				project_widget_project_name.clear();
			}
			emit ProjectRenamed(old_name, new_name);
		} catch (Exception& e) {
			QMessageBox::warning(this, "Error", QString("Could not rename project: ") + e.what());
		}
	}
}

void WorkspaceView::SLOT_Remove()
{
	QModelIndexList list = selectedIndexes();
	if (list.empty()) {
		return;
	}
	QModelIndex index = list.first();

	if (model->GetType(index) == WorkspaceQtModel::FileType) {
		if (model->rowCount(model->parent(index)) == 1) {
			if (QMessageBox::warning(this, "Continue?",
					"This is the only file in this project.\nRemoving it will remove the project.\nContinue?",
					QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
				index = model->parent(index);
				string project_name = model->GetName(index);
				try {
					model->Close(index);
					model->Workspace()->Remove(project_name); // may throw
					// Remove project widget if there is one
					if (project_widget_project_name == project_name) {
						project_widget.reset();
						project_widget_project_name.clear();
					}
					emit ProjectRemoved(project_name);
				} catch (Exception& e) {
					QMessageBox::warning(this, "Error", QString("Could not remove project: ") + e.what());
				}
			}
		} else {
			if (QMessageBox::warning(this, "Warning", "Remove file?\nThis cannot be undone.",
					QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
				string project_name = model->GetName(model->parent(index));
				string file_name = model->GetName(index);
				try {
					auto project = model->Workspace()->Get(project_name);
					project->Remove(file_name);
				} catch (Exception& e) {
					QMessageBox::warning(this, "Error", QString("Could not remove file:") + e.what());
				}
			}
		}

	} else if (model->GetType(index) == WorkspaceQtModel::ProjectType) {
		if (QMessageBox::warning(this, "Warning", "Remove project?\nThis cannot be undone.",
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
			string project_name = model->GetName(index);
			try {
				model->Close(index);
				model->Workspace()->Remove(project_name);
				// Remove project widget if there is one
				if (project_widget_project_name == project_name) {
					project_widget.reset();
					project_widget_project_name.clear();
				}
				emit ProjectRemoved(project_name);

			} catch (Exception& e) {
				QMessageBox::warning(this, "Error", QString("Could not remove project.") + e.what());
			}
		}
	}
}

void WorkspaceView::SLOT_SelectionChanged(QItemSelection const &, QItemSelection const &)
{
	if (QItemSelectionModel* s_model = selectionModel()) {
		QModelIndexList indexes = s_model->selection().indexes();
		if (indexes.empty()) {
			// no selection -> disable most actions
			action_open->setEnabled(false);
			action_rename->setEnabled(false);
			action_remove->setEnabled(false);
			action_close->setEnabled(false);
		} else {
			action_remove->setEnabled(true);

			QModelIndex index = indexes.first(); // only one item can be selected at the same time.

			action_close->setEnabled(model->IsOpened(index));

			if (model->GetType(index) == WorkspaceQtModel::ProjectType) {
				action_open->setEnabled(true);
				action_rename->setEnabled(!model->IsOpened(index));
			} else if (model->GetType(index) == WorkspaceQtModel::FileType) {
				action_open->setEnabled(true);
				action_rename->setEnabled(false);
			}
		}
	} else {
		// no model -> disable all actions
		action_open->setEnabled(false);
		action_rename->setEnabled(false);
		action_remove->setEnabled(false);
		action_close->setEnabled(false);
	}
}

void WorkspaceView::SLOT_ProjectWidgetAction()
{
	auto& entry = object_to_callback_map[sender()];
	auto prefs = Ws::MergedPreferences::Create(
		model->Workspace(),
		model->Workspace()->Get(entry.project_name));
	project_widget = entry.callback(prefs, this);
	//project_widget->setWindowModality(Qt::ApplicationModal);
	project_widget_project_name = entry.project_name;
}

void WorkspaceView::contextMenuEvent(QContextMenuEvent* event)
{
	QMenu menu(this);

	QModelIndexList list = selectedIndexes();
	if (!list.empty()) {
		QModelIndex const & index = list.first();
		menu.addAction(action_open);
		menu.addSeparator();
		if (model->GetType(index) == WorkspaceQtModel::ProjectType) {
			object_to_callback_map.clear();
			auto project = model->Workspace()->Get(model->GetName(index));
			bool project_is_opened = model->IsOpened(index);
			for (auto widget : project->GetWidgets()) {
				auto a = menu.addAction(QString::fromStdString(widget.name));
				if (project_is_opened)
					a->setEnabled(false);
				object_to_callback_map[a] = {widget.callback, model->GetName(index)};
				// TODO: In Qt5, we can connect signals to C++11 lambda functions,
				// this would make this whole thing much easier...
				connect(a, SIGNAL(triggered()), this, SLOT(SLOT_ProjectWidgetAction()));
			}
		}
		for (auto& action : model->GetContextMenuActions(index)) {
			menu.addAction(action);
		}
		if (model->GetType(index) == WorkspaceQtModel::ProjectType) {
			menu.addAction(action_rename);
		}
		menu.addAction(action_remove);
		menu.addSeparator();
		action_close->setEnabled(model->IsOpened(index));
		menu.addAction(action_close);
	}
	menu.exec(event->globalPos());
}

void WorkspaceView::mouseDoubleClickEvent(QMouseEvent* e)
{
	auto const& list = selectedIndexes();
	if (!list.empty()) {
		auto const& index = list.first();
		WorkspaceQtModel::ItemType type = model->GetType(index);
		if (type == WorkspaceQtModel::FileType) {
			return (void) SLOT_Open();
		}
	}

	// don't override other events
	return (void) QTreeView::mouseDoubleClickEvent(e);
}

} // end of namespace Gui
} // end of namespace SimShell
