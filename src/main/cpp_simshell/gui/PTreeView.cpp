/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PTreeView.
 */

#include "PTreeView.h"

#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QContextMenuEvent>
#include <QDragEnterEvent>
#include <QHeaderView>
#include <QMenu>
#include <QMimeData>

#include "qtmodel/PTreeModel.h"
#include "MyFindDialog.h"


namespace SimShell {
namespace Gui {

PTreeView::PTreeView(QWidget* parent)
	: MyTreeView(parent)
{
	m_only_edit_data = false;

	// performance optimization
	setUniformRowHeights(true);

	// drag 'n drop
	setDragDropMode(QAbstractItemView::DragDrop);
	setDropIndicatorShown(true);
	setDefaultDropAction(Qt::MoveAction);
	setDragEnabled(true);
	setAcceptDrops(true);
	setEditTriggers(DoubleClicked | AnyKeyPressed);

	setAnimated(true);

	/* actions */
	m_action_undo            = new QAction(QIcon::fromTheme("edit-undo"), "&Undo", this);
	m_action_redo            = new QAction(QIcon::fromTheme("edit-redo"), "&Redo", this);
	m_action_cut             = new QAction(QIcon::fromTheme("edit-cut"), "Cut", this);
	m_action_copy            = new QAction(QIcon::fromTheme("edit-copy"), "Copy", this);
	m_action_paste           = new QAction(QIcon::fromTheme("edit-paste"), "&Paste", this);
	m_action_move_up         = new QAction(QIcon::fromTheme("go-up"), "Move Up", this);
	m_action_move_down       = new QAction(QIcon::fromTheme("go-down"), "Move Down", this);
	m_action_insert          = new QAction(QIcon::fromTheme("list-add"), "Insert", this);
	m_action_insert_child    = new QAction(QIcon::fromTheme("list-add"), "Insert Child", this);
	m_action_remove          = new QAction(QIcon::fromTheme("list-remove"), "Remove", this);
	m_action_find_dialog     = new QAction(QIcon::fromTheme("edit-find"), "&Find...", this);
	m_action_clear_highlight = new QAction(QIcon::fromTheme("edit-clear"), "&Clear Highlight", this);
	m_action_expand_all      = new QAction(QIcon::fromTheme("list-add"), "Expand All", this);
	m_action_expand_none     = new QAction(QIcon::fromTheme("list-remove"), "Expand None", this);

	m_action_undo->setShortcut(QKeySequence("Ctrl+Z"));
	m_action_undo->setShortcutContext(Qt::WidgetShortcut);
	m_action_undo->setEnabled(false);
	m_action_redo->setShortcut(QKeySequence("Shift+Ctrl+Z"));
	m_action_redo->setShortcutContext(Qt::WidgetShortcut);
	m_action_redo->setEnabled(false);
	m_action_cut->setShortcut(QKeySequence("Ctrl+X"));
	m_action_cut->setShortcutContext(Qt::WidgetShortcut);
	m_action_cut->setEnabled(false);
	m_action_copy->setShortcut(QKeySequence("Ctrl+C"));
	m_action_copy->setShortcutContext(Qt::WidgetShortcut);
	m_action_copy->setEnabled(false);
	m_action_paste->setShortcut(QKeySequence("Ctrl+V"));
	m_action_paste->setShortcutContext(Qt::WidgetShortcut);
	m_action_paste->setEnabled(false);
	m_action_move_up->setEnabled(false);
	m_action_move_down->setEnabled(false);
	m_action_insert->setEnabled(false);
	m_action_insert_child->setEnabled(false);
	m_action_remove->setShortcut(QKeySequence("Del"));
	m_action_remove->setShortcutContext(Qt::WidgetShortcut);
	m_action_remove->setEnabled(false);
	m_action_find_dialog->setShortcut(QKeySequence("Ctrl+F"));
	m_action_find_dialog->setShortcutContext(Qt::WidgetShortcut);

	// add actions so they can be triggered by shortcut when this view has focus.
	addAction(m_action_undo);
	addAction(m_action_redo);
	addAction(m_action_cut);
	addAction(m_action_copy);
	addAction(m_action_paste);
	addAction(m_action_move_up);
	addAction(m_action_move_down);
	addAction(m_action_insert);
	addAction(m_action_insert_child);
	addAction(m_action_remove);
	addAction(m_action_find_dialog);
	addAction(m_action_clear_highlight);

	connect(&m_undo_stack, SIGNAL(canUndoChanged(bool)), m_action_undo, SLOT(setEnabled(bool)));
	connect(&m_undo_stack, SIGNAL(canRedoChanged(bool)), m_action_redo, SLOT(setEnabled(bool)));
	connect(&m_undo_stack, SIGNAL(cleanChanged(bool)), this, SIGNAL(CleanChanged(bool)));
	connect(&m_undo_stack, SIGNAL(indexChanged(int)), this, SIGNAL(Edited()));
	connect(m_action_undo, SIGNAL(triggered()), &m_undo_stack, SLOT(undo()));
	connect(m_action_redo, SIGNAL(triggered()), &m_undo_stack, SLOT(redo()));
	connect(m_action_cut, SIGNAL(triggered()), this, SLOT(Cut()));
	connect(m_action_copy, SIGNAL(triggered()), this, SLOT(Copy()));
	connect(m_action_paste, SIGNAL(triggered()), this, SLOT(Paste()));
	connect(m_action_move_up, SIGNAL(triggered()), this, SLOT(MoveUp()));
	connect(m_action_move_down, SIGNAL(triggered()), this, SLOT(MoveDown()));
	connect(m_action_insert, SIGNAL(triggered()), this, SLOT(Insert()));
	connect(m_action_insert_child, SIGNAL(triggered()), this, SLOT(InsertChild()));
	connect(m_action_remove, SIGNAL(triggered()), this, SLOT(Remove()));
	connect(m_action_find_dialog, SIGNAL(triggered()), this, SLOT(FindDialog()));
	connect(m_action_clear_highlight, SIGNAL(triggered()), this, SLOT(ClearHighlight()));
	connect(m_action_expand_all, SIGNAL(triggered()), this, SLOT(expandAll()));
	connect(m_action_expand_none, SIGNAL(triggered()), this, SLOT(collapseAll()));


	QClipboard* clipboard = QApplication::clipboard();
	connect(clipboard, SIGNAL(dataChanged()), this, SLOT(SetClipboardChanged()));

	// don't create find dialog until it is shown
	m_find_dialog = 0;
}

void PTreeView::ClearHighlight()
{
	if (!model()) {
		return;
	}
	for (QModelIndexList::iterator i = m_results.begin(); i != m_results.end(); i++) {
		model()->setData(*i, QVariant(), Qt::BackgroundRole);
	}
	m_query = "";
	m_results.clear();
	m_result = m_results.end();
	m_action_clear_highlight->setEnabled(false);
}

void PTreeView::contextMenuEvent(QContextMenuEvent* event)
{
	QMenu menu(this);
	menu.addAction(m_action_insert);
	menu.addAction(m_action_insert_child);
	menu.addSeparator();
	menu.addAction(m_action_move_up);
	menu.addAction(m_action_move_down);
	menu.addSeparator();
	menu.addAction(m_action_remove);
	menu.addSeparator();
	menu.addAction(m_action_cut);
	menu.addAction(m_action_copy);
	menu.addAction(m_action_paste);
	menu.exec(event->globalPos());
}

void PTreeView::Copy()
{
	if (model()) {
		QModelIndexList indexes = selectedIndexes();
		if (indexes.empty()) {
			return;
		}
		QMimeData* mime = model()->mimeData(indexes);
		QClipboard* clipboard = QApplication::clipboard();
		clipboard->setMimeData(mime);
		// ownership of mime is transferred to clipboard, no need to free memory
	}
}

void PTreeView::Cut()
{
	if (!model()) {
		return;
	}

	QModelIndexList indexes = selectedIndexes();
	if (indexes.empty()) {
		return;
	}
	Copy();
	QModelIndex const & index = indexes.first();
	model()->removeRow(index.row(), model()->parent(index));
	selectionModel()->clearSelection();
	selectionModel()->setCurrentIndex(QModelIndex(), QItemSelectionModel::Clear);
}

void PTreeView::FindDialog()
{
	if (!m_find_dialog) {
		m_find_dialog = new MyFindDialog(this);
		connect(m_find_dialog, SIGNAL(FindNext(QString const &, bool)),
				this, SLOT(FindNext(QString const &, bool)));
	}
	m_find_dialog->show();
	m_find_dialog->raise();
	m_find_dialog->activateWindow();
	m_find_dialog->CorrectFocus();
}

void PTreeView::FindNext(QString const & new_query, bool match_case)
{
	if (!model()) {
		return;
	}
	Qt::MatchFlags new_flags = Qt::MatchContains | Qt::MatchWrap | Qt::MatchRecursive;
	if (match_case) {
		new_flags |= Qt::MatchCaseSensitive;
	}
	if (new_query == m_query && new_flags == m_flags && m_result != m_results.end()) {
		// if same old query ("find next"), jump to next result
		if (++m_result == m_results.end()) {
			m_result = m_results.begin();
		}
	} else {
		// un-highlight previous results
		for (QModelIndexList::iterator i = m_results.begin(); i != m_results.end(); i++) {
			model()->setData(*i, QVariant(), Qt::BackgroundRole);
		}
		if (new_query == "") {
			m_results.clear();
		} else {
			m_results = model()->match(model()->index(0, 0),
					Qt::DisplayRole, QVariant(new_query), -1, new_flags);
		}
		m_result = m_results.begin();

		m_query = new_query;
		m_flags = new_flags;

		// highlight results
		for (QModelIndexList::iterator i = m_results.begin(); i != m_results.end(); ++i) {
			model()->setData(*i, QVariant(QBrush(QColor(255, 255, 0))), Qt::BackgroundRole);
		}
	}

	if (m_results.size() == 1) {
		emit StatusChanged(QString::number(m_results.size()) + " result found");
	} else {
		emit StatusChanged(QString::number(m_results.size()) + " results found");
	}

	if (m_result != m_results.end()) {
		selectionModel()->select(*m_result, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
		scrollTo(*m_result);
		m_action_clear_highlight->setEnabled(true);
	} else {
		m_action_clear_highlight->setEnabled(false);
	}
}

QAction* PTreeView::GetUndoAction() const           {return m_action_undo;}
QAction* PTreeView::GetRedoAction() const           {return m_action_redo;}
QAction* PTreeView::GetCutAction() const            {return m_action_cut;}
QAction* PTreeView::GetCopyAction() const           {return m_action_copy;}
QAction* PTreeView::GetPasteAction() const          {return m_action_paste;}
QAction* PTreeView::GetMoveUpAction() const         {return m_action_move_up;}
QAction* PTreeView::GetMoveDownAction() const       {return m_action_move_down;}
QAction* PTreeView::GetInsertBeforeAction() const   {return m_action_insert;}
QAction* PTreeView::GetInsertChildAction() const    {return m_action_insert_child;}
QAction* PTreeView::GetRemoveAction() const         {return m_action_remove;}
QAction* PTreeView::GetFindDialogAction() const     {return m_action_find_dialog;}
QAction* PTreeView::GetClearHighlightAction() const {return m_action_clear_highlight;}
QAction* PTreeView::GetExpandAllAction() const      {return m_action_expand_all;}
QAction* PTreeView::GetExpandNoneAction() const     {return m_action_expand_none;}


void PTreeView::Insert()
{
	QModelIndexList list = selectedIndexes();
	if (list.size()) {
		for (auto const & i : list) {
			if (i.column() == 0) {
				QModelIndex parent = model()->parent(i);
				model()->insertRow(i.row(), parent);
				QModelIndex inserted = model()->index(i.row(), 0, parent);
				selectionModel()->select(inserted, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
				selectionModel()->setCurrentIndex(inserted, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			}
		}
	} else {
		model()->insertRow(0);
		QModelIndex inserted = model()->index(0, 0);
		selectionModel()->select(inserted, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
		selectionModel()->setCurrentIndex(inserted, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
	}
}

void PTreeView::InsertChild()
{
	for (auto const & i : selectedIndexes()) {
		if (i.column() == 0) {
			model()->insertRow(0, i);
		}
	}
}

bool PTreeView::IsClean() const
{
	return m_undo_stack.isClean();
}

bool PTreeView::IsOnlyEditData() const
{
	return m_only_edit_data;
}

void PTreeView::MoveDown()
{
	for (auto const & i : selectedIndexes()) {
		if (i.column() == 0) {
			if (PTreeModel* ptree_model = dynamic_cast<PTreeModel*>(model())) {
				int row = i.row();
				QModelIndex parent = ptree_model->parent(i);
				ptree_model->MoveRow(row, parent, row+1, parent);
				QModelIndex moved = ptree_model->index(row+1, 0, parent);
				selectionModel()->select(moved, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
				selectionModel()->setCurrentIndex(moved, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			}
		}
	}
}

void PTreeView::MoveUp()
{
	for (auto const & i : selectedIndexes()) {
		if (i.column() == 0) {
			if (PTreeModel* ptree_model = dynamic_cast<PTreeModel*>(model())) {
				int row = i.row();
				QModelIndex parent = ptree_model->parent(i);
				ptree_model->MoveRow(row, parent, row-1, parent);
				QModelIndex moved = ptree_model->index(row-1, 0, parent);
				selectionModel()->select(moved, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
				selectionModel()->setCurrentIndex(moved, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			}
		}
	}
}

void PTreeView::Paste()
{
	if (model()) {
		QClipboard* clipboard = QApplication::clipboard();
		QMimeData const * mime = clipboard->mimeData();

		QModelIndexList indexes = selectedIndexes();
		if (indexes.empty()) {
			// no selection? paste in root of tree
			model()->dropMimeData(mime, Qt::CopyAction, 0, 0, QModelIndex());
			QModelIndex index = model()->index(0,0);
			selectionModel()->select(index, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			selectionModel()->setCurrentIndex(index, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			scrollTo(index);
		} else {
			// paste before selection
			QModelIndex index = indexes.first();
			model()->dropMimeData(mime, Qt::CopyAction, index.row(), index.column(), model()->parent(index));
			selectionModel()->select(index, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			selectionModel()->setCurrentIndex(index, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			scrollTo(index);
		}
	}
}

void PTreeView::Remove()
{
	for (auto const & i : selectedIndexes()) {
		if (i.column() == 0) {
			model()->removeRow(i.row(), model()->parent(i));
		}
	}
	selectionModel()->clearSelection();
	selectionModel()->setCurrentIndex(QModelIndex(), QItemSelectionModel::Clear);
}

void PTreeView::SetClean()
{
	m_undo_stack.setClean();
}

void PTreeView::SetClipboardChanged()
{
	if (model()) {
		QClipboard const * clipboard = QApplication::clipboard();
		if (QMimeData const * mime = clipboard->mimeData())
			if (mime->hasFormat("text/plain")) {
				m_action_paste->setEnabled(true);
				return;
			}
	}
	m_action_paste->setEnabled(false);
}

void PTreeView::setModel(QAbstractItemModel* m)
{
	m_undo_stack.clear();
	MyTreeView::setModel(m);

	if (m) {
		if (PTreeModel* ptree_model = dynamic_cast<PTreeModel*>(m)) {
			// tell PTreeModel to use our undo stack
			ptree_model->SetUndoStack(&m_undo_stack);
		}

		QItemSelectionModel* s = selectionModel();
		connect(s, SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(SetSelectionChanged(const QItemSelection &, const QItemSelection &)));

		if (!m_only_edit_data) {
			m_action_insert->setEnabled(true);
		}
	} else {
		m_action_insert->setEnabled(false);
	}

	// this will check if clipboard contains pastable data and enable/disable action_paste correspondingly
	SetClipboardChanged();

	// selection model doesn't emit selectionChanged() when view is reset, so we have to do it ourselves.
	SetSelectionChanged(QItemSelection(), QItemSelection());

	// search was cleared
	m_action_clear_highlight->setEnabled(false);
}

void PTreeView::SetOnlyEditData(bool b)
{
	m_only_edit_data = b;
	if (m_only_edit_data) {
		setDragDropMode (DragOnly);
	} else {
		setDragDropMode (DragDrop);
	}
}

void PTreeView::SetSelectionChanged(QItemSelection const &, QItemSelection const &)
{
	if (model() && !selectedIndexes().empty()) {
		m_action_copy->setEnabled(true);
		if (!m_only_edit_data) {
			m_action_cut->setEnabled(true);
			m_action_paste->setEnabled(true);
			m_action_move_up->setEnabled(true);
			m_action_move_down->setEnabled(true);
			m_action_insert_child->setEnabled(true);
			m_action_remove->setEnabled(true);
		} else {
			m_action_cut->setEnabled(false);
			m_action_paste->setEnabled(false);
			m_action_move_up->setEnabled(false);
			m_action_move_down->setEnabled(false);
			m_action_insert_child->setEnabled(false);
			m_action_remove->setEnabled(false);
		}
	} else {
		m_action_cut->setEnabled(false);
		m_action_copy->setEnabled(false);
		m_action_paste->setEnabled(false);
		m_action_move_up->setEnabled(false);
		m_action_move_down->setEnabled(false);
		m_action_insert_child->setEnabled(false);
		m_action_remove->setEnabled(false);
	}
}

} // end of namespace Gui
} // end of namespace SimShell
