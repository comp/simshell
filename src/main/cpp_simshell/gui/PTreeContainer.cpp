/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PTreeContainer.
 */

#include "PTreeContainer.h"

#include "MyDockWidget.h"
#include "PTreeEditorWindow.h"
#include "PTreeMenu.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

PTreeContainer::PTreeContainer(QString const & title, QWidget* parent)
	: QWidget(parent), HasUnsavedChanges(title.toStdString()), m_opened(false), m_title(title)
{
	m_window = new PTreeEditorWindow();
	m_window->setWindowTitle(m_title);
	m_dock = new MyDockWidget(nullptr);
	m_dock->setWindowTitle(m_title);
	m_dock->setWidget(m_window);
	m_menu = new PTreeMenu(m_title, QString(), nullptr);

	connect(m_menu, SIGNAL(ItemTriggered(QString const &)), this, SLOT(OpenPath(QString const &)));
	connect(m_window, SIGNAL(ApplyTriggered(boost::property_tree::ptree const &)), this, SLOT(RefreshMenu(boost::property_tree::ptree const &)));
	connect(m_window, SIGNAL(ApplyTriggered(boost::property_tree::ptree const &)), this, SIGNAL(Applied(boost::property_tree::ptree const &)));
	connect(m_window, SIGNAL(StatusChanged(QString const &)), this, SIGNAL(StatusChanged(QString const &)));

	SetChildren({
		m_window,
		m_menu
	});
}

PTreeContainer::~PTreeContainer()
{
	delete m_window;
	delete m_dock;
	delete m_menu;
}

QDockWidget* PTreeContainer::GetDock() const
{
	return m_dock;
}

std::string PTreeContainer::GetEditPath() const
{
	return m_window->GetEditPath();
}


PTreeMenu* PTreeContainer::GetMenu() const {
	return m_menu;
}

ptree PTreeContainer::GetPTreeState() const
{
	ptree result;
	result.put_child("editor_window", m_window->GetPTreeState());
	result.put_child("dock", m_dock->GetPTreeState());
	return result;
}

void PTreeContainer::InternalForceClose()
{
	m_opened = false;
	m_dock->setWindowTitle(m_title);
}

bool PTreeContainer::InternalIsClean() const
{
	return true;
}

bool PTreeContainer::InternalSave()
{
	return true;
}

bool PTreeContainer::IsOpened() const
{
	return m_opened;
}

bool PTreeContainer::Open(boost::property_tree::ptree const & pt, QString const & edit_path)
{
	if (m_window->OpenPTree(pt, edit_path))
	{
		m_menu->OpenPTree(pt);
		m_dock->setWindowTitle(m_title + ": " + edit_path);
		m_opened = true;
		return true;
	}
	return false;
}

void PTreeContainer::OpenPath(QString const & path)
{
	m_window->OpenPath(path);
	m_dock->show();
	m_dock->setWindowTitle(m_title + ": " + path);
}

void PTreeContainer::Redo() {
	m_window->Redo();
}

void PTreeContainer::RefreshMenu(boost::property_tree::ptree const & pt)
{
	m_menu->OpenPTree(pt);
}

void PTreeContainer::SetPTreeState(const ptree& state)
{
	auto editor_window_state = state.get_child_optional("editor_window");
	if (editor_window_state)
		m_window->SetPTreeState(editor_window_state.get());

	auto dock_state = state.get_child_optional("dock");
	if (dock_state) {
		m_dock->SetPTreeState(dock_state.get());
	}
}

void PTreeContainer::SetOnlyEditData(bool b)
{
	m_window->SetOnlyEditData(b);
}

void PTreeContainer::Undo() {
	m_window->Undo();
}

} // namespace Gui
} // namespace SimShell
