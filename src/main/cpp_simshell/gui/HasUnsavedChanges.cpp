/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for HasUnsavedChanges.
 */

#include "HasUnsavedChanges.h"

using namespace std;

namespace SimShell {
namespace Gui {

HasUnsavedChanges::HasUnsavedChanges(string&& title)
	: m_title(move(title))
{
}

HasUnsavedChanges::~HasUnsavedChanges()
{
}

HasUnsavedChanges::ChildrenType::iterator HasUnsavedChanges::begin()
{
	return m_children.begin();
}

HasUnsavedChanges::ChildrenType::const_iterator HasUnsavedChanges::begin() const
{
	return m_children.begin();
}

HasUnsavedChanges::ChildrenType::iterator HasUnsavedChanges::end()
{
	return m_children.end();
}

HasUnsavedChanges::ChildrenType::const_iterator HasUnsavedChanges::end() const
{
	return m_children.end();
}

void HasUnsavedChanges::ForceClose()
{
	if (IsOpened()) {
		InternalPreForceClose();
		// Force close children first
		for (auto child : m_children) {
			child->ForceClose();
		}
		InternalForceClose();
	}
}

const string& HasUnsavedChanges::GetTitle()
{
	return m_title;
}

bool HasUnsavedChanges::IsClean() const
{
	if (!InternalIsClean()) {
		return false;
	}
	for (auto child : m_children) {
		if (!child->IsClean())
			return false;
	}
	return true;
}

bool HasUnsavedChanges::Save()
{
	// Save children first
	for (auto child : m_children) {
		if (!child->Save())
			return false;
	}
	if (IsOpened()) {
		return InternalSave();
	} else {
		return true;
	}
}

bool HasUnsavedChanges::SaveAndClose()
{
	if (Save()) {
		ForceClose();
		return true;
	} else {
		return false;
	}
}

void HasUnsavedChanges::AddChild(HasUnsavedChanges::ChildrenType::value_type&& v)
{
	m_children.push_back(move(v));
}

void HasUnsavedChanges::SetChildren(HasUnsavedChanges::ChildrenType&& c)
{
	m_children = move(c);
}

} // namespace Gui
} // namespace SimShell
