#ifndef SIMSHELL_GUI_IFACTORY_H_INCLUDED
#define SIMSHELL_GUI_IFACTORY_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011-2014, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for IFactory.
 */

#include "workspace/IWorkspaceFactory.h"

namespace SimShell {
namespace Gui {

/**
 * Factory for application-specific stuff in generic simshell GUI.
 */
class IFactory
{
public:
	/// Virtual destructor for interface class.
	virtual ~IFactory() {}

	/// Create workspace factory.
	virtual std::shared_ptr<Ws::IWorkspaceFactory> CreateWorkspaceFactory() const = 0;

	/// Get text shown in the "About" box.
	virtual std::string GetAbout() const = 0;

	/// Get application name.
	virtual std::string GetApplicationName() const = 0;

	/// Get organization name.
	virtual std::string GetOrganizationName() const = 0;
};

} // end namespace Gui
} // end namespace SimShell

#endif
