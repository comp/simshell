#ifndef SAVECHANGESDIALOG_H_INCLUDED
#define SAVECHANGESDIALOG_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for SaveChangesDialog.
 */

#include <map>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include <QDialog>

class QAbstractButton;
class QCheckBox;
class QPushButton;
class QTreeView;
class QVBoxLayout;

namespace SimShell {
namespace Gui {

class CheckableTreeModel;
class HasUnsavedChanges;

/**
 * Dialog window that displays a tree of objects containing unsaved changes.
 * For each object, a checkbox exists such that the user can select which objects to save and which to discard.
 *
 * Typically you want to show this dialog when closing an object.
 */
class SaveChangesDialog : public QDialog
{
    Q_OBJECT
public:
	/// Create and display a dialog from a HasUnsavedChanges interface.
	/// If user clicks "Save", all checked items will be saved.
	/// Unless user clicks "Cancel", all HasUnsavedChanges interfaces will be force-closed.
	/// @param parent      Dialog parent widget.
	/// @param prompt_map  A map containing PromptOnClose interfaces, indexed by some title. If empty, "true" will be returned immediately (no dialog will be shown).
	/// @return true if user clicked "Save" or "Discard", false if user clicked "Cancel".
	static bool Prompt(HasUnsavedChanges* widget, QWidget* parent = nullptr);

private slots:
	void SLOT_Clicked(QAbstractButton*);

private:
	SaveChangesDialog(const boost::property_tree::ptree&, QWidget* parent = 0);

	/// One of these values will be returned by the SaveChangesDialog::exec() method.
	enum Result
	{
		Save, Cancel, Discard
	};

	QPushButton*             m_save_button;
	QPushButton*             m_cancel_button;
	QPushButton*             m_discard_button;
	CheckableTreeModel*      m_model;
};

} // end of namespace Gui
} // end of namespace SimShell

#endif
