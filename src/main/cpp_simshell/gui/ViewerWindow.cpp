/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation of ViewerWindow.
 */

#include <QAction>
#include <QCloseEvent>
#include "ViewerWindow.h"

using namespace std;

namespace SimShell {
namespace Gui {

ViewerWindow::ViewerWindow(const shared_ptr<Ws::MergedPreferences>& p, QWidget* parent, std::function<void()> on_close)
	: QMainWindow(parent, Qt::Dialog), m_preferences(p), m_on_close(on_close)
{
	setWindowModality(Qt::NonModal);

	auto pos_x = m_preferences->Get<int>("position.x");
	auto pos_y = m_preferences->Get<int>("position.y");
	auto size_x = m_preferences->Get<int>("size.x");
	auto size_y = m_preferences->Get<int>("size.y");
	setGeometry(pos_x, pos_y, size_x, size_y);
}

ViewerWindow::~ViewerWindow()
{
	auto g = geometry();
	m_preferences->Put("position.x", g.x());
	m_preferences->Put("position.y", g.y());
	m_preferences->Put("size.x", g.width());
	m_preferences->Put("size.y", g.height());
}

void ViewerWindow::closeEvent(QCloseEvent* )
{
	if (m_on_close)
		m_on_close();
}

} // end namespace Gui
} // end namespace SimShell
