#ifndef GUI_PTREECONTAINERPREFERENCESOBSERVER_H_INCLUDED
#define GUI_PTREECONTAINERPREFERENCESOBSERVER_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PTreeContainerPreferencesObserver.
 */

#include "PTreeContainer.h"
#include "workspace/event/PreferencesChanged.h"

namespace SimShell {
namespace Gui {

/**
 * PTreeContainer meant for displaying/editing preferences ptrees that can also
 * be modified elsewhere in the code.
 * This class can act as an observer for Ws::Event::PreferencesChanged events.
 * @see Update
 */
class PTreeContainerPreferencesObserver : public PTreeContainer
{
	Q_OBJECT
public:
	PTreeContainerPreferencesObserver(const QString& title, QWidget* parent = nullptr);

	/**
	 * Virtual destructor.
	 */
	virtual ~PTreeContainerPreferencesObserver();

	/**
	 * Call this method to update the maintained ptree.
	 * Typically, you want to register this method as a callback for
	 * Ws::Event::PreferencesChanged events.
	 */
	void Update(const Ws::Event::PreferencesChanged&);
};

} // namespace Gui
} // namespace SimShell

#endif
