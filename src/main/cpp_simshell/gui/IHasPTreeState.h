#ifndef GUI_IHASPTREESTATE_H_INCLUDED
#define GUI_IHASPTREESTATE_H_INCLUDED
/*
 *  This file is part of the SimShell software.
 *  SimShell is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  SimShell is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with SimShell. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012, 2013, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * HasPtreeState interface.
 */

#include <boost/property_tree/ptree.hpp>

namespace SimShell {
namespace Gui {

/**
 * Interface for objects that have non-critical state (metadata) information representable as a ptree.
 *
 * Typically used for remembering non-critical information, such as user interface layout, between sessions.
 */
class IHasPTreeState
{
public:
	virtual ~IHasPTreeState() {}

	/**
	 * Return (possibly empty) ptree containing information about the current state of the object.
	 * Returned ptree can be used as parameter for SetPTreeState at a later point in time.
	 */
	virtual boost::property_tree::ptree GetPTreeState() const = 0;

	/**
	 * Set state of the object. Argument should be a ptree retrieved by GetPTreeState at an earlier moment in time.
	 *
	 * Implementation should be able to deal with missing information, e.g. a missing subtree should not cause this method to throw an error.
	 * An empty ptree as parameter should cause nothing to happen.
	 *
	 * The setting of the ptree state of an object must only serve as an enhancement to the usability of the program.
	 */
	virtual void SetPTreeState(const boost::property_tree::ptree&) = 0;
};

} // namespace Gui
} // namespace SimShell

#endif
