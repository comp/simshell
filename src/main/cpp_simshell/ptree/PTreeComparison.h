#ifndef PTREE_COMPARISON_H_INCLUDED
#define PTREE_COMPARISON_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeComparison.
 */

#include <utility>
#include <boost/property_tree/ptree_fwd.hpp>
#include "util/Exception.h"


namespace VirtualLeaf {

using boost::property_tree::ptree;

/**
 * Collection of functions for recursively comparing ptrees, with some 'array' semantics applied.
 *
 * Because all data in a ptree is both ordered (sequential) and unordered (mapped by a key) at the same time,
 * we apply the semantics that, if a subtree path ends with "_array", the order of its children is semantically taken into account.
 * For 2 'array' subtrees to be equal, we thus require all children to be present in the same order in both subtrees.
 * If a subtree path does not end with "_array", we just want all values with a certain key to be found under the same key in the subtree it is compared with.
 *
 * This is all done recursively: For instance, the ordered items of an array can, in its turn, be arrays or unordered subtrees,
 * and the unordered items of an indexed subtree can be arrays or unordered subtrees.
 * The right compare method is selected on each recursion.
 */
class PTreeComparison
{
public:
	/**
	 * Test if root values of both ptrees are equal, with the possibility
	 * of having some tolerance for small differences in case both root
	 * values are floats.
	 *
	 * Does NOT recursively compare children of the given ptrees.
	 *
	 * @param pt1                 First ptree.
	 * @param pt2                 Second ptree.
	 * @param acceptable_diff     Largest tolerated difference between
	 *                            root values if both are float type.
	 * @return  Whether root values tested equal.
	 */
	static bool CompareRootValues(ptree const& pt1, ptree const& pt2, double acceptable_diff);

	/**
	 * Test if ordered list of children of both ptrees are equal,
	 * i.e. lenghts must be the same, and every item in one ptree must
	 * occur in the other at the same index.
	 *
	 * Root values of children are compared using CompareRootValues.
	 * On top of that, recursively compares children of both ptrees:
	 * When a key under which a child ptree is found ends with the suffix
	 * "_array", CompareArray is used. Otherwise, CompareNonArray is used.
	 *
	 * @param pt1                First ptree.
	 * @param pt2                Second ptree.
	 * @param acceptable_diff    Largest tolerated difference between root
	 *                           values of children in both ptrees.
	 * @return  Whether both ptrees tested equal.
	 */
	static bool CompareArray(ptree const& pt1, ptree const& pt2, double acceptable_diff);

	/**
	 * Test if every child in first ptree with certain key can also be
	 * found in second ptree under the same key, and vice versa.
	 *
	 * Comparison is RECURSIVE, see PTreeComparison::CompareArray for details.
	 *
	 * @param pt1                First ptree.
	 * @param pt2                Second ptree.
	 * @param acceptable_diff    Largest tolerated difference between root
	 *                           values of children in both ptrees.
	 * @return  Whether both ptrees tested equal.
	 */
	static bool CompareNonArray(ptree const& pt1, ptree const& pt2, double acceptable_diff);
};
}

#endif // end-of-include-guard
