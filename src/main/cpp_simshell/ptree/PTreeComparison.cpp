/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation for PTreeComparison.
 */

#include "PTreeComparison.h"

#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>

#include "util/Exception.h"
#include "util/StringUtils.h"


using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using boost::optional;
using namespace UA_CoMP::Util;

namespace VirtualLeaf {

bool PTreeComparison::CompareRootValues(ptree const& pt1, ptree const& pt2, double acceptable_diff)
{
	std::string pt1_value = pt1.get_value<std::string>();
	std::string pt2_value = pt2.get_value<std::string>();

	if (acceptable_diff > 1.0e-15) {
		bool pt1_value_is_number = false;
		bool pt2_value_is_number = false;

		double pt1_value_parsed = 0.0;
		double pt2_value_parsed = 0.0;

		try {
			pt1_value_parsed = pt1.get_value<double>();
			pt1_value_is_number = true;
		} catch (ptree_bad_data &) {
			pt1_value_is_number = false;
		}

		try {
			pt2_value_parsed = pt2.get_value<double>();
			pt2_value_is_number = true;
		} catch (ptree_bad_data &) {
			pt2_value_is_number = false;
		}

		if (pt1_value_is_number && pt2_value_is_number) {
			if (pt1_value.find('.') == std::string::npos
			                && pt2_value.find('.') == std::string::npos) {
				// both values are integers => must be exactly the same
				return pt1_value == pt2_value;
			} else {
				double const diff = fabs(pt2_value_parsed - pt1_value_parsed)
				                / (1.0 + fabs(pt1_value_parsed));
				bool equal = (diff < acceptable_diff);
				if (!equal) {
					cout << "LeafTransformer::CompareRootValues> Diff too large: " << diff << endl;
				}
				return equal;
			}
		}
	}

	return pt1_value == pt2_value;
}

bool PTreeComparison::CompareArray(ptree const& pt1, ptree const& pt2, double acceptable_diff)
{

	if (!CompareRootValues(pt1, pt2, acceptable_diff)) {
		cout << "LeafTransformer::CompareArray> Root values not equal" << endl;
		return false;
	}

	if (pt1.size() != pt2.size()) {
		cout << "LeafTransformer::CompareArray> Different number of children." << endl;
		return false;
	}

	auto pt1_child_entry = pt1.begin();
	auto pt2_child_entry = pt2.begin();
	while (pt1_child_entry != pt1.end() && pt2_child_entry != pt2.end()) {
		if (pt1_child_entry->first != pt2_child_entry->first) {
			cout << "LeafTransformer::CompareArray> Array elements have different keys." << endl;
			return false;
		}

		if (pt1_child_entry->first.rfind("_array") == pt1_child_entry->first.length()-6) {
			if (!CompareArray(pt1_child_entry->second, pt2_child_entry->second, acceptable_diff)) {
				return false;
			}
		} else {
			if (!CompareNonArray(pt1_child_entry->second, pt2_child_entry->second, acceptable_diff)) {
				cout << "LeafTransformer::CompareNonArray> Subtree: " << pt1_child_entry->first << endl;
				return false;
			}
		}

		pt1_child_entry++;
		pt2_child_entry++;
	}
	return true;
}

bool PTreeComparison::CompareNonArray(ptree const& pt1, ptree const& pt2, double acceptable_diff)
{

	if (!CompareRootValues(pt1, pt2, acceptable_diff)) {
		cout << "LeafTransformer::CompareNonArray> Root values not equal" << endl;
		return false;
	}

	if (pt1.size() != pt2.size()) {
		cout << "LeafTransformer::CompareNonArray> Different number of children." << endl;
		return false;
	}

	for (auto const& pt1_child_entry : pt1) {
		ptree const & pt1_child = pt1_child_entry.second;
		optional<ptree const&> pt2_child = pt2.get_child_optional(pt1_child_entry.first);

		if (!pt2_child) {
			cout << "LeafTransformer::CompareNonArray> Child doesn't exist: " << pt1_child_entry.first
			                << endl;
			return false;
		}

		if (pt1_child_entry.first.rfind("_array") == pt1_child_entry.first.length()-6) {
			if (!CompareArray(pt1_child, *pt2_child, acceptable_diff)) {
				return false;
			}
		} else {
			if (!CompareNonArray(pt1_child, *pt2_child, acceptable_diff)) {
				cout << "LeafTransformer::CompareNonArray> Subtree: " << pt1_child_entry.first << endl;
				return false;
			}
		}
	}
	return true;
}

} // end namespace VirtualLeaf
