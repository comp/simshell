#ifndef MAIN_PTREE_EDITOR_H_INCLUDED
#define MAIN_PTREE_EDITOR_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for PTreeEditor.
 */

#include <string>
#include <boost/property_tree/ptree.hpp>
#include <QMainWindow>
#include "gui/HasUnsavedChangesPrompt.h"

class QTabWidget;
class QMenu;
class QStatusBar;
class QToolBar;

namespace SimShell {
namespace Gui {

class MyFindDialog;
class PTreeView;
class PTreeModel;

} // namespace Gui
} // namespace SimShell

namespace VirtualLeaf {
namespace Gui {

using namespace SimShell::Gui;

/**
 * Stand-alone Property Tree editor.
 */
class PTreeEditor : public QMainWindow, public HasUnsavedChangesPrompt
{
	Q_OBJECT
public:
	PTreeEditor(QWidget* parent = 0);
	virtual ~PTreeEditor() {}

	/**
	 * Open file.
	 * If a file is already opened, it is closed first.
	 * @param path  Path to file to open.
	 * @return true if succesful.
	 */
	bool OpenPath(std::string const & path);

	/**
	 * Save opened file to a given path.
	 * If no file is opened, nothing happens.
	 * @param path  Path to save file to.
	 * @return true if application is in clean state and opened_path is equal to given argument.
	 */
	bool SavePath(std::string const & path);

	/// Test whether app is in opened state.
	virtual bool IsOpened() const;

private slots:
	/**
	 * Save opened file to opened path. If opened file is untitled (new file), will show a save dialog.
	 * @return true if application is in clean state and opened_path is an existing file on disk.
	 * @see PromptOnClose::Save().
	 */
	void SLOT_Save();

	/**
	 * Close file. Confirmation dialog will be shown if there are unsaved changes.
	 * @return true if application is in unopened state.
	 */
	void SLOT_PromptClose();

	/**
	 * Create new file for editing.
	 */
	void SLOT_New();

	/**
	 * Display dialog to open file.
	 */
	void SLOT_OpenDialog();

	/**
	 * Display dialog to save file.
	 * @return true if application is in clean state and opened_path is an existing file on disk.
	 */
	bool SLOT_SaveAsDialog();

	/**
	 * Set fullscreen mode.
	 */
	void SLOT_SetFullscreenMode(bool);

	/**
	 * Display 'about' dialog.
	 */
	void SLOT_AboutDialog();

	/**
	 * Handles PTreeView cleanChanged events.
	 */
	void SLOT_SetClean(bool);

protected:
	/**
	 * Re-implemented from QMainWindow.
	 * Closes file if file is opened, possibly displaying 'save changes?'-dialog.
	 * Event is accepted only if this results in the application remaining in an unopened state.
	 * @see QWidget::closeEvent
	 */
	virtual void closeEvent(QCloseEvent *event);

	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private:
	QAction*     action_new;
	QAction*     action_open;
	QAction*     action_save;
	QAction*     action_save_as;
	QAction*     action_close;
	QAction*     action_quit;
	QAction*     action_view_toolbar;
	QAction*     action_view_statusbar;
	QAction*     action_fullscreen;
	QAction*     action_about;

	QToolBar*    toolbar;
	QStatusBar*  statusbar;

	std::string      opened_path; // empty but ptree_model != 0 means an untitled new file is opened.
	PTreeView*       ptree_view;
	PTreeModel*      ptree_model; // 0 if app is in unopened state

	boost::property_tree::ptree     root_pt;
	std::string                     subtree_key;

	static const QString g_caption;
	static const QString g_caption_with_file;
};

} // Gui
} // VirtualLeaf

#endif
