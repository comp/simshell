#ifndef PTREEUTILS_H_INCLUDED
#define PTREEUTILS_H_INCLUDED
/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * PTreeUtils interface.
 */

#include <list>
#include <set>
#include <string>
#include <utility>
#include <boost/property_tree/ptree_fwd.hpp>

namespace UA_CoMP {
namespace Util {

/**
 * A collection of functions for manipulating the structure of ptrees.
 */
class PTreeUtils
{
public:
	/**
	 * Copies every node from 'source' ptree if that node doesn't exist in 'destination' ptree.
	 *
	 * @param src       'source' ptree.
	 * @param dst       'destination' ptree.
	 * @param ignore    A list of node keys not to copy. (they are skipped).
	 */
	static void CopyNonExistingChildren(
		const boost::property_tree::ptree& src,
		boost::property_tree::ptree& dst,
		const std::set<std::string>& ignore = std::set<std::string>());

	static void CopyStructure(
		const boost::property_tree::ptree& src,
		boost::property_tree::ptree& dst,
		const std::string& fill_value,
		const std::set<std::string>& ignore = std::set<std::string>());

	/**
	 * Removes every node in 'destination' ptree if that node doesn't exist in 'source' ptree.
	 *
	 * @param src       'source' ptree.
	 * @param dst       'destination' ptree.
	 * @param ignore    A list of node keys not to remove. (they are skipped).
	 */
	static void RemoveNonExistingChildren(
		const boost::property_tree::ptree& src,
		boost::property_tree::ptree& dst,
		const std::set<std::string>& ignore = std::set<std::string>());

	/**
	 * Fill the first ptree with the second ptree.
	 * The ptrees should have the same structures.
	 *
	 * @param	pt1		The first ptree.
	 * @param	pt2		The second ptree.
	 */
	static void FillPTree(boost::property_tree::ptree& pt1, const boost::property_tree::ptree& pt2);

	/**
	 * Flattens a ptree from a given path.
	 *
	 * @param	pt		The given ptree.
	 * @param	indexedArray	Whether to append indexes '[i]' to children of '*_array' entries of the ptree.
	 * @param	path		The given path (shouldn't be given when the full ptree should be flatten).
	 * @return	A list with for each path in the ptree, the associated value.
	 */
	static std::list<std::pair<std::string, std::string>>
	Flatten(const boost::property_tree::ptree& pt, bool indexedArray = false, const std::string& path = "");

	/**
	 * Returns a reference to the subptree in the given ptree, taking indexes of
	 * array-elements (of PTreeUtils::Flatten function) into account.
	 *
	 * @param	pt		The given ptree.
	 * @param	path		The given path.
	 * @return	The subtree to get.
	 */
	static const boost::property_tree::ptree&
	GetIndexedChild(const boost::property_tree::ptree& pt, const std::string& path);

	/**
	 * Put the child in the given ptree on the given path. The path can contain indexes.
	 * The path can't be empty and when a node doesn't exist, it will only be created when it has
	 * no index or when the index is zero.
	 *
	 * @param	pt		The given ptree.
	 * @param	path		The given path.
	 * @param	child		The given child.
	 */
	static void PutIndexedChild(
		boost::property_tree::ptree& pt,
		const std::string& path,
		const boost::property_tree::ptree& child);
};

} // namespace Util
} // namespace UA_CoMP

#endif // include-guard
