/*
 *  This file is part of the VirtualLeaf 2 (a.k.a. vleaf2) software.
 *  VirtualLeaf 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  VirtualLeaf 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with VirtualLeaf 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2012, Joeri Exelmans, Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * PTreeUtils implementation.
 */

#include "PTreeUtils.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/xpressive/xpressive.hpp>
#include "util/StringUtils.h"


using namespace boost::property_tree;
using namespace std;

namespace UA_CoMP {
namespace Util {

namespace {

const string ARRAY_SUFFIX = "_array";


/**
 * Flattens an array (sub)ptree
 *
 * @param	pt		The ptree to flatten
 * @param	path		The path to the array entry
 * @param	elementsKey	The key of the elements of the array
 */
list<pair<string, string>> FlattenArray(const ptree& pt, const string& elementsKey, const string& path)
{
	if (pt.size() == 0) {
		return list<pair<string, string>>({make_pair(path, pt.data().c_str())});
	} else {
		list<pair<string, string>> entries;
		int index = 0;
		for (ptree::const_iterator it = pt.begin(); it != pt.end(); it++) {
			const string& key = it->first;
			string newPath;
			if (key == elementsKey) {
				newPath = path + "[" + StringUtils::ToString(index++) + "]";
			} else {
				newPath = path == "" ? key : path + "." + key;
			}
			entries.splice(entries.end(), PTreeUtils::Flatten(it->second, true, newPath));
		}
		return entries;
	}
}

}

void PTreeUtils::CopyNonExistingChildren(ptree const& from, ptree& into, set<string> const& ignore)
{
	for (ptree::value_type const& v : from) {
		if (ignore.find(v.first) != ignore.end()) {
			continue;
		}
		try {
			// Will throw if child doesn't exist in result.
			ptree & child = into.get_child(v.first);
			// Continue recursively with subtree.
			CopyNonExistingChildren(v.second, child, ignore);
		}
		catch (ptree_bad_path&) {
			// Child didn't exist, copy entire subtree.
			into.put_child(v.first, v.second);
		}
	}
}

void PTreeUtils::CopyStructure(ptree const& from, ptree& into, string const& fill_value, set<string> const& ignore)
{
	for (ptree::value_type const& v : from) {
		if (ignore.find(v.first) != ignore.end()) {
			continue;
		}
		try {
			ptree & child = into.get_child(v.first); // Will throw if child doesn't exist in result.
			// Continue recursively with subtree.
			CopyStructure(v.second, child, fill_value, ignore);
		}
		catch (ptree_bad_path&) {
			if (v.second.empty()) {
				// Add child present in src to dst, replace value by fill_value
				into.put_child(v.first, ptree(fill_value));
			} else {
				// Add empty child in dst, continue recursively to fill child up.
				auto& inserted = into.put_child(v.first, ptree());
				CopyStructure(v.second, inserted, fill_value, ignore);
			}
		}
	}
}

void PTreeUtils::FillPTree(ptree& pt1, const ptree& pt2)
{
	assert(pt1.size() == pt2.size() && "The ptrees don't have the same structure.");
	if (pt1.size() == 0) {
		if (pt1.data() == "") {
			pt1.data() = pt2.data();
		}
	} else {
		ptree::iterator it1 = pt1.begin();
		ptree::const_iterator it2 = pt2.begin();
		while (it1 != pt1.end()) {
			assert(it1->first == it2->first && "The ptrees don't have the same structure.");
			FillPTree(it1->second, it2->second);
			++it1;
			++it2;
		}
	}
}

list<pair<string, string>> PTreeUtils::Flatten(const ptree& pt, bool indexedArray, const string& path)
{
	if (pt.size() == 0) {
		return list<pair<string, string>>({make_pair(path, pt.data().c_str())});
	} else {
		list<pair<string, string>> entries;
		for (ptree::const_iterator it = pt.begin(); it != pt.end(); ++it) {
			const string& key = it->first;
			string newPath = (path == "" ? key : path + "." + key);
			if (indexedArray && key.length() > ARRAY_SUFFIX.length()
				&& key.compare(key.length() - ARRAY_SUFFIX.length(),
					ARRAY_SUFFIX.length(), ARRAY_SUFFIX) == 0) {
				entries.splice(entries.end(), FlattenArray(it->second,
					key.substr(0, key.length() - ARRAY_SUFFIX.length()), newPath));
			} else {
				entries.splice(entries.end(), Flatten(it->second, indexedArray, newPath));
			}
		}
		return entries;
	}
}

const ptree& PTreeUtils::GetIndexedChild(const ptree& pt, const string& path)
{
	if (path == "") {
		return pt;
	} else {
		string pathElement = path;
		string newPath = "";

		size_t separatorIndex = path.find('.');
		if (separatorIndex != string::npos) {
			pathElement = path.substr(0, separatorIndex);
			newPath = path.substr(separatorIndex + 1);
		}
		assert(pathElement != "" && "There's no element after the separator.");


		//Check whether the path element is of this format: *_array[int].
		const boost::xpressive::sregex match_indexed = boost::xpressive::sregex::compile("(.+)_array\\[(\\d+)\\]");
		boost::xpressive::smatch match;
		if (boost::xpressive::regex_match(pathElement, match, match_indexed)) {
			string arrayElementKey = match[1];
			const ptree& subPtree = pt.get_child(arrayElementKey + ARRAY_SUFFIX);
			unsigned int index = StringUtils::FromString<unsigned int>(match[2]);
			for (const auto& entry : subPtree) {
				if (entry.first == arrayElementKey) {
					if (index == 0) {
						return GetIndexedChild(entry.second, newPath);
					} else {
						--index;
					}
				}
			}

			throw ptree_bad_path("No such node", boost::property_tree::path(pathElement));
		} else {
			return GetIndexedChild(pt.get_child(pathElement), newPath);
		}
	}
}

void PTreeUtils::PutIndexedChild(ptree& pt, const string& path, const ptree& child)
{
	if (path == "") {
		pt = child;
	} else {
		string pathElement = path;
		string newPath = "";

		size_t separatorIndex = path.find('.');
		if (separatorIndex != string::npos) {
			pathElement = path.substr(0, separatorIndex);
			newPath = path.substr(separatorIndex + 1);
		}
		assert(pathElement != "" && "There's no element after the separator.");

		//Check whether the path element is of this format: *_array[int].
		const boost::xpressive::sregex match_indexed
			= boost::xpressive::sregex::compile("(.+)_array\\[(\\d+)\\]");
		boost::xpressive::smatch match;
		if (boost::xpressive::regex_match(pathElement, match, match_indexed)) {
			string arrayElementKey = match[1];
			ptree& subPtree = pt.get_child(arrayElementKey + ARRAY_SUFFIX);
			unsigned int index = StringUtils::FromString<unsigned int>(match[2]);

			for (auto& entry : subPtree) {
				if (entry.first == arrayElementKey) {
					if (index == 0) {
						PutIndexedChild(entry.second, newPath, child);
						return;
					} else {
						--index;
					}
				}
			}

			throw ptree_bad_path("No such node", boost::property_tree::path(pathElement));
		} else {
			PutIndexedChild(pt.get_child(pathElement), newPath, child);
		}
	}
}

void PTreeUtils::RemoveNonExistingChildren(const ptree& src, ptree& dst, const set<string>& ignore)
{
	for (auto dst_it = dst.begin(); dst_it != dst.end(); ++dst_it) {
		if (ignore.find(dst_it->first) != ignore.end()) {
			continue;
		}
		try {
			// Will throw if child doesn't exist in src.
			const ptree& child = src.get_child(dst_it->first);
			// Continue recursively with subtree.
			RemoveNonExistingChildren(child, dst_it->second, ignore);
		}
		catch (ptree_bad_path&) {
			// Child didn't exist, copy entire subtree.
			dst_it = dst.erase(dst_it);
		}
	}
}

} // namespace Util
} // namespace UA_CoMP
