#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################

#============================================================================
# api_doc
#============================================================================
find_package( Doxygen )

if( DOXYGEN_FOUND )	

	#=== dot tool ===
	if( DOXYGEN_DOT_FOUND )
		set( DOXY_HAVE_DOT YES )
	else()
		set( DOXY_HAVE_DOT NO )
	endif()
	
	#=== setup ===			
	set( DOXY_HTML_DIR 	"api_doc_html" )
	configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.cmake.in 
					${CMAKE_CURRENT_BINARY_DIR}/Doxyfile )	

	#=== target ===
	add_custom_target( api_doc ALL	${DOXYGEN_EXECUTABLE} Doxyfile )
	
	#=== install ===
	# html generated directly to ${CMAKE_INSTALL_PREFIX}/doc/${DOXY_HTML_DIR}
	# need to install image files though
	
	install(DIRECTORY      ${CMAKE_CURRENT_BINARY_DIR}/${DOXY_HTML_DIR}
	       DESTINATION     ${DOC_INSTALL_LOCATION} )

	# Create a link to the documentation in the install package if packaging
	if( ${CMAKE_PROJECT_NAME}_MAKE_PACKAGE )
		package_create_doc_link()
	endif()
	
	add_subdirectory( images )
		
	#=== unset ===
	unset( DOXY_HAVE_DOT )
	unset( DOXY_HTML_DIR )
	
endif( DOXYGEN_FOUND )

#############################################################################