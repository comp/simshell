#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################
#
#  CmakeLists.txt for the whole project.
#
#############################################################################
cmake_minimum_required(VERSION 2.6)
if( NOT CMAKE_VERSION VERSION_LESS 3 )
	cmake_policy(SET CMP0042 OLD)
endif()

#============================================================================
# CMAKE_BUILD_TYPE: provide default build type:
#============================================================================
set( CMAKE_BUILD_TYPE "Release"
	CACHE STRING "Build type: None Debug Release RelWithDebInfo MinSizeRel."
)

#============================================================================
# CMAKE_INSTALL_PREFIX: install directory definitions:
#============================================================================
set( CMAKE_INSTALL_PREFIX  "${CMAKE_BINARY_DIR}/installed" 
	CACHE PATH "Prefix prepended to install directories" 
)
 
#============================================================================
# Project name:
#============================================================================
project( SIMSHELL )

#============================================================================
# Include file with configuration for CMake tool itself:
#============================================================================
include( CMakeCMakeConfig.txt )
 
#============================================================================
# Include file with macro definitions for build & install:
#============================================================================
include( CMakeBuildConfig.txt )

#============================================================================
# The C++ compile & link configuration is included at this level to 
# provide configuration for both the main/cpp and test/cpp directories.
#============================================================================
include( CMakeCPPConfig.txt )
  	
#============================================================================
# Add subdirs
#============================================================================
add_subdirectory( main )
add_subdirectory( test )
if( SIMSHELL_MAKE_DOC )
	add_subdirectory( doc )
endif()
		    		
#============================================================================
# overview report:
#============================================================================
message( STATUS " " )
message( STATUS "------> Settings and options: " )
message( STATUS "------> SIMSHELL_MAKE_DOC            : ${SIMSHELL_MAKE_DOC} "     )
message( STATUS "------> SIMSHELL_MAKE_PACKAGE        : ${SIMSHELL_MAKE_PACKAGE} " )
message( STATUS " " )
message( STATUS "------> Configuration: " )
message( STATUS "------> Using CMAKE_CXX_COMPILER     : ${CMAKE_CXX_COMPILER}"    )
if( CMAKE_CXX_COMPILER_ID STREQUAL "GNU" )
	message( STATUS "------> gcc cxx compiler version     : ${GNUCC_VERSION}"     )
endif()
if( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" )
	message( STATUS "------> Clang cxx compiler version   : ${CLANG_VERSION}"     )
endif()
message( STATUS "------> Using CMAKE_BUILD_TYPE       : ${CMAKE_BUILD_TYPE} "     )
message( STATUS "------> Using CMAKE_BINARY_DIR       : ${CMAKE_BINARY_DIR} "     )
message( STATUS "------> Using CMAKE_INSTALL_PREFIX   : ${CMAKE_INSTALL_PREFIX}"  )
message( STATUS "------> Using CMAKE_BUILD_TYPE        : ${CMAKE_BUILD_TYPE} "    )
message( STATUS "------> Using CMAKE_BINARY_DIR        : ${CMAKE_BINARY_DIR} "    )
message( STATUS "------> Using CMAKE_INSTALL_PREFIX    : ${CMAKE_INSTALL_PREFIX}" )
message( STATUS "------> Using CMAKE_CXX_COMPILER      : ${CMAKE_CXX_COMPILER}"   )
if( CMAKE_CXX_COMPILER_ID STREQUAL "GNU" )
	message( STATUS "------> gcc cxx compiler version      : ${GCC_VERSION}"      )
endif()
if( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" )
	message( STATUS "------> Clang cxx compiler version    : ${CLANG_VERSION}"           )
endif()
message( STATUS "------> Using CMAKE_CXX_FLAGS         : ${CMAKE_CXX_FLAGS}"             )
if( CMAKE_BUILD_TYPE MATCHES "^[Rr]elease$" )
	message( STATUS "------> Using CMAKE_CXX_FLAGS_RELEASE : ${CMAKE_CXX_FLAGS_RELEASE}" )
endif()
if( CMAKE_BUILD_TYPE MATCHES "^[Dd]ebug$" )
	message( STATUS "------> Using CMAKE_CXX_FLAGS_DEBUG   : ${CMAKE_CXX_FLAGS_DEBUG}"   )
endif()
message( STATUS " " )

#############################################################################
