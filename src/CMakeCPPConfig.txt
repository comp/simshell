#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################
#
#  This file contains the C++ compile & link configuration.
#  It is meant to be included in the src/CMakeLists.txt and 
#  to provide identical C++ configuration for the main/cpp 
#  and the test/ccp directories and subdirectories.
#
#############################################################################

#----------------------------------------------------------------------------
# Check CMAKE_BUILD_TYPE
#----------------------------------------------------------------------------
if( NOT (CMAKE_BUILD_TYPE MATCHES "Release" OR CMAKE_BUILD_TYPE MATCHES "Debug") )
	message(FATAL_ERROR  "========> CMAKE_BUILD_TYPE HAS TO MATCH EITHER Release OR Debug." )
endif()

#----------------------------------------------------------------------------
# Compile flags
#----------------------------------------------------------------------------
set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Ofast" )
set( CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG} -O0"   )

#----------------------------------------------------------------------------
# Platform dependent compile flags
#----------------------------------------------------------------------------
if( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_HOST_APPLE )
	add_definitions( -D__APPLE__ )
	set( CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libc++" )
elseif( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_HOST_APPLE )
	set( CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -std=c++11" )
elseif( NOT WIN32 AND CMAKE_CXX_COMPILER_ID STREQUAL "GNU" )
	set( CMAKE_CXX_FLAGS 	       "${CMAKE_CXX_FLAGS} -fPIC -std=c++11" )
elseif( WIN32 )
	set( CMAKE_CXX_FLAGS 	"${CMAKE_CXX_FLAGS} -std=c++0x -U__STRICT_ANSI__ -mthreads" )
endif()
#
include_directories( ${CMAKE_HOME_DIRECTORY}/main                  )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_cedit        )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_conv         )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_minimalshell )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_simshell     )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_util         )

#----------------------------------------------------------------------------
# Qt configuration
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
# Qt configuration
#----------------------------------------------------------------------------
set( QT_IS_STATIC       FALSE )
set( QT_USE_QTMAIN      TRUE  )
find_package( Qt5 COMPONENTS Core Gui PrintSupport Network Widgets QUIET ) # Prefer Qt5 over Qt4. Outcomment this line to always use Qt4.
if (Qt5_FOUND)
	set( VLEAF2_QT_VERSION 5 )
	set( QT_INCLUDE_DIR
		${Qt5Core_INCLUDE_DIRS}
		${Qt5Gui_INCLUDE_DIRS}
		${Qt5PrintSupport_INCLUDE_DIRS}
		${Qt5Network_INCLUDE_DIRS}
		${Qt5Widgets_INCLUDE_DIRS} )
	include_directories( ${QT_INCLUDE_DIR} )		
	set( QT_LIBRARIES
		${Qt5Core_LIBRARIES}
		${Qt5Gui_LIBRARIES}
		${Qt5PrintSupport_LIBRARIES}
		${Qt5Network_LIBRARIES}
		${Qt5Widgets_LIBRARIES} )
else()
	# If no Qt5, require Qt4
	find_package( Qt4 COMPONENTS QtCore QtGui QtNetwork REQUIRED)
	set( VLEAF2_QT_VERSION 4 )
	include( ${QT_USE_FILE} )
endif()
set( LIBS   ${LIBS}   ${QT_LIBRARIES} )
#
if( CMAKE_BUILD_TYPE MATCHES "Release" )
	add_definitions( -DQT_NO_DEBUG_OUTPUT -DQT_NO_WARNING_OUTPUT )
endif()
if( CMAKE_BUILD_TYPE MATCHES "Debug" )
	add_definitions( -DQDEBUG_OUTPUT )
endif()

#----------------------------------------------------------------------------
# TCLAP Library (command line processing)
#----------------------------------------------------------------------------
include_directories( ${CMAKE_HOME_DIRECTORY}/main/resources/lib/tclap/include )

#----------------------------------------------------------------------------
# Standard math lib
#----------------------------------------------------------------------------
set( LIBS   ${LIBS}   m )

#----------------------------------------------------------------------------
# Boost
#----------------------------------------------------------------------------
find_package( Boost REQUIRED )
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} )
set( LIBS   ${LIBS}    ${Boost_LIBRARIES} )

#----------------------------------------------------------------------------
# Linking (Note: if ${CMAKE_PROJECT_NAME}_MAKE_PACKAGE is ON the RPATH 
# will be patched in the packaging process on some platforms, e.g. Mac).
#----------------------------------------------------------------------------
set( BUILD_SHARED_LIBS 	    ON 	 )
set( CMAKE_INSTALL_RPATH    ${CMAKE_INSTALL_PREFIX}/${BIN_INSTALL_LOCATION} )

#----------------------------------------------------------------------------
# Win32/MinGW specific link configuration
#----------------------------------------------------------------------------
if( WIN32 )
	#------------- Setting linker flags 
	set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -mthreads -Wl,--allow-multiple-definition -Wl,-enable-auto-import")
	set( CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -mthreads -Wl,--allow-multiple-definition -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc -Wl,-subsystem,windows")		
	#------------- MinGW specific libs
	set( LIBS   ${LIBS}   mingw32 wsock32 )
endif()

#############################################################################
