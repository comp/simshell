#############################################################################
#  This file is part of the SimShell software. 
#  SimShell is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  SimShell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with SimShell.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
#
#############################################################################
#
#   Meta makefile calls cmake to do the heavy lifting. It first
#   includes the file MakeLocalConfig (if it exists) for local
#   configuration. If no such file exists the defaults below apply.
#   This file is tracked by the mercurial repository so do not
#   change this for personal customization. That should be done in
#   the file MakeLocalConfig.
#
#############################################################################

#============================================================================
#   Project name
#============================================================================
PROJECT = SIMSHELL

#============================================================================
#   Load MakeLocalConfig (if it exists) to override defaults below
#============================================================================
MakeLocalConfig = $(wildcard MakeLocalConfig)
ifeq ($(MakeLocalConfig),MakeLocalConfig)
	include MakeLocalConfig
endif

#============================================================================
# 	CMake command
#============================================================================
ifeq ($(CMAKE),)
	CMAKE = cmake
endif

#============================================================================
#   Platform info: Windows, Mac, Linux/UNIX
#============================================================================
ifdef SystemRoot
	# Compiling for MinGW on Windows
	ifeq ($(CMAKE_GENERATOR),)
		CMAKE_GENERATOR = "MinGW Makefiles"
	endif
else
	# Compiling for Mac OSX or *nix
	ifeq ($(CMAKE_GENERATOR),)
		CMAKE_GENERATOR = "Unix Makefiles"
	endif
endif

#============================================================================
#   MACRO definitions to pass on to cmake
#============================================================================
ifneq ($(CMAKE_GENERATOR),)
	CMAKE_ARGS += -DCMAKE_GENERATOR=$(CMAKE_GENERATOR)
endif
ifneq ($(CMAKE_BUILD_TYPE),)
	CMAKE_ARGS += -DCMAKE_BUILD_TYPE:STRING=$(CMAKE_BUILD_TYPE)
endif
ifneq ($(CMAKE_INSTALL_PREFIX),)
	CMAKE_ARGS += -DCMAKE_INSTALL_PREFIX:PATH=$(CMAKE_INSTALL_PREFIX)
endif
ifneq ($(CMAKE_C_COMPILER),)
	CMAKE_ARGS += -DCMAKE_C_COMPILER:FILEPATH=$(CMAKE_C_COMPILER)
endif
ifneq ($(CMAKE_CXX_COMPILER),)
	CMAKE_ARGS += -DCMAKE_CXX_COMPILER:FILEPATH=$(CMAKE_CXX_COMPILER)
endif
ifneq ($(CMAKE_CXX_FLAGS),)
	CMAKE_ARGS += -DCMAKE_CXX_FLAGS:STRING=$(CMAKE_CXX_FLAGS)
endif
ifneq ($(CMAKE_CXX_FLAGS_DEBUG),)
	CMAKE_ARGS += -DCMAKE_CXX_FLAGS_DEBUG:STRING=$(CMAKE_CXX_FLAGS_DEBUG)
endif
ifneq ($(SIMSHELL_MAKE_DOC),)
	CMAKE_ARGS += -DSIMSHELL_MAKE_DOC:BOOL=$(SIMSHELL_MAKE_DOC)
endif
ifeq ($(BUILD_DIR),)
	BUILD_DIR = ./build
endif

#============================================================================
#   Targets
#============================================================================
.PHONY: help configure all install package test installcheck distclean

help:
	@ $(CMAKE) -E echo " "
	@ $(CMAKE) -E echo " Current macro values are (cmake will use an appropriate"
	@ $(CMAKE) -E echo " default for any macro that has not been set):"
	@ $(CMAKE) -E echo "   BUILD_DIR               : " $(BUILD_DIR)
	@ $(CMAKE) -E echo "   CMAKE_GENERATOR         : " $(CMAKE_GENERATOR)
	@ $(CMAKE) -E echo "   CMAKE_C_COMPILER        : " $(CMAKE_C_COMPILER)
	@ $(CMAKE) -E echo "   CMAKE_CXX_COMPILER      : " $(CMAKE_CXX_COMPILER)
	@ $(CMAKE) -E echo "   CMAKE_CXX_FLAGS         : " $(CMAKE_CXX_FLAGS)
	@ $(CMAKE) -E echo "   CMAKE_BUILD_TYPE        : " $(CMAKE_BUILD_TYPE)
	@ $(CMAKE) -E echo "   CMAKE_INSTALL_PREFIX    : " $(CMAKE_INSTALL_PREFIX)
	@ $(CMAKE) -E echo "   SIMSHELL_MAKE_DOC       : " $(SIMSHELL_MAKE_DOC)
	@ $(CMAKE) -E echo " "
				
configure: 
	$(CMAKE) -E make_directory $(BUILD_DIR)
	$(CMAKE) -E chdir $(BUILD_DIR) $(CMAKE) $(CMAKE_ARGS) -DSIMSHELL_MAKE_PACKAGE=OFF ../src

all: configure
	$(MAKE) $(PARALLEL_MAKE) -C $(BUILD_DIR) all

install: configure
	$(MAKE) $(PARALLEL_MAKE) -C $(BUILD_DIR) --no-print-directory install   
	
install_main:
	$(MAKE) $(PARALLEL_MAKE) -C $(BUILD_DIR)/main --no-print-directory install

install_test: install_main
	$(MAKE) $(PARALLEL_MAKE) -C $(BUILD_DIR)/test --no-print-directory install

package:
	$(CMAKE) -E make_directory $(BUILD_DIR)
	@ $(CMAKE) -E echo "Bootstrapping, now enabling packaging commands"
	$(CMAKE) -E chdir $(BUILD_DIR) $(CMAKE) $(CMAKE_ARGS) -D$(PROJECT)_MAKE_PACKAGE=ON ../src
	$(MAKE)  -C $(BUILD_DIR) package
	@ $(CMAKE) -E echo " Bootstrapping, now disabling packaging commands"
	$(CMAKE) -E chdir $(BUILD_DIR) $(CMAKE) $(CMAKE_ARGS) -D$(PROJECT)_MAKE_PACKAGE=OFF ../src

distclean clean:
	$(CMAKE) -E remove_directory $(BUILD_DIR)

test installcheck: install_test
	$(MAKE) -C $(BUILD_DIR)/test --no-print-directory run_ctest
	
#############################################################################
